﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HotbarUI : MonoBehaviour
{

    public Image inventoryIconPrefab;
    public HotbarCellUI hotBarCellPrefab;
    public Color hotbarCellSelectedColour;

    public int Length { get; private set; }
    HotbarCellUI[] hotBarCellImages;

    private void Awake()
    {

        Length = 8;
        hotBarCellImages = new HotbarCellUI[Length];

        for (int i = -4; i < 4; i++)
        {
            HotbarCellUI hotBarCell = Instantiate(hotBarCellPrefab, transform, false);
            hotBarCell.transform.localPosition = new Vector3((i * hotBarCell.CellImage.preferredWidth) + (hotBarCell.CellImage.preferredWidth / 2f), 0, 0);
            hotBarCellImages[i + 4] = hotBarCell;
        }
    }

    public void UpdateHotbarUI(InventorySystem inventory)
    {
        for (int i = 0; i < hotBarCellImages.Length; i++)
        {
            if (i == inventory.selectedHotbarCellIndex)
            {
                hotBarCellImages[i].SetColour(hotbarCellSelectedColour);
            }
            else
            {
                hotBarCellImages[i].SetColour(Color.white);
            }
        }

        for (int i = 0; i < hotBarCellImages.Length; i++)
        {
            // Hotbar is first X cells of inventory by convention so just check inventory.cells[i]
            if (inventory.cells[i].ContainsItem)
            {
                hotBarCellImages[i].SetIcon(inventory.cells[i].containedItem.icon);
                hotBarCellImages[i].itemCountText.SetText(inventory.cells[i].itemCount.ToString());
            }
            else
            {
                hotBarCellImages[i].SetIcon(null);
                hotBarCellImages[i].itemCountText.SetText("");
            }
        }
    }
}
