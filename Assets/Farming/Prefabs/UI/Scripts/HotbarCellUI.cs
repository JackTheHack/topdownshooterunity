﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HotbarCellUI : MonoBehaviour
{
    public Image CellImage { get; private set; }
    public Image itemIcon;
    public TextMeshProUGUI itemCountText;

    public bool ContainsItem
    {
        get
        {
            return itemIcon.sprite != null;
        }
    }

    protected virtual void Awake()
    {
        CellImage = GetComponent<Image>();
    }

    public void SetIcon(Sprite icon)
    {
        if (icon == null)
        {
            itemIcon.color = new Color(1, 1, 1, 0);
        }
        else
        {
            itemIcon.color = new Color(1, 1, 1, 1);
        }
        itemIcon.sprite = icon;
    }

    public void SetColour(Color colour)
    {
        CellImage.color = colour;
    }
}
