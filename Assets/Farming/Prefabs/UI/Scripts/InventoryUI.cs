﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{

    public InventoryCellUI inventoryCellPrefab;

    public InventoryCellUI[] uiCells;
    public Vector2 cellPadding;

    Image inventoryPanel;

    public bool generateHotbarLabels;

    public bool InventoryOpen {
        get
        {
            return inventoryPanel.enabled;
        }
    }

    private void Awake()
    {
        inventoryPanel = GetComponent<Image>();
    }

    public void Init(InventorySystem inv)
    {
        uiCells = new InventoryCellUI[inv.cells.Length];

        int halfWidth = Mathf.CeilToInt(inv.Width / 2f);

        int cellID = 0;
        for (int y = 0; y < inv.Height; y++)
        {
            for (int i = -halfWidth; i < halfWidth; i++)
            {
                if (inv.Width == 1 && inv.Height == 1 && i == halfWidth - 1)
                {
                    Debug.Log("uiCell workaround");
                    break;
                }

                int index = (y * inv.Width) + i + halfWidth;

                uiCells[index] = Instantiate(inventoryCellPrefab, transform, false);
                uiCells[index].Init(cellID, inv);

                if (generateHotbarLabels && cellID < inv.Width)
                {
                    uiCells[index].SetCellLabel(cellID.ToString() );
                } else
                {
                    uiCells[index].SetCellLabel("");
                }

                Image img = uiCells[index].GetComponent<Image>();
                uiCells[index].transform.localPosition = new Vector3((i * (img.preferredWidth + cellPadding.x)) + (img.preferredWidth / 2f) , y * (-img.preferredHeight + cellPadding.y), 0);

                cellID++;
            }
        }
    }

    public void TearDown()
    {
        if (uiCells != null)
        {
            for (int i = uiCells.Length - 1; i >= 0; i--)
            {
                Destroy(uiCells[i].gameObject);
            }
            uiCells = null;
        }
        
    }

    public void UpdateInventoryUI(InventorySystem inventory)
    {
        for (int i = 0; i < inventory.cells.Length; i++)
        {
            if (inventory.cells[i].ContainsItem)
            {
                uiCells[i].SetIcon(inventory.cells[i].containedItem.icon);
                uiCells[i].itemCountText.SetText(inventory.cells[i].itemCount.ToString());
            }
            else
            {
                uiCells[i].SetIcon(null);
                uiCells[i].itemCountText.SetText("");
            }
        }
    }

    public bool ToggleMainPanelVisibility()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
        return gameObject.activeInHierarchy;
    }

    public void MakeVisible()
    {
        gameObject.SetActive(true);
    }

    public void MakeInvisible()
    {
        gameObject.SetActive(false);
    }

}
