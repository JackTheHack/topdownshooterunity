﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryCellUI : HotbarCellUI
{

    int id;
    public TextMeshProUGUI cellLabel;
    FarmerUIManager uiManager;

    InventorySystem attachedInventory;

    public void Init(int id, InventorySystem attachedInventory)
    {
        this.id = id;
        this.attachedInventory = attachedInventory;
    }

    public void SetCellLabel(string label)
    {
        cellLabel.SetText(label);
    }

    protected override void Awake()
    {
        uiManager = GetComponentInParent<FarmerUIManager>();
        //cellLabel = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void OnCellLeftClick()
    {
        if (uiManager.HoldingItem)
        {
            uiManager.PlaceItemInCell(attachedInventory, id, InventoryMoveType.WholeStack);
        } else if (ContainsItem)
        {
            uiManager.PickItemFromCell(attachedInventory, id, InventoryMoveType.WholeStack);
        }
    }

    public void OnCellRightClick()
    {

        uiManager.SwapOrTakeOne(attachedInventory, id);
        //if (ContainsItem && uiManager.HoldingItem)
        //{
        //    // single item removal, either swap or take one depending on if the items are the same or not.
            
        //} else if (ContainsItem && !uiManager.HoldingItem)
        //{
        //    uiManager.PickItemFromCell(id, InventoryMoveType.SingleItem);
        //} else if (!ContainsItem && uiManager.HoldingItem)
        //{
        //    uiManager.PlaceItemInCell(id, InventoryMoveType.SingleItem);
        //}
    }


}
