﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TerrainNoise
{

    // Determines how normalisation is done, estimation of min and max noise values.
    public enum NormaliseMode { Local, Global}
    
    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, TerrainNoiseSettings settings, Vector2 sampleCentre)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        System.Random prng = new System.Random(settings.seed);
        Vector2[] octaveOffsets = new Vector2[settings.octaves];

        float maxPossibleHeight = 0;
        float amplitude = 1;
        float frequency = 1;

        for (int i = 0; i < settings.octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + settings.offset.x + sampleCentre.x;
            float offsetY = prng.Next(-100000, 100000) - settings.offset.y - sampleCentre.y;

            octaveOffsets[i] = new Vector2(offsetX, offsetY);

            maxPossibleHeight += amplitude;
            amplitude *= settings.persistence;
        }

        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;

        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                amplitude = 1;
                frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < settings.octaves; i++)
                {
                    float sampleX = (x - halfWidth + octaveOffsets[i].x) / settings.scale * frequency;
                    float sampleY = (y - halfHeight + octaveOffsets[i].y) / settings.scale * frequency;

                    float perlinValue = (Mathf.PerlinNoise(sampleX, sampleY) * 2) - 1; // range of [-1,1]
                    noiseHeight += perlinValue * amplitude;

                    // Decreases each octave since persistence between 0 and 1.
                    amplitude *= settings.persistence;
                    frequency *= settings.lacunarity;
                }

                // Keep track of max and min for normalising afterwards.
                if (noiseHeight > maxLocalNoiseHeight)
                {
                    maxLocalNoiseHeight = noiseHeight;
                }
                if (noiseHeight < minLocalNoiseHeight)
                {
                    minLocalNoiseHeight = noiseHeight;
                }

                noiseMap[x, y] = noiseHeight;

                if (settings.normaliseMode == NormaliseMode.Global)
                {
                    float normalisedHeight = (noiseMap[x, y] + 1) / (maxPossibleHeight / 0.9f);
                    noiseMap[x, y] = Mathf.Clamp(normalisedHeight, 0, int.MaxValue);
                }
            }
        }

        if (settings.normaliseMode == NormaliseMode.Local)
        {
            // Normalise noiseHeight values to [0,1]
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                }
            }
        }

        return noiseMap;
    }
}

[System.Serializable]
public class TerrainNoiseSettings
{
    public TerrainNoise.NormaliseMode normaliseMode;
    public float scale = 50f;

    [Range(0, 10)]
    public int octaves = 6;

    [Range(0, 1)]
    public float persistence = 0.6f;

    [Range(1, 10)]
    public float lacunarity = 2f;

    public int seed;
    public Vector2 offset;

    public void ValidateValues()
    {
        scale = Mathf.Max(scale, 0.01f);
        octaves = Mathf.Max(octaves, 1);
        lacunarity = Mathf.Max(lacunarity, 1);
        persistence = Mathf.Clamp01(persistence);
    }
}