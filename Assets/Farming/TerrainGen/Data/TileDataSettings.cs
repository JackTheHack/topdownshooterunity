﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/TileDataSettings")]
public class TileDataSettings : ScriptableObject
{
    public TerrainTileData[] tileDatas;

}
