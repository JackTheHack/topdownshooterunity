﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Tilemaps;

//public class TilemapTerrainChunk
//{
//    public event System.Action<TilemapTerrainChunk, bool> onVisibilityChanged;
//    public Vector2 coord;

//    const float colliderGenerationDistanceThreshold = 5f;

//    GameObject meshObject;
//    Vector2 sampleCentre;
//    public Bounds bounds;

//    HeightMap heightMap;
//    bool heightMapReceived;

//    bool hasSetCollider;
//    float maxViewDist;

//    HeightMapSettings heightMapSettings;
//    Transform viewer;

//    Grid tileGrid;
//    Tilemap tilemap;
//    TerrainTileData[] tileDataObjects;

//    public TilemapTerrainChunk(Vector2Int coord, Grid tileGrid, Tilemap tilemap, HeightMapSettings heightMapSettings, TerrainTileData[] tileDataObjects, Transform parent, Transform viewer)
//    {
//        this.coord = coord;
//        this.tileGrid = tileGrid;
//        this.tilemap = tilemap;
//        this.heightMapSettings = heightMapSettings;
//        this.viewer = viewer;
//        this.tileDataObjects = tileDataObjects;

//        float chunkWorldSize = (TilemapTerrainGen.ChunkSize * tileGrid.cellSize.x);

        
//        int halfChunkSize = TilemapTerrainGen.ChunkSize / 2;

//        Vector2 chunkCentre = new Vector2((coord.x * chunkWorldSize) + chunkWorldSize * 0.5f, (coord.y * chunkWorldSize) + chunkWorldSize * 0.5f);

//        //Vector2 chunkCentrePos = tileGrid.CellToWorld(new Vector3Int((coord.x * TilemapTerrainGen.ChunkSize) + halfChunkSize, (coord.y * TilemapTerrainGen.ChunkSize) + halfChunkSize, 0));
//        bounds = new Bounds(chunkCentre, Vector3.one * chunkWorldSize);

//        sampleCentre = chunkCentre;

//        maxViewDist = 200;
//    }

//    public void Load()
//    {
//        ThreadedDataRequester.RequestData(() => HeightMapGenerator.GenerateHeightMap(TilemapTerrainGen.ChunkSize, TilemapTerrainGen.ChunkSize, heightMapSettings, sampleCentre), OnHeightMapReceived);
//    }

//    void OnHeightMapReceived(object heightMap)
//    {
//        this.heightMap = (HeightMap)heightMap;
//        heightMapReceived = true;

//        UpdateTerrainChunk();
//    }

//    Vector2 viewerPosition
//    {
//        get
//        {
//            return new Vector2(viewer.position.x, viewer.position.y);
//        }
//    }

//    public void UpdateTerrainChunk()
//    {
//        if (heightMapReceived)
//        {
//            float viewerDistFromNearestEdge = Mathf.Sqrt(bounds.SqrDistance(viewerPosition));

//            bool visible = viewerDistFromNearestEdge <= maxViewDist;

//            if (visible)
//            {
//                Vector3Int chunkBasePos = new Vector3Int(((int)coord.x * TilemapTerrainGen.ChunkSize), ((int)coord.y * TilemapTerrainGen.ChunkSize), 0);
//                bool freshChunk = TerrainManager.terrainDataDict.ContainsKey(chunkBasePos);


//                //tilemap.BoxFill(chunkBasePos, tileDataObjects[0].tile, chunkBasePos.x, chunkBasePos.y, chunkBasePos.x + TilemapTerrainGen.ChunkSize, chunkBasePos.y + TilemapTerrainGen.ChunkSize);
                

//                for (int y = 0; y < TilemapTerrainGen.ChunkSize; y++)
//                {
//                    for (int x = 0; x < TilemapTerrainGen.ChunkSize; x++)
//                    {

//                        float normalisedValue = Mathf.InverseLerp(heightMap.minValue, heightMap.maxValue, heightMap.values[x, y]);
//                        TerrainTileData chosenTileData = tileDataObjects[0];

//                        for (int i = 0; i < tileDataObjects.Length; i++)
//                        {
//                            if (normalisedValue > tileDataObjects[i].startHeight)
//                            {
//                                chosenTileData = tileDataObjects[i];
//                            }
//                        }
                        
                        

//                        Vector3Int position = new Vector3Int( ((int)coord.x * TilemapTerrainGen.ChunkSize) + x, ((int)coord.y * TilemapTerrainGen.ChunkSize) + y, 0);

//                        tilemap.SetTile(position, chosenTileData.tile);

//                        if (freshChunk)
//                        {
//                            TerrainManager.terrainDataDict.Add(position, chosenTileData);
//                        }
//                    }
//                }

//            }
//        }
//    }
//}