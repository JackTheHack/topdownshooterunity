﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldItem : MonoBehaviour
{
    public ItemData itemData;

    public bool placedInEditor;

    SpriteRenderer spriteRenderer;

    public Vector3Int[] tilePositions;

    protected int numberHeld;
    public virtual int NumberHeld
    {
        get { return numberHeld; }
        set
        {
            numberHeld = Mathf.Max(0, value);
        }
    }

    protected virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Start()
    {

    }

    public virtual void ChangeItemData(ItemData itemData)
    {
        this.itemData = itemData;
        RefreshSprite(itemData.GetSpriteForWorldItem());
    }

    public void RemoveItemData()
    {
        this.itemData = null;
        spriteRenderer.enabled = false;
    }

    public void RefreshSprite(Sprite sprite)
    {
        spriteRenderer.sprite = sprite;
        spriteRenderer.enabled = true;
    }

    public void SetToWorldSprite()
    {
        spriteRenderer.sprite = itemData.GetSpriteForWorldItem();
        spriteRenderer.enabled = true;
    }

    public bool InteractWith()
    {
        if (itemData.ItemType == ItemType.Storage)
        {
            StorageFurnitureData storageData = itemData as StorageFurnitureData;
            return true;
        }

        return false;
    }

}
