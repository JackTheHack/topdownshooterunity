﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShippingBin : EditorPlacedItem
{
    StorageComponent storage;

    GameTimeManager timeManager;

    // Start is called before the first frame update
    void Start()
    {
        storage = GetComponent<StorageComponent>();

        storage.Inventory.OnInsert += SellItem;

        timeManager = FindObjectOfType<GameTimeManager>();
        timeManager.OnEndDay += SellItemLeftInBin;
    }

    //you're a bean  <3


    void SellItem(InventoryAction invAction)
    {
        if (invAction.result == InventoryActionResult.Swap)
        {
            RemoveAndSell(invAction.cellIndex);
            // Insert the currently held item, this will destroy it and fill the cell.
            storage.ItemUseManager.RequestInventoryInsertion(storage.Inventory, invAction.cellIndex, InventoryMoveType.WholeStack);
        }
        // Can probably refactor this case into farmerUIManager since it only really deals with UI stuff
        // and is a duplicate of the default InsertCallback
        else if (storage.FarmerUIManager.HoldingItem && (invAction.result == InventoryActionResult.Merge || invAction.result == InventoryActionResult.Insertion))
        {

            // If a whole stack move, then delete the held item.
            // If a single item move, then check if no items are left.
            if (invAction.moveType == InventoryMoveType.WholeStack)
            {
                storage.FarmerUIManager.DestroyHeldUIItem();
            }
            else if (invAction.moveType == InventoryMoveType.SingleItem)
            {
                storage.FarmerUIManager.currentlyHeldItemGameobject.NumberHeld -= 1;
                if (storage.FarmerUIManager.currentlyHeldItemGameobject.NumberHeld == 0)
                {
                    storage.FarmerUIManager.DestroyHeldUIItem();
                }
            }
        }

        storage.ItemUseManager.UpdateUIAndCheckEquipStatus(invAction.inventory);
    }

    void RemoveAndSell(int cellIndex)
    {
        // Remove the item in the cell and put it into a new held item without assigning it yet.
        InventoryActionResult result = storage.Inventory.RequestRemoval(cellIndex, out UseableItemData soldItem, out int soldAmount, InventoryMoveType.WholeStack);
        PersistentDayData.AddSoldItem(new SellRecord(soldItem, soldAmount));
    }

    void SellItemLeftInBin()
    {
        if (storage.Inventory.cells[0].ContainsItem)
        {
            RemoveAndSell(0);
        }

        Debug.Log("Selling items left in the shipping bin.");
    }

}

public struct SellRecord
{
    public UseableItemData itemData;
    public int amount;

    public SellRecord(UseableItemData itemData, int amount)
    {
        this.itemData = itemData;
        this.amount = amount;
    }
}