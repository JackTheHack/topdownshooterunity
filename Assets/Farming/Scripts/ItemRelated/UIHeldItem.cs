﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIHeldItem : WorldItem
{

    TextMeshPro itemCountText;

    public override int NumberHeld
    {
        get { return numberHeld; }
        set
        {
            numberHeld = Mathf.Max(0, value);
            itemCountText.text = value.ToString();
        }
    }

    protected override void Awake()
    {
        base.Awake();
        itemCountText = GetComponentInChildren<TextMeshPro>();
    }

    private void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = mousePos;
    }

    public override void ChangeItemData(ItemData itemData)
    {
        this.itemData = itemData;
        RefreshSprite(itemData.icon);
    }

}
