﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Things that the player can select from their hotbar or inventory and will do something when the player presses a button.
// Either swinging a tool, placing an object, eating a piece of food etc.
public class UseableItem : MonoBehaviour
{

    public UseableItemData itemData;
    public bool readyToUse;

    ParticleSystem useEffect;

    public bool ReadyForItemUse
    {
        get
        {
            return itemData != null && readyToUse;
        }
    }

    SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        
        readyToUse = true;
    }

    public void ChangeItemData(UseableItemData itemData)
    {
        MakeReady();
        this.itemData = itemData;
        spriteRenderer.sprite = itemData.inHandSprite;
        spriteRenderer.enabled = true;

        if (itemData.ItemType == ItemType.Tool)
        {
            ToolData toolData = itemData as ToolData;
            if (toolData != null && toolData.useEffect != null)
            {
                if (useEffect != null)
                {
                    Destroy(useEffect.gameObject);
                }
                useEffect = Instantiate(toolData.useEffect, transform, false);
            }
        }
    }

    public void RemoveItemData()
    {
        if (this.itemData != null)
        {
            this.itemData = null;
            spriteRenderer.enabled = false;

            if (useEffect != null)
            {
                Destroy(useEffect.gameObject);
            }
        }
    }

    public void Use()
    {
        readyToUse = false;
        if (useEffect != null)
        {
            useEffect.Play();
        }
        Invoke("MakeReady", itemData.useCooldownTime);
    }

    private void MakeReady()
    {
        readyToUse = true;
    }

}
