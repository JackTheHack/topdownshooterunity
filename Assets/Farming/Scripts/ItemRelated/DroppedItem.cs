﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItem : WorldItem
{
    public float pickupForce = 2f;
    public float attractionDistance = 2f;
    public float pickupDistance = 0.1f;

    public bool moveLocked;

    FarmerController target;
    Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {

        rb2d = GetComponent<Rigidbody2D>();

        target = FindObjectOfType<FarmerController>();
        target.NotifyDroppedItemCreated(this);

        RefreshSprite(itemData.icon);

        LockMovement();
    }

    public void LockMovement()
    {
        moveLocked = true;
    }

    public void UnlockMovement()
    {
        moveLocked = false;
    }


    public void SuckIn()
    {
        if (!moveLocked)
        {
            Vector2 diff = (Vector2)target.transform.position - (Vector2)transform.position;
            rb2d.AddForce(diff.normalized * pickupForce);
        }
    }




}
