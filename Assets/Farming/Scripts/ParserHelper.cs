﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public static class ParserHelper
{
    public const string ITEM_SEPARATOR = ";";
    public const string PART_SEPARATOR = ",";
    public const string SUB_ITEM_SEPARATOR = "#";

    public static void WriteSavedToString<T>(string filename, T[] items, Func<T, String> writer)
    {
        var f = File.CreateText(filename);
        for (int i = 0; i < items.Length; i++)
        {
            f.WriteLine(writer(items[i]));
        }
        f.Close();
    }

    public static void WriteSavedToString<T>(string filename, T item, Func<T, String> writer)
    {
        var f = File.CreateText(filename);
        f.WriteLine(writer(item));
        f.Close();
    }

    public static T[] ReadSaved<T>(String filename, Func<String, T> parser)
    {
        List<T> records = new List<T>();
        if (File.Exists(filename))
        {
            var sr = File.OpenText(filename);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                records.Add(parser(line));
            }
            sr.Close();
        }
        else
        {
            Debug.Log("Could not open the file " + filename + " for reading.");
        }
        return records.ToArray();
    }

    // ================================================================================================= //

    public static Vector3Int[] ParseVector3Int(string vecString)
    {
        string[] items = vecString.Split(SUB_ITEM_SEPARATOR[0]);
        List<Vector3Int> vecs = new List<Vector3Int>();

        for (int i = 0; i < items.Length; i++)
        {
            string[] parts = items[i].Split(PART_SEPARATOR[0]);
            vecs.Add(new Vector3Int(int.Parse(parts[0]), int.Parse(parts[1]), 0));
        }

        return vecs.ToArray();
    }

    public static string Vector3IntToString(Vector3Int pos)
    {
        return pos.x + PART_SEPARATOR + pos.y;
    }

    public static string Vector3IntToString(Vector3Int[] multiPos)
    {
        System.Text.StringBuilder b = new System.Text.StringBuilder();
        for (int i = 0; i < multiPos.Length; i++)
        {
            b.Append(Vector3IntToString(multiPos[i]) + SUB_ITEM_SEPARATOR);
        }
        if (b.Length > 0)
        {
            b.Remove(b.Length - 1, 1); // Remove trailing separator
        }
        return b.ToString();
    }

    // ================================================================================================= //

    public static string SavedTileToString(TerrainManager.SavedTile tile)
    {
        return Vector3IntToString(tile.pos) + ITEM_SEPARATOR[0] + TerrainTypeToString(tile.terrainType);
    }

    public static TerrainManager.SavedTile ParseSavedTile(string tileString)
    {
        string[] items = tileString.Split(ITEM_SEPARATOR[0]);
        if (items.Length == 2)
        {
            // Making the assumption here that a SavedTile will always have only one position
            // only taking the [0] item
            return new TerrainManager.SavedTile(ParseVector3Int(items[0])[0], ParseTerrainType(items[1]));
        } else
        {
            return new TerrainManager.SavedTile(new Vector3Int(0, 0, 0), TerrainType.Water);
        }
    }

    public static string TerrainTypeToString(TerrainType terrainType)
    {
        return terrainType.ToString();
    }

    public static TerrainType ParseTerrainType(string terrainTypeString)
    {
        return (TerrainType) Enum.Parse(typeof(TerrainType), terrainTypeString);
    }

    // ========================================================================================== //

    public static string SavedWorldItemToString(TerrainManager.SavedWorldItem savedItem)
    {
        return savedItem.key + ITEM_SEPARATOR[0] + SavedItemDataToString(savedItem);
    }

    public static TerrainManager.SavedWorldItem ParseSavedWorldItem(string itemString)
    {
        string[] items = itemString.Split(ITEM_SEPARATOR[0]);
        if (items.Length >= 2)
        {
            string extra = "";
            if (items.Length == 3)
            {
                extra = items[2];
            }
            return new TerrainManager.SavedWorldItem(items[0], ParseVector3Int(items[0]), ParseItemData(items[1]), extra);
        }
        else
        {
            Debug.Log("Unable to parse SavedWorldItem from string: " + itemString);
            return new TerrainManager.SavedWorldItem("", null);
        }
    }

    public static string SavedItemDataToString(TerrainManager.SavedWorldItem savedItem)
    {
        string name = savedItem.itemData.itemName;
        switch (savedItem.itemData.ItemType)
        {
            case ItemType.Furniture:
                //(savedItem.itemData as FurnitureData).CalculateCellCoverage()
                return name;
            case ItemType.Plant:
                PlantComponent plant = savedItem.worldItem.GetComponent<PlantComponent>();
                return name + ITEM_SEPARATOR[0] + /* extraData -> */ plant.DatePlanted.GetSaveString();
            case ItemType.Storage:
                // assume its a WorldItem rather than a WorldTile
                StorageComponent store = savedItem.worldItem.GetComponent<StorageComponent>();
                return name + ITEM_SEPARATOR[0] + /* extraData -> */ store.Inventory.GenerateSaveString();
        }
        return "";
    }

    public static ItemData ParseItemData(string itemName)
    {
        //string[] parts = itemDataString.Split(PART_SEPARATOR[0]);

        //extraData = new string[parts.Length - 1];
        //Array.Copy(parts, 1, extraData, 0, extraData.Length);

        // parts[0] should contain item name;
        bool result = ItemDefinitionStore.allItemsDict.TryGetValue(itemName, out ItemData itemDataPrefab);
        if (result)
        {
            return itemDataPrefab;
        } else
        {
            Debug.LogError("Unable to parse item data from name: " + itemName);
            return null;
        }
    }

    // ========================================================================================== //

    public static string SavedStatsToString(FarmerStatsManager.SavedPlayerStats stats)
    {
        return stats.health.GetSaveString() + ITEM_SEPARATOR[0] + stats.energy.GetSaveString() + ITEM_SEPARATOR[0] + stats.money;
    }

    public static FarmerStatsManager.SavedPlayerStats ParseSavedStats(string savedStats)
    {
        string[] parts = savedStats.Split(ITEM_SEPARATOR[0]);

        if (parts.Length == 3)
        {
            RangeStat health = RangeStat.Parse(parts[0]);
            RangeStat energy = RangeStat.Parse(parts[1]);
            int money = int.Parse(parts[2]);
            return new FarmerStatsManager.SavedPlayerStats(health, energy, money);
        } else
        {
            Debug.LogError("Incorrect number of parts parsed from SavedPlayerStats string (expected 3): " + parts.Length);
            return new FarmerStatsManager.SavedPlayerStats(null, null, 0);
        }
    }




    // ========================================================================================== //

}
