﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LightingData")]
public class LightingData : ScriptableObject
{
    public Gradient gradient;
}
