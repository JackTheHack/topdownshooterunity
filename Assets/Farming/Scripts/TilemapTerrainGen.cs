﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Tilemaps;

//public class TilemapTerrainGen : MonoBehaviour
//{

//    public HeightMapSettings heightMapSettings;
//    public Tilemap terrainTilemap;
//    public static int ChunkSize = 16;

//    Grid tileGrid;

//    public TerrainTileData[] tileDataObjects;

//    TerrainManager terrainManager;


//    const float viewerMoveThresholdForChunkUpdate = 5f;
//    const float sqrViewerMoveThresholdForChunkUpdate = viewerMoveThresholdForChunkUpdate * viewerMoveThresholdForChunkUpdate;

//    public int colliderLODIndex;

//    public Transform viewer;

//    Vector2 viewerPosition;
//    Vector2 viewerPositionOld;

//    int chunksVisibleInViewDist;

//    Dictionary<Vector2, TilemapTerrainChunk> terrainChunkDictionary = new Dictionary<Vector2, TilemapTerrainChunk>();
//    List<TilemapTerrainChunk> visibleTerrainChunks = new List<TilemapTerrainChunk>();

//    // Start is called before the first frame update
//    void Start()
//    {
//        terrainManager = GetComponent<TerrainManager>();
//        tileGrid = GetComponent<Grid>();

//        chunksVisibleInViewDist = 1;
//        //chunksVisibleInViewDist = Mathf.RoundToInt(maxViewDist / meshWorldSize);

//        UpdateVisibleChunks();


//        //HeightMap heightMap = HeightMapGenerator.GenerateHeightMap(ChunkSize, ChunkSize, heightMapSettings, sampleCentre);
        


//    }

//    void Update()
//    {

//        viewerPosition = new Vector2(viewer.position.x, viewer.position.y);

//        if ((viewerPositionOld - viewerPosition).sqrMagnitude > sqrViewerMoveThresholdForChunkUpdate)
//        {
//            viewerPositionOld = viewerPosition;
//            UpdateVisibleChunks();
//        }

//        //foreach (TilemapTerrainChunk chunk in terrainChunkDictionary.Values)
//        //{
//        //    DrawBounds(chunk.bounds);
//        //}
//    }

//    void UpdateVisibleChunks()
//    {
//        HashSet<Vector2> alreadyUpdatedChunkCoords = new HashSet<Vector2>();

//        for (int i = visibleTerrainChunks.Count - 1; i >= 0; i--)
//        {
//            alreadyUpdatedChunkCoords.Add(visibleTerrainChunks[i].coord);
//            visibleTerrainChunks[i].UpdateTerrainChunk();
//        }

//        Vector3Int viewerTilePos = tileGrid.WorldToCell(viewerPosition);
//        int currentChunkCoordX = viewerTilePos.x / ChunkSize;
//        int currentChunkCoordY = viewerTilePos.y / ChunkSize;

//        for (int yOffset = -chunksVisibleInViewDist - 1; yOffset <= chunksVisibleInViewDist; yOffset++)
//        {
//            for (int xOffset = -chunksVisibleInViewDist - 1; xOffset <= chunksVisibleInViewDist; xOffset++)
//            {
//                Vector2Int viewedChunkCoord = new Vector2Int(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);

//                if (!alreadyUpdatedChunkCoords.Contains(viewedChunkCoord))
//                {
//                    if (terrainChunkDictionary.ContainsKey(viewedChunkCoord))
//                    {
//                        terrainChunkDictionary[viewedChunkCoord].UpdateTerrainChunk();
//                    }
//                    else
//                    {
//                        TilemapTerrainChunk newChunk = new TilemapTerrainChunk(viewedChunkCoord, tileGrid, terrainTilemap, heightMapSettings, tileDataObjects, this.transform, viewer);
//                        terrainChunkDictionary.Add(viewedChunkCoord, newChunk);
//                        newChunk.onVisibilityChanged += OnTerrainChunkVisibilityChanged;
//                        newChunk.Load();
//                    }
//                }
//            }
//        }
//    }

//    void OnTerrainChunkVisibilityChanged(TilemapTerrainChunk chunk, bool isVisible)
//    {
//        if (isVisible)
//        {
//            visibleTerrainChunks.Add(chunk);
//        }
//        else
//        {
//            visibleTerrainChunks.Remove(chunk);
//        }
//    }

//    void DrawBounds(Bounds b, float delay = 0)
//    {
//        // bottom
//        var p1 = new Vector3(b.min.x, b.min.y, b.min.z);
//        var p2 = new Vector3(b.max.x, b.min.y, b.min.z);
//        var p3 = new Vector3(b.max.x, b.min.y, b.max.z);
//        var p4 = new Vector3(b.min.x, b.min.y, b.max.z);

//        Debug.DrawLine(p1, p2, Color.blue, delay);
//        Debug.DrawLine(p2, p3, Color.red, delay);
//        Debug.DrawLine(p3, p4, Color.yellow, delay);
//        Debug.DrawLine(p4, p1, Color.magenta, delay);

//        // top
//        var p5 = new Vector3(b.min.x, b.max.y, b.min.z);
//        var p6 = new Vector3(b.max.x, b.max.y, b.min.z);
//        var p7 = new Vector3(b.max.x, b.max.y, b.max.z);
//        var p8 = new Vector3(b.min.x, b.max.y, b.max.z);

//        Debug.DrawLine(p5, p6, Color.blue, delay);
//        Debug.DrawLine(p6, p7, Color.red, delay);
//        Debug.DrawLine(p7, p8, Color.yellow, delay);
//        Debug.DrawLine(p8, p5, Color.magenta, delay);

//        // sides
//        Debug.DrawLine(p1, p5, Color.white, delay);
//        Debug.DrawLine(p2, p6, Color.gray, delay);
//        Debug.DrawLine(p3, p7, Color.green, delay);
//        Debug.DrawLine(p4, p8, Color.cyan, delay);
//    }
//}

