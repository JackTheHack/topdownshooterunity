﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantComponent : MonoBehaviour
{

    public PlayerItemUseManager ItemUseManager { get; private set; }
    public FarmerUIManager FarmerUIManager { get; private set; }

    public int currentGrowthStage;
    public int maxGrowthStage;

    public FarmerDateData DatePlanted { get; private set; }

    public WorldItem attachedTo { get; private set; }

    public void Awake()
    {
        ItemUseManager = FindObjectOfType<PlayerItemUseManager>();
        FarmerUIManager = FindObjectOfType<FarmerUIManager>();

        attachedTo = GetComponent<WorldItem>();
    }

    // When initialising the Plant, we work out its current growth stage based 
    // on the time between the date it was planted and the current date.
    public void Initialise(int maxGrowthStage, FarmerDateData datePlanted)
    {
        this.maxGrowthStage = maxGrowthStage;

        PlantData plantData = attachedTo.itemData as PlantData;
        int daysSincePlanted = FarmerDateData.DaysBetween(datePlanted, PersistentDayData.today);
        currentGrowthStage = Mathf.Clamp(daysSincePlanted / plantData.daysToAdvanceStage, 0, maxGrowthStage - 1);

        this.DatePlanted = datePlanted;
        attachedTo.RefreshSprite(plantData.growthStageSprites[currentGrowthStage]);
    }

    public bool FullyGrown
    {
        get
        {
            return currentGrowthStage == maxGrowthStage - 1;
        }
    }

    // Currently Initialise() is the only place the growth stage is set

    // Currently only used in CheatGrowAllPlants()
    public void AdvanceGrowth()
    {
        currentGrowthStage = Mathf.Clamp(currentGrowthStage + 1, 0, maxGrowthStage - 1);

        PlantData plantData = attachedTo.itemData as PlantData;
        attachedTo.RefreshSprite(plantData.growthStageSprites[currentGrowthStage]);
    }

    // Currently unused
    public void SetGrowthStage(int newStage)
    {
        currentGrowthStage = newStage;

        PlantData plantData = attachedTo.itemData as PlantData;
        attachedTo.RefreshSprite(plantData.growthStageSprites[currentGrowthStage]);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
