﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupForEditorStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        SceneLoadData.sceneLoadReason = SceneLoadReason.StartOfGame;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
