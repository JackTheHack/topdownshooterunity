﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Text;

using System;

public class InventorySystem
{
    public InventoryCell[] cells;
    public int hotBarLength { get; private set; } // first N cells are considered part of the hotbar.

    //public Dictionary<UseableItemData, bool> itemCheckDict;

    public int selectedHotbarCellIndex;

    public int Width { get; private set; }
    public int Height { get; private set; }

    public bool belongsToPlayer { get; private set; }

    private const char ITEM_SEPARATOR = '~';
    private const char PART_SEPARATOR = '#';

    public const int STANDARD_WIDTH = 8;
    public const int STANDARD_HEIGHT = 3;

    public event Action<InventoryAction> OnInsert;
    public event Action<InventoryAction, DroppedItem> OnItemPickup;

    public string GenerateSaveString()
    {
        StringBuilder b = new StringBuilder();
        b.AppendFormat("{0}#{1}~", Width, Height);
        for (int i = 0; i < cells.Length; i++)
        {
            if (cells[i].ContainsItem)
            {
                b.AppendFormat("{0}#{1}#{2}~", i, cells[i].containedItem.itemName, cells[i].itemCount);
            }
        }
        if (b.Length > 0)
        {
            b.Remove(b.Length - 1, 1); // Remove trailing separator
        }

        //Debug.Log(b.ToString());
        return b.ToString();
    }

    public static InventorySystem RestoreInventory(string invString, bool isPlayerInventory)
    {

        string[] items = invString.Split(ITEM_SEPARATOR);

        string[] dimensionsString = items[0].Split(PART_SEPARATOR);
        int width = int.Parse(dimensionsString[0]);
        int height = int.Parse(dimensionsString[1]);

        InventorySystem inventory = new InventorySystem(width, height, isPlayerInventory);

        for (int i = 1; i < items.Length; i++)
        {
            string[] parts = items[i].Split(PART_SEPARATOR);

            int cellIndex = int.Parse(parts[0]);
            string itemName = parts[1];
            int itemCount = int.Parse(parts[2]);

            inventory.Insert(cellIndex, ItemDefinitionStore.itemDict[itemName], itemCount, InventoryMoveType.WholeStack);
        }

        return inventory;
    }

    public InventorySystem(bool belongsToPlayer)
    {
        Initialise(STANDARD_WIDTH, STANDARD_HEIGHT, belongsToPlayer);
    }

    public InventorySystem(int width, int height, bool belongsToPlayer)
    {
        Initialise(width, height, belongsToPlayer);
    }

    private void Initialise(int width, int height, bool belongsToPlayer)
    {
        Width = Mathf.Max(1, width);
        Height = Mathf.Max(1, height);

        this.cells = new InventoryCell[width * height];

        for (int i = 0; i < cells.Length; i++)
        {
            cells[i] = new InventoryCell();
        }

        selectedHotbarCellIndex = 0;
        this.hotBarLength = width;

        this.belongsToPlayer = belongsToPlayer;
    }

    public void InsertDroppedItem(UseableItemData itemData, int amountInStack, InventoryMoveType moveType, DroppedItem droppedItem)
    {

        // General insertion of item, can only add to stack or add to fresh slot.
        // can't result in a swap (CAN CURRENTLY DUE TO NEW EVENT BASED IMPLEMENTATION). Intended for use when picking item off the ground
        // or quick moving from chest.


        //bool itemAlreadyInInventory = false;
        //for (int i = 0; i < cells.Length; i++)
        //{
        //    if (cells[i].containsItem && cells[i].containedItem == item && cells[i].itemCount < item.stackSize)
        //    {
        //        itemAlreadyInInventory = true;
        //        break;
        //    }
        //}

        //if (itemAlreadyInInventory)
        //{

        InventoryAction action = new InventoryAction(this, InventoryActionResult.Failure, moveType, -1);

        // Try and add the item to an existing stack in the inventory.
        for (int i = 0; i < cells.Length; i++)
        {
            if (cells[i].ContainsItem && cells[i].containedItem.name == itemData.name && cells[i].itemCount + amountInStack <= cells[i].containedItem.maxStackSize)
            {
                cells[i].itemCount += amountInStack;
                action.result = InventoryActionResult.Merge;
                action.cellIndex = i;
                NotifyInsertionListeners(action, droppedItem);
                return;
            }
            //else if (cells[i].itemCount >= cells[i].containedItem.stackSize)
            //{
            //    return InventoryActionResult.Failure;
            //}
            //else if (cells[i].containsItem)
            //{
            //    return InventoryActionResult.Swap;
            //}
        }

        // Put item in a fresh slot
        for (int i = 0; i < cells.Length; i++)
        {
            if (!cells[i].ContainsItem)
            {
                cells[i].containedItem = itemData;
                cells[i].itemCount += amountInStack;
                action.result = InventoryActionResult.Insertion;
                action.cellIndex = i;
                NotifyInsertionListeners(action, droppedItem);
                return;
            }
        }

        
    }

    // Insertion with row and column specified, can result in add to fresh slot, 
    // add to stack, or notify caller about swap with held item.
    public void Insert(int row, int col, UseableItemData itemData, int amountInStack, InventoryMoveType moveType)
    {

        int cellIndex = (row * Width) + col;

        InventoryAction action = new InventoryAction(this, InventoryActionResult.Failure, moveType, cellIndex);
        InventoryCell cell = cells[cellIndex];
        if (cell.ContainsItem)
        {
            if (cell.containedItem.name == itemData.name && cell.itemCount + amountInStack <= cell.containedItem.maxStackSize)
            {
                cell.itemCount += amountInStack;
                action.result = InventoryActionResult.Merge;
            } else if (cell.containedItem.name != itemData.name)
            {
                action.result = InventoryActionResult.Swap;
            }
        } else
        {
            cell.containedItem = itemData;
            cell.itemCount += amountInStack;
            action.result = InventoryActionResult.Insertion;
        }

        NotifyInsertionListeners(action);
    }

    // Insertion to raw cellIndex, can result in add to fresh slot, 
    // add to stack, or notify caller about swap with held item.
    public void Insert(int cellIndex, UseableItemData itemData, InventoryMoveType moveType)
    {
        Insert(cellIndex, itemData, 1, moveType);
    }

    public void Insert(int cellIndex, UseableItemData itemData, int amountInStack, InventoryMoveType moveType)
    {
        InventoryAction action = new InventoryAction(this, InventoryActionResult.Failure, moveType, cellIndex);
        InventoryCell cell = cells[cellIndex];
        if (cell.ContainsItem)
        {
            if (SameItem(cellIndex, itemData) && cell.itemCount + amountInStack <= cell.containedItem.maxStackSize)
            {
                cell.itemCount += amountInStack;
                action.result = InventoryActionResult.Merge;
            }
            else if (cell.containedItem.name != itemData.name)
            {
                action.result = InventoryActionResult.Swap;
            }
            
        }
        else
        {
            cell.containedItem = itemData;
            //cell.itemCount += amountInStack; // this will be incorrect if the itemCount of an empty cell is ever NOT ZERO.
            cell.itemCount = amountInStack; // This way ensures that the itemCount is correct, but may cover up a bug where itemCounts of empty cells were not zero.
            action.result = InventoryActionResult.Insertion;
        }

        NotifyInsertionListeners(action);
    }

    private void NotifyInsertionListeners(InventoryAction action)
    {
        OnInsert?.Invoke(action);
    }

    private void NotifyInsertionListeners(InventoryAction action, DroppedItem droppedItem)
    {
        OnInsert?.Invoke(action);
        OnItemPickup?.Invoke(action, droppedItem);
    }

    public InventoryActionResult RequestRemoval(int cellID, out UseableItemData removedItem, out int amountRemoved, InventoryMoveType moveType)
    {
        removedItem = null;
        amountRemoved = 0;

        if (cellID >= 0 && cellID < cells.Length)
        {
            if (cells[cellID].ContainsItem)
            {
                removedItem = cells[cellID].containedItem;
                if (moveType == InventoryMoveType.WholeStack)
                {
                    amountRemoved = cells[cellID].RemoveItem();
                } else if (moveType == InventoryMoveType.SingleItem)
                {
                    amountRemoved = cells[cellID].RemoveOneItem();
                }

                return InventoryActionResult.Removal;
            }
        }
        return InventoryActionResult.Failure;
    }

    public InventoryActionResult RequestEquippedItemConsumption(InventoryMoveType moveType)
    {
        UseableItemData itemData;
        int amountRemoved = 0;
        InventoryActionResult result = RequestRemoval(selectedHotbarCellIndex, out itemData, out amountRemoved, moveType);

        if (result == InventoryActionResult.Failure)
        {
            Debug.Log("Failed to consume equipped item");
        }

        return result;
    }

    public UseableItemData GetSelectedHotbarItem()
    {
        return cells[selectedHotbarCellIndex].containedItem;
    }

    public void ChangeSelectedHotbarCell(int newIndex)
    {
        selectedHotbarCellIndex = Mathf.Clamp(newIndex, 0, hotBarLength - 1);
    }

    public bool SameItem(int cellIndex, UseableItemData itemData)
    {
        return cells[cellIndex].containedItem.name == itemData.name;
    }

}

public enum InventoryActionResult { Insertion, Removal, Swap, Merge, Failure }


public struct InventoryAction
{
    public InventorySystem inventory;
    public InventoryActionResult result;
    public InventoryMoveType moveType;
    public int cellIndex;

    public InventoryAction(InventorySystem inventory, InventoryActionResult result, InventoryMoveType moveType, int cellIndex)
    {
        this.inventory = inventory;
        this.result = result;
        this.moveType = moveType;
        this.cellIndex = cellIndex;
    }
}


public class InventoryCell
{
    public UseableItemData containedItem;
    public int itemCount;
    public bool ContainsItem {
        get
        {
            return containedItem != null;
        }
    }

    public int RemoveItem()
    {
        containedItem = null;
        int amountRemoved = itemCount;
        itemCount = 0;

        return amountRemoved;
    }

    public int RemoveOneItem()
    {
        if (itemCount > 0)
        {
            itemCount--;
            if (itemCount == 0)
            {
                containedItem = null;
            }
            return 1;
        }
        return 0;
    }
}
