﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DoorManager
{

    // Will be assigned with the unique ID of the door the player just went through.
    // Unique IDs per pair of doors.
    // When player is spawned, spawn at position of door with matching id.
    public static string currentDoorTag; 

}
