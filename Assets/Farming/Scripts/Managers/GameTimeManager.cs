﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;

public class GameTimeManager : MonoBehaviour
{

    public FarmerDateData DateData { get; private set; }

    public TextMeshProUGUI dateText;
    public TextMeshProUGUI timeText;

    public event Action OnEndDay;

    private TerrainManager terrainManager;
    private FarmerStatsManager statsManager;
    private PlayerItemUseManager itemUseManager;

    private LevelChangerUI levelChanger;

    public bool LogEndOfDaySales;

    public LightingData dayNightGradient;

    public Light sun;

    private bool usingSun;

    [Range(0.1f, 20)]
    public float timeScale;

    private void OnValidate()
    {
        OneOverTimeScale = 1 / timeScale;
        ResetTimeScale();
    }

    public float TimeScale
    {
        get
        {
            return timeScale;
        }
        set
        {
            timeScale = value;
            OneOverTimeScale = 1 / timeScale;
            ResetTimeScale();
        }
    }

    private float OneOverTimeScale;

    private void Awake()
    {
        OneOverTimeScale = 1 / TimeScale;

        terrainManager = FindObjectOfType<TerrainManager>();
        statsManager = FindObjectOfType<FarmerStatsManager>();
        itemUseManager = FindObjectOfType<PlayerItemUseManager>();
        levelChanger = FindObjectOfType<LevelChangerUI>();

        usingSun = sun != null;


        if (SceneLoadData.sceneLoadReason == SceneLoadReason.NewDay || SceneLoadData.sceneLoadReason == SceneLoadReason.StartOfGame)
        {
            SceneLoadData.sceneLoadReason = SceneLoadReason.Invalid;
            StartDay();
        }
        else
        {
            DateData = PersistentDayData.lastTravelTime;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

        //FarmerDateData beforeTest1 = new FarmerDateData(5, 2, 1);
        //FarmerDateData afterTest1 = new FarmerDateData(7, 2, 1);

        //FarmerDateData beforeTest2 = new FarmerDateData(1, 2, 1);
        //FarmerDateData afterTest2 = new FarmerDateData(2, 3, 1);

        //FarmerDateData beforeTest3 = new FarmerDateData(2, 2, 1);
        //FarmerDateData afterTest3 = new FarmerDateData(2, 2, 2);




        // The day is advanced when GameTimeManager.Awake() is called, before any Start() methods
        // The plants are only restored when TerrainManager.Start() is called, so this needs to come after
        //PlantComponent[] plants = FindObjectsOfType<PlantComponent>();
        //for (int i = 0; i < plants.Length; i++)
        //{
        //    int daysSincePlanted = FarmerDateData.DaysBetween(plants[i].DatePlanted, PersistentDayData.today);
        //    PlantData plantData = plants[i].attachedTo.itemData as PlantData;

        //    Debug.Log(daysSincePlanted + "  " + plantData.daysToAdvanceStage + "   " + plants[i].maxGrowthStage);
        //    plants[i].SetGrowthStage(Mathf.Clamp(daysSincePlanted / plantData.daysToAdvanceStage, 0, plants[i].maxGrowthStage));
        //}

        RefreshText();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            EndDay();
            //dateData.AdvanceTime();
            //RefreshText();
        }

    }

    void StartDay()
    {
        DateData = PersistentDayData.AdvanceFromYesterday();

        InvokeRepeating("PassTimeAndUpdateUI", 5 * OneOverTimeScale, 5 * OneOverTimeScale);

        if (usingSun)
        {
            float fractionThroughDay = Mathf.InverseLerp(0, FarmerDateData.dayLength, DateData.hour + Mathf.InverseLerp(0, 6, DateData.hourProgress));
            sun.color = dayNightGradient.gradient.Evaluate(fractionThroughDay);
        }
    }

    public void EndDay()
    {

        OnEndDay?.Invoke();

        CancelInvoke("PassTimeAndUpdateUI");

        PersistentDayData.yesterday = DateData;

        if (LogEndOfDaySales)
        {
            Debug.Log("Sold:\n");
            for (int i = 0; i < PersistentDayData.soldItems.Count; i++)
            {
                SellRecord sold = PersistentDayData.soldItems[i];
                Debug.Log(sold.itemData.itemName + ":" + sold.amount + "\n");
            }
        }

        // Making money here
        for (int i = 0; i < PersistentDayData.soldItems.Count; i++)
        {
            SellRecord sold = PersistentDayData.soldItems[i];
            statsManager.AddMoney(sold.itemData.baseSellValue * sold.amount /*any modifiers could go here*/);
        }

        // Need to save the money amount/player inventory between scenes next

        terrainManager.SaveForSceneTransition();
        statsManager.SaveForSceneTransition();
        //itemUseManager.SaveForSceneTransition();

        //SceneLoadData.sceneLoadReason = SceneLoadReason.NewDay;
        levelChanger.FadeToScene("NextDayScene");

    }

    void PassTimeAndUpdateUI()
    {
        DateData.AdvanceTime();
        RefreshText();

        if (usingSun)
        {
            float fractionThroughDay = Mathf.InverseLerp(0, FarmerDateData.dayLength, DateData.hour + Mathf.InverseLerp(0, 6, DateData.hourProgress));
            sun.color = dayNightGradient.gradient.Evaluate(fractionThroughDay);
        }
    }

    void ResetTimeScale()
    {
        if (UnityEditor.EditorApplication.isPlaying)
        {
            CancelInvoke("PassTimeAndUpdateUI");
            InvokeRepeating("PassTimeAndUpdateUI", 5 * OneOverTimeScale, 5 * OneOverTimeScale);
        }
    }
    
    void RefreshText()
    {
        if (DateData != null)
        {
            dateText.SetText(DateData.GetDateString());
            timeText.SetText(DateData.GetTimeString());
        }
    }

  
}

public class FarmerDateData
{
    //[Range(1, 6)]
    public int hourProgress;
    //[Range(1, 24)]
    public int hour;
    //[Range(1, 22)]
    public int day;
    //[Range(1, 4)]
    public int season;

    public int year;

    public const int realHourLength = 60; // minutes
    public const int hourLength = 6; // progress increments
    public const int dayLength = 24; // hours
    public const int seasonLength = 22; // days
    public const int yearLength = 4; // seasons

    public const int dayStartHour = 6;

    private const int wakeUpHourPenalty = 2;

    public FarmerDateData()
    {
        hourProgress = 0;
        hour = dayStartHour;
        day = 1;
        season = 1;
        year = 1;
    }

    public FarmerDateData(int day, int season, int year)
    {
        hourProgress = 0;
        hour = 0;
        this.day = day;
        this.season = season;
        this.year = year;
    }

    public void AdvanceTime()
    {
        hourProgress += 1;
        if (hourProgress >= hourLength)
        {
            hourProgress = 0;
            hour += 1;
            if (hour >= dayLength)
            {
                hour = 0;
                AdvanceDay();
                Debug.Log("ITS A NEW DAY");
            }
        }
    }

    // Advances the day simply when the hour reaches dayLength.
    // Call when time passes midnight but the player hasn't gone to bed yet.
    public void AdvanceDay()
    {
        day += 1;
        if (day > seasonLength)
        {
            day = 1;
            season += 1;
            if (season > yearLength)
            {
                season = 1;
                year += 1;
            }
        }
    }

    // Advances the day and also moves the hour to the waking up time.
    // Call this when the player has slept or fainted.
    public void AdvanceDayToDayStartHour()
    {
        if (hour < dayStartHour)
        {
            // Stayed up after midnight
            // Don't go to next day (we're already there). Apply wake up penalty.
            hour = dayStartHour + wakeUpHourPenalty;
            hourProgress = 0;
        } else
        {
            // Need to go to the next day, no wake up penalty.
            AdvanceDay();
            hour = dayStartHour;
            hourProgress = 0;
        }
    }

    public const string DATE_SEPARATOR = "#";
    public string GetSaveString()
    {
        return day.ToString() + DATE_SEPARATOR[0] + season.ToString() + DATE_SEPARATOR[0] + year.ToString();
    }

    public static FarmerDateData ParseDate(string dateString)
    {
        FarmerDateData result = new FarmerDateData();

        string[] parts = dateString.Split(DATE_SEPARATOR[0]);
        if (parts.Length == 3)
        {
            result.day = int.Parse(parts[0]);
            result.season = int.Parse(parts[1]);
            result.year = int.Parse(parts[2]);
        }
        return result;
    }

    public static int DaysBetween(FarmerDateData before, FarmerDateData after)
    {
        FarmerDateData origin = new FarmerDateData(1, 1, 1);

        int daysInYear = yearLength * seasonLength;

        int daysBetweenBeforeAndOrigin = (daysInYear * (before.year - origin.year)) + (seasonLength * (before.season - origin.season)) + (before.day - origin.day);
        int daysBetweenAfterAndOrigin = (daysInYear * (after.year - origin.year)) + (seasonLength * (after.season - origin.season)) + (after.day - origin.day);

        int daysBetweenBeforeAndAfter = (daysInYear * (after.year - before.year)) + (seasonLength * (after.season - before.season)) + (after.day - before.day);

        //Debug.Log(before.GetSaveString() + "  " + after.GetSaveString() + "     " + daysBetweenBeforeAndAfter);

        return daysBetweenBeforeAndAfter;
    }

    public string GetDateString()
    {
        string daySuffix = "";
        if (day == 1 || day == 21)
        {
            daySuffix = "st";
        }
        else if (day == 2 || day == 22)
        {
            daySuffix = "nd";
        }
        else if (day == 3)
        {
            daySuffix = "rd";
        }
        else
        {
            daySuffix = "th";
        }

        string seasonString = "";
        if (season == 1)
        {
            seasonString = "Spring";
        }
        else if (season == 2)
        {
            seasonString = "Summer";
        }
        else if (season == 3)
        {
            seasonString = "Autumn";
        }
        else if (season == 4)
        {
            seasonString = "Winter";
        }

        return day.ToString() + daySuffix + " of " + seasonString + ", (Year " + year + ")";
    }

    public string GetTimeString()
    {
        int minutes = (realHourLength / hourLength) * hourProgress;

        string hourProgressAsMinutes = minutes.ToString("D2");

        return hour.ToString("D2") + ":" + hourProgressAsMinutes;
    }
}
