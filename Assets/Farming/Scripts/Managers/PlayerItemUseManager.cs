﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

using System;

public enum InteractionResult { OpenPlayerInventory, OpenOtherInventory, OpenDualInventory, SpawnedItem, StartDialogue, UpdateQuest, OpenDoor, Failure }

public class InteractionResultData
{
    public InteractionResult result;
    public InventorySystem otherInventory;
    public InteractableFurnitureData furnitureData;

    public InteractionResultData(InteractionResult result)
    {
        this.result = result;
        otherInventory = new InventorySystem(0, 0, false);
    }

    public InteractionResultData(InteractionResult result, InventorySystem otherInventory)
    {
        this.result = result;
        this.otherInventory = otherInventory;
    }

    public InteractionResultData(InteractionResult result, InteractableFurnitureData interactedItem)
    {
        this.result = result;
        this.furnitureData = interactedItem;
    }
}

public class PlayerItemUseManager : MonoBehaviour
{

    public Camera gameCamera;
    public SpriteRenderer tileOutlineSprite;
    public FarmerController currentFarmerInstance;
    public FarmerUIManager farmerUIManager;
    public TerrainManager terrainManager;
    public FarmerStatsManager statsManager;

    public UseableItemData[] testItems;
    bool inventoryOpen = false;

    public InventorySystem playerInventory;
    public const string INVENTORY_SAVE_KEY = "INVENTORY";



    public Queue<DroppedItem> droppedItemQueue = new Queue<DroppedItem>();


    // Start is called before the first frame update
    void Start()
    {

        string invString = PlayerPrefs.GetString(INVENTORY_SAVE_KEY);

        if (invString != "")
        {

            playerInventory = new InventorySystem(true);

            farmerUIManager.InitialisePlayerInventoryUI(playerInventory);

            UseableItemData carrotSeeds = Instantiate(testItems[0]);
            UseableItemData hoe = Instantiate(testItems[1]);
            UseableItemData wateringCan = Instantiate(testItems[2]);
            UseableItemData fence = Instantiate(testItems[3]);
            UseableItemData pickaxe = Instantiate(testItems[4]);
            UseableItemData chest = Instantiate(testItems[5]);

            InventoryMoveType move = InventoryMoveType.WholeStack;

            playerInventory.Insert(0, carrotSeeds, 10, move);
            playerInventory.Insert(1, hoe, move);
            playerInventory.Insert(2, wateringCan, move);
            playerInventory.Insert(3, fence, move);
            playerInventory.Insert(4, pickaxe, move);
            playerInventory.Insert(5, chest, move);

            playerInventory.Insert(9, Instantiate(testItems[0]), move);

            currentFarmerInstance.EquipItem(carrotSeeds);

            //PersistentPlayerData.SetInventoryInitialised();
            PlayerPrefs.SetString(INVENTORY_SAVE_KEY, playerInventory.GenerateSaveString());
        } else
        {
            playerInventory = InventorySystem.RestoreInventory(invString, true);
            farmerUIManager.InitialisePlayerInventoryUI(playerInventory);
            currentFarmerInstance.EquipItem(playerInventory.cells[0].containedItem);

        }

        farmerUIManager.UpdatePlayerInventoryUI(playerInventory);
        farmerUIManager.CloseDualInventory();

        playerInventory.OnInsert += InsertCallback;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            string invString = playerInventory.GenerateSaveString();
            Debug.Log(invString);
        }


        currentFarmerInstance.ClearDodgeSkip(); // Have to call this from here because if we clear it from the farmer controller
        // the farmer controller's Update may have been processed after this thus the code looking for the dodge skip would never
        // see it as true.

        Vector2 mouseScreenPos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int cellPos = terrainManager.tileGrid.WorldToCell(mouseScreenPos);
        
        if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.I))
        {
            ToggleInventoryOpen();
        }

        if (!inventoryOpen && terrainManager.baseMap.HasTile(cellPos))
        {
            tileOutlineSprite.transform.position = (Vector2Int)cellPos * (Vector2)terrainManager.tileGrid.cellSize;
            tileOutlineSprite.enabled = true;

            if (Input.GetMouseButtonDown(0))
            {
                // Left click action
                HandleLeftClickAt(cellPos);
                
            } else if (Input.GetMouseButtonDown(1))
            {
                // Right click action
                HandleRightClickAt(cellPos);
            }
        } else
        {
            tileOutlineSprite.enabled = false;
        }

        if (Input.mouseScrollDelta.y != 0)
        {
            ChangeHotbarCell(playerInventory.selectedHotbarCellIndex - (int)Input.mouseScrollDelta.y);
        }

        HandleNumericKeyInput();

    }

    void HandleLeftClickAt(Vector3Int cellPos)
    {
        if (currentFarmerInstance.heldItem.ReadyForItemUse)
        {

            bool success = false;

            // If the equipped item modifies the tilemap in some way, send to terrainManager.
            if (currentFarmerInstance.heldItem.itemData.ActsOnTilemap())
            {
                success = terrainManager.UseOnTerrain(cellPos, currentFarmerInstance.heldItem.itemData);

            }
            else
            {

            }

            if (success)
            {
                if (currentFarmerInstance.heldItem.itemData.consumedOnUse)
                {
                    playerInventory.RequestEquippedItemConsumption(InventoryMoveType.SingleItem);
                    farmerUIManager.UpdatePlayerInventoryUI(playerInventory);
                }
                statsManager.HandleItemUse(currentFarmerInstance.heldItem);
            }

            bool stillHasItem = CheckEquippedItemStatus();

            if (stillHasItem)
            {
                currentFarmerInstance.heldItem.Use();
            }
        }
    }

    void HandleRightClickAt(Vector3Int cellPos)
    {
        // Only checks the tilemap (plus world items that have been placed)
        InteractionResultData interactionData = terrainManager.InteractWithItemAt(cellPos);

        if (interactionData.result == InteractionResult.OpenDualInventory)
        {
            farmerUIManager.OpenDualInventory(interactionData.otherInventory);
        }

        if (interactionData.result != InteractionResult.Failure)
        {
            currentFarmerInstance.SkipDodgeThisFrame();
        }
    }

    // BEGIN Static object save/load
    // ================================================================================================== //


    // Static editor placed objects only need extra state like inventories or special configurations to
    // be saved

    private Dictionary<Vector3Int, InventorySystem> FindEditorPlacedInventories()
    {
        Dictionary<Vector3Int, InventorySystem> invDict = new Dictionary<Vector3Int, InventorySystem>();
        EditorPlacedItem[] staticObjects = FindObjectsOfType<EditorPlacedItem>();

        for (int i = 0; i < staticObjects.Length; i++)
        {
            StorageComponent store = staticObjects[i].GetComponent<StorageComponent>();
            if (store != null)
            {
                // Item has an inventory
                invDict.Add(terrainManager.tileGrid.WorldToCell(store.transform.position), store.Inventory);
            }
        }
        return invDict;
    }



    public void SaveInventories()
    {

        Dictionary<Vector3Int, InventorySystem> staticInvs = FindEditorPlacedInventories();


        //SavedInv[] savedInvs = new SavedInv[]

        //SavedInv[] savedTiles = new SavedInv[terrainDataDict.Keys.Count];
        //int index = 0;
        //foreach (Vector3Int pos in terrainDataDict.Keys)
        //{
        //    TerrainTileData tileData = terrainDataDict[pos];
        //    savedTiles[index++] = new SavedTile(pos, tileData.type);
        //}

        //var f = File.CreateText(GetCurrentSceneTilemapFilename());
        //for (int i = 0; i < savedTiles.Length; i++)
        //{
        //    f.WriteLine(ParserHelper.SavedTileToString(savedTiles[i]));
        //}
        //f.Close();
    }

    public SavedInv[] ReadInventories()
    {
        List<SavedInv> tiles = new List<SavedInv>();
        //string filename = GetCurrentSceneTilemapFilename();
        //if (File.Exists(filename))
        //{
        //    var sr = File.OpenText(filename);
        //    string line;
        //    while ((line = sr.ReadLine()) != null)
        //    {
        //        tiles.Add(ParserHelper.ParseSavedTile(line));
        //    }
        //    sr.Close();
        //}
        //else
        //{
        //    Debug.Log("Could not open the file " + filename + " for reading.");
        //}

        return tiles.ToArray();
    }

    public struct SavedInv
    {
        public Vector3Int pos;
        public TerrainType terrainType;

        public SavedInv(Vector3Int pos, TerrainType terrainType)
        {
            this.pos = pos;
            this.terrainType = terrainType;
        }
    }


    // END Static object save/load
    // ================================================================================================== //


    void ToggleInventoryOpen()
    {
        inventoryOpen = farmerUIManager.ToggleInventories();
    }

    // When right clicking
    public void RequestSwapOrTakeOne(InventorySystem inventory, int cellIndex)
    {
        if (inventory.cells[cellIndex].ContainsItem && farmerUIManager.HoldingItem)
        {
            if (inventory.SameItem(cellIndex, farmerUIManager.currentlyHeldItemData))
            {
                RequestInventoryRemoval(inventory, cellIndex, InventoryMoveType.SingleItem);
            }
            else
            {
                SwapItemWithHeldItem(inventory, cellIndex);
            }
        }
        else if (inventory.cells[cellIndex].ContainsItem)
        {
            RequestInventoryRemoval(inventory, cellIndex, InventoryMoveType.SingleItem);
        }
        else if (farmerUIManager.HoldingItem)
        {
            RequestInventoryInsertion(inventory, cellIndex, InventoryMoveType.SingleItem);
        }
    }

    public void RequestInventoryInsertion(DroppedItem item)
    {

        //droppedItemQueue.Enqueue(item);

        playerInventory.OnItemPickup += CheckToDestroyDroppedItem;

        playerInventory.InsertDroppedItem(item.itemData as UseableItemData, item.NumberHeld, InventoryMoveType.WholeStack, item);
        // event has already been invoked by this point since Insert is actually "returning" immediately
        farmerUIManager.UpdatePlayerInventoryUI(playerInventory);
        CheckEquippedItemStatus();

        // give a special movetype/new boolean/enum for picking up items from ground.
        // this would be accessible in the InsertCallback() and allow for conditionally Invoking the
        // OnItemPickup event.
    }

    public void CheckToDestroyDroppedItem(InventoryAction action, DroppedItem item)
    {
        if (action.result != InventoryActionResult.Failure)
        {
            Destroy(item.gameObject);
            currentFarmerInstance.NotifyDroppedItemDestroyed(item);
        }
        return;
    }

    public void RequestInventoryInsertion(InventorySystem inventory, int cellIndex, InventoryMoveType moveType)
    {
        if (farmerUIManager.HoldingItem)
        {
            if (cellIndex >= 0 && cellIndex < inventory.cells.Length)
            {
                int amountToInsert = moveType == InventoryMoveType.WholeStack ? farmerUIManager.currentlyHeldItemGameobject.NumberHeld : 1;
                inventory.Insert(cellIndex, farmerUIManager.currentlyHeldItemData, amountToInsert, moveType);
            }
        }
    }

    public void InsertCallback(InventoryAction invAction)
    {
        if (invAction.result == InventoryActionResult.Swap)
        {
            SwapItemWithHeldItem(invAction.inventory, invAction.cellIndex);
        }
        else if (farmerUIManager.HoldingItem && (invAction.result == InventoryActionResult.Merge || invAction.result == InventoryActionResult.Insertion))
        {

            // If a whole stack move, then delete the held item.
            // If a single item move, then check if no items are left.
            if (invAction.moveType == InventoryMoveType.WholeStack)
            {
                farmerUIManager.DestroyHeldUIItem();
            }
            else if (invAction.moveType == InventoryMoveType.SingleItem)
            {
                farmerUIManager.currentlyHeldItemGameobject.NumberHeld -= 1;
                if (farmerUIManager.currentlyHeldItemGameobject.NumberHeld == 0)
                {
                    farmerUIManager.DestroyHeldUIItem();
                }
            }
        }

        UpdateUIAndCheckEquipStatus(invAction.inventory);
    }

    public void UpdateUIAndCheckEquipStatus(InventorySystem inventory)
    {
        farmerUIManager.UpdateAppropriateInventoryPanel(inventory);
        CheckEquippedItemStatus();
    }

    public void RequestInventoryRemoval(InventorySystem inventory, int cellIndex, InventoryMoveType moveType)
    {
        UseableItemData potentialRemovedItem;
        int removedStackSize = 0;
        InventoryActionResult result = inventory.RequestRemoval(cellIndex, out potentialRemovedItem, out removedStackSize, moveType);
        if (removedStackSize > 0)
        {
            if (farmerUIManager.HoldingItem)
            {
                farmerUIManager.currentlyHeldItemGameobject.NumberHeld += removedStackSize;
            } else
            {
                farmerUIManager.CreateAndAssignHeldUIItem(potentialRemovedItem, removedStackSize);
            }

            farmerUIManager.UpdateAppropriateInventoryPanel(inventory);
            CheckEquippedItemStatus();
        }
    }

    void SwapItemWithHeldItem(InventorySystem inventory, int cellIndex)
    {
        Debug.Log("swap");

        // Remove the item in the cell and put it into a new held item without assigning it yet.
        UseableItemData removedItem;
        int amountRemoved = 0;
        InventoryActionResult result = inventory.RequestRemoval(cellIndex, out removedItem, out amountRemoved, InventoryMoveType.WholeStack);
        UIHeldItem newHeldItem = farmerUIManager.CreateHeldUIItem(removedItem, amountRemoved);

        // Insert the currently held item, this will destroy it and fill the cell.
        RequestInventoryInsertion(inventory, cellIndex, InventoryMoveType.WholeStack);

        // Set the held item to the created held item from the cell's original contents.
        farmerUIManager.SetHeldItem(newHeldItem, removedItem);
    }

    public void DropHeldItem()
    {
        if (farmerUIManager.HoldingItem)
        {
            terrainManager.CreateDroppedItemAt(currentFarmerInstance.transform.position,
                farmerUIManager.currentlyHeldItemGameobject.itemData,
                farmerUIManager.currentlyHeldItemGameobject.NumberHeld);

            farmerUIManager.DestroyHeldUIItem();
        }
    }

    void ChangeHotbarCell(int newIndex)
    {
        playerInventory.ChangeSelectedHotbarCell(newIndex);
        farmerUIManager.UpdatePlayerInventoryUI(playerInventory);

        CheckEquippedItemStatus();
    }

    bool CheckEquippedItemStatus()
    {
        UseableItemData itemInNewCell = playerInventory.GetSelectedHotbarItem();
        if (itemInNewCell != null)
        {
            currentFarmerInstance.EquipItem(itemInNewCell);
            return true;
        }
        else
        {
            currentFarmerInstance.UnequipItem();
            return false;
        }
    }

    // Call from Update
    void HandleNumericKeyInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangeHotbarCell(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangeHotbarCell(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangeHotbarCell(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ChangeHotbarCell(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ChangeHotbarCell(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            ChangeHotbarCell(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            ChangeHotbarCell(6);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            ChangeHotbarCell(7);
        }
    }
}
