﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDefinitionStore : MonoBehaviour
{

    // Note - Need one of these in any scene you want to start fromsddddddd

    public static Dictionary<string, ItemData> allItemsDict = new Dictionary<string, ItemData>();
    public static Dictionary<string, UseableItemData> itemDict = new Dictionary<string, UseableItemData>();

    public bool initialLoad;
    public ItemData[] itemsToLoad;

    // Start is called before the first frame update
    void Awake()
    {
        if (itemDict.Count == 0)
        {
            UseableItemData[] allUseableItems = Resources.FindObjectsOfTypeAll<UseableItemData>();

            for (int i = 0; i < allUseableItems.Length; i++)
            {
                itemDict.Add(allUseableItems[i].itemName, allUseableItems[i]);
            }
        }

        if (allItemsDict.Count == 0)
        {
            ItemData[] allItems = Resources.FindObjectsOfTypeAll<ItemData>();
            for (int i = 0; i < allItems.Length; i++)
            {
                //Debug.Log(allItems[i].itemName);
                allItemsDict.Add(allItems[i].itemName, allItems[i]);
            }
        }

        if (initialLoad)
        {
            FindObjectOfType<LevelChangerUI>().FadeToScene("MainMenu");
        }
    }

}
