﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FarmerUIManager : MonoBehaviour
{


    public UIHeldItem heldItemPrefab;
    public bool HoldingItem
    {
        get
        {
            return currentlyHeldItemData != null;
        }
    }
    public UIHeldItem currentlyHeldItemGameobject { get; private set; }
    public UseableItemData currentlyHeldItemData { get; private set; }

    public void SetHeldItem(UIHeldItem heldItemGameObject, UseableItemData itemData)
    {
        currentlyHeldItemGameobject = heldItemGameObject;
        currentlyHeldItemData = itemData;
    }

    public TextMeshProUGUI moneyText;
    public Image HealthBar;
    public Image EnergyBar;

    HotbarUI hotbarUI;
    public InventoryUI playerInventoryUI;
    public InventoryUI otherInventoryUI;

    public PlayerItemUseManager playerItemUseManager;
    private FarmerStatsManager statsManager;

    private const float healthBarWaveOffsetMin = 0.2f;
    private const float healthBarWaveOffsetMax = 0.91f;

    // Start is called before the first frame update
    void Awake()
    {

        hotbarUI = GetComponentInChildren<HotbarUI>();


        statsManager = FindObjectOfType<FarmerStatsManager>();
        statsManager.OnMoneyChanged += UpdateMoneyText;
        statsManager.energy.OnChanged += UpdateEnergyBar;
        statsManager.health.OnChanged += UpdateHealthBar;
    }

    private void Start()
    {

        // Better to just set the values manually rather than
        // running these which runs the coroutine uneccesarily.
        // UpdateEnergyBar(statsManager.energy.Current, statsManager.energy.Max);
        // UpdateHealthBar(statsManager.health.Current, statsManager.health.Max);

        EnergyBar.fillAmount = statsManager.energy.Current / (float)statsManager.energy.Max;
        float energyWaveOffsetEndValue = Mathf.Lerp(healthBarWaveOffsetMin, healthBarWaveOffsetMax, 1 - (EnergyBar.fillAmount - 0.05f));
        EnergyBar.material.SetFloat("_waveOffsetY", energyWaveOffsetEndValue);

        HealthBar.fillAmount = statsManager.health.Current / (float)statsManager.health.Max;
        float healthWaveOffsetEndValue = Mathf.Lerp(healthBarWaveOffsetMin, healthBarWaveOffsetMax, 1 - (HealthBar.fillAmount - 0.05f));
        HealthBar.material.SetFloat("_waveOffsetY", healthWaveOffsetEndValue);
    }

    // Update is called once per frame
    void Update()
    {


    }


    // =========================== //
    // Manage inventory open/closed
    public bool ToggleInventories()
    {
        otherInventoryUI.TearDown();
        otherInventoryUI.gameObject.SetActive(false);
        return playerInventoryUI.ToggleMainPanelVisibility();
    }

    public void OpenDualInventory(InventorySystem otherInventory)
    {
        otherInventoryUI.Init(otherInventory);
        otherInventoryUI.UpdateInventoryUI(otherInventory);

        playerInventoryUI.MakeVisible();
        otherInventoryUI.MakeVisible();
    }

    public void CloseDualInventory()
    {
        otherInventoryUI.TearDown();

        playerInventoryUI.MakeInvisible();
        otherInventoryUI.MakeInvisible();
    }

    public void CreateAndAssignHeldUIItem(UseableItemData itemData, int removedStackSize)
    {
        currentlyHeldItemGameobject = Instantiate(heldItemPrefab, Input.mousePosition, Quaternion.identity);
        currentlyHeldItemGameobject.ChangeItemData(itemData);
        currentlyHeldItemGameobject.NumberHeld = removedStackSize;
        currentlyHeldItemData = itemData;
        //heldItem.transform.localPosition = inventoryUI.uiCells[cellIndex].transform.position;
    }

    public UIHeldItem CreateHeldUIItem(UseableItemData itemData, int removedStackSize)
    {
        UIHeldItem heldItem = Instantiate(heldItemPrefab, Input.mousePosition, Quaternion.identity);
        heldItem.ChangeItemData(itemData);
        heldItem.NumberHeld = removedStackSize;

        return heldItem;
    }

    public void DestroyHeldUIItem()
    {
        Destroy(currentlyHeldItemGameobject.gameObject);
        currentlyHeldItemData = null;
    }

    public void InitialisePlayerInventoryUI(InventorySystem inventory)
    {
        playerInventoryUI.Init(inventory);
    }

    public void UpdatePlayerInventoryUI(InventorySystem inventory)
    {
        hotbarUI.UpdateHotbarUI(inventory);
        playerInventoryUI.UpdateInventoryUI(inventory);
    }

    public void UpdateOtherInventoryUI(InventorySystem inventory)
    {
        otherInventoryUI.UpdateInventoryUI(inventory);
    }

    public void UpdateAppropriateInventoryPanel(InventorySystem inventory)
    {
        if (inventory.belongsToPlayer)
        {
            UpdatePlayerInventoryUI(inventory);
        } else
        {
            UpdateOtherInventoryUI(inventory);
        }
    }


    public void UpdateMoneyText(int newAmount)
    {
        moneyText.SetText(newAmount.ToString());
    }

    public void UpdateHealthBar(int current, int max)
    {
        float waveOffsetEndValue = Mathf.Lerp(healthBarWaveOffsetMin, healthBarWaveOffsetMax, 1 - (HealthBar.fillAmount - 0.05f));
        StartCoroutine(RangeStatSmoothUpdate(HealthBar, current / (float)max, waveOffsetEndValue, 0.5f));
    }

    public void UpdateEnergyBar(int current, int max)
    {
        float waveOffsetEndValue = Mathf.Lerp(healthBarWaveOffsetMin, healthBarWaveOffsetMax, 1 - (EnergyBar.fillAmount - 0.05f));
        StartCoroutine(RangeStatSmoothUpdate(EnergyBar, current / (float)max, waveOffsetEndValue, 0.5f));
    }

    IEnumerator RangeStatSmoothUpdate(Image bar, float endValue, float waveOffsetEndValue, float time)
    {
        float elapsedTime = 0;
        float startValue = bar.fillAmount;
        float waveOffsetStartValue = bar.material.GetFloat("_waveOffsetY");

        while (elapsedTime < time)
        {
            float t = (elapsedTime / time);
            t = t * t * (3f - 2f * t); // smoothstep
            bar.fillAmount = Mathf.Lerp(startValue, endValue, t);

            bar.material.SetFloat("_waveOffsetY", Mathf.Lerp(waveOffsetStartValue, waveOffsetEndValue, t));

            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    public void PickItemFromCell(InventorySystem inventory, int cellId, InventoryMoveType moveType)
    {
        playerItemUseManager.RequestInventoryRemoval(inventory, cellId, moveType);
    }

    public void PlaceItemInCell(InventorySystem inventory, int cellId, InventoryMoveType moveType)
    {
        playerItemUseManager.RequestInventoryInsertion(inventory, cellId, moveType);
    }

    public void SwapOrTakeOne(InventorySystem inventory, int cellId)
    {
        playerItemUseManager.RequestSwapOrTakeOne(inventory, cellId);
    }

}

public enum InventoryMoveType { WholeStack, SingleItem }
