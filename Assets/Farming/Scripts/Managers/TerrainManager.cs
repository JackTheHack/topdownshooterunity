﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class TerrainManager : MonoBehaviour
{

    const string TILE_SAVE_FILENAME = "Resources/TileData_";
    const string WORLD_ITEMS_SAVE_FILENAME = "Resources/WorldItems_";

    [HideInInspector]
    public Grid tileGrid;
    public Tilemap collisionMap;
    public Tilemap baseMap;
    public Tilemap overlayMap;

    public WorldItem worldItemPrefab;
    public DroppedItem droppedItemPrefab;

    public TerrainTileData[] tileDataObjects;
    //public TerrainType[] terrainTypes;

    // terrainDataDict holds information about base tiles such as dirt/grass/water
    // tileTypeDict links terrain type e.g. dirt to tile data
    public Dictionary<Vector3Int, TerrainTileData> terrainDataDict = new Dictionary<Vector3Int, TerrainTileData>();
    Dictionary<TerrainType, TerrainTileData> tileTypeDict = new Dictionary<TerrainType, TerrainTileData>();


    // worldItemsDict holds positions of world items that are implemented as sprites, e.g. plants.
    // worldItemsDict uses string keys to support taking up multiple tiles, the keys are made of concatenated Vector3Ints
    // worldTilesDict holds positions of items that are implemented as tiles, e.g. fences.
    Dictionary<string, WorldItem> worldItemsDict = new Dictionary<string, WorldItem>();
    Dictionary<Vector3Int, ItemData> worldTilesDict = new Dictionary<Vector3Int, ItemData>();
    
    // Maps single tiles to the position keys of their multiblock structures
    public Dictionary<Vector3Int, string> multiTileMap = new Dictionary<Vector3Int, string>();

    GameObject worldItemsParent;


    public T GetEnumValue<T>(object o)
    {
        T enumVal = (T)Enum.Parse(typeof(T), o.ToString());
        return enumVal;
    }


    // BEGIN Tilemap save/load
    // ================================================================================================== //

    public void SaveTilemap()
    {
        SavedTile[] savedTiles = new SavedTile[terrainDataDict.Keys.Count];
        int index = 0;
        foreach (Vector3Int pos in terrainDataDict.Keys)
        {
            TerrainTileData tileData = terrainDataDict[pos];
            savedTiles[index++] = new SavedTile(pos, tileData.type);
        }

        ParserHelper.WriteSavedToString<SavedTile>(GetCurrentSceneFilename(TILE_SAVE_FILENAME), savedTiles, ParserHelper.SavedTileToString);
    }


    public static string GetCurrentSceneFilename(string prefix)
    {
        return prefix + SceneManager.GetActiveScene().name + ".txt";
    }

    public struct SavedTile
    {
        public Vector3Int pos;
        public TerrainType terrainType;

        public SavedTile(Vector3Int pos, TerrainType terrainType)
        {
            this.pos = pos;
            this.terrainType = terrainType;
        }
    }

    public bool SavedTilemapExists()
    {
        return File.Exists(GetCurrentSceneFilename(TILE_SAVE_FILENAME));
    }

    public bool SavedWorldItemsExists()
    {
        return File.Exists(GetCurrentSceneFilename(WORLD_ITEMS_SAVE_FILENAME));
    }

    private Dictionary<Vector3Int, TerrainTileData> RestoreTilemapFromSaved(SavedTile[] saved)
    {
        Dictionary<Vector3Int, TerrainTileData> terrainDict = new Dictionary<Vector3Int, TerrainTileData>();
        for (int i = 0; i < saved.Length; i++)
        {
            TerrainType type = saved[i].terrainType;
            terrainDict.Add(saved[i].pos, tileTypeDict[type]);
            GetAppropriateTilemap(type).SetTile(saved[i].pos, tileTypeDict[type].tile);
        }

        return terrainDict;
    }

    private Tilemap GetAppropriateTilemap(TerrainType terrainType)
    {
        switch (terrainType)
        {
            case TerrainType.Dirt:
                return baseMap;
            case TerrainType.Grass:
                return baseMap;
            case TerrainType.Rock:
                return baseMap;
            case TerrainType.Tilled:
                return overlayMap;
            case TerrainType.Water:
                return baseMap;
            case TerrainType.Watered:
                return overlayMap;
            default:
                return baseMap;
        }
    }

    private Dictionary<Vector3Int, TerrainTileData> GenerateDataDictFromSceneData()
    {
        Dictionary<Vector3Int, TerrainTileData> terrainDict = new Dictionary<Vector3Int, TerrainTileData>();
        for (int y = -baseMap.size.y; y < baseMap.size.y; y++)
        {
            for (int x = -baseMap.size.x; x < baseMap.size.x; x++)
            {
                for (int i = 0; i < tileDataObjects.Length; i++)
                {
                    Vector3Int pos = new Vector3Int(x, y, 0);
                    TileBase tile = baseMap.GetTile(pos);
                    if (tile != null && tileDataObjects[i].tile == tile)
                    {
                        terrainDict.Add(pos, tileDataObjects[i]);
                    }
                }
            }
        }
        return terrainDict;
    }

    // END Tilemap save/load
    // ================================================================================================== //

    // Currently can't handle saving multi-tile structures since they take up multiple entries in the worldItemsDict
    private void SaveWorldItems()
    {
        List<SavedWorldItem> savedItems = new List<SavedWorldItem>();

        // save worlditems that are implemented as GameObjects
        foreach (var entry in worldItemsDict)
        {
            savedItems.Add(new SavedWorldItem(entry.Key, entry.Value));
        }

        // Save worlditems that are implemented as tiles
        foreach (var entry in worldTilesDict)
        {
            savedItems.Add(new SavedWorldItem(entry.Key.ToString(), new Vector3Int[] { entry.Key }, entry.Value));
        }

        ParserHelper.WriteSavedToString<SavedWorldItem>(GetCurrentSceneFilename(WORLD_ITEMS_SAVE_FILENAME), savedItems.ToArray(), ParserHelper.SavedWorldItemToString);
    }

    // The methods for each item type will handle adding the items they create to the appropriate dictionary
    private void RestoreWorldItemsFromSaved(SavedWorldItem[] saved)
    {
        for (int i = 0; i < saved.Length; i++)
        {
            Vector3Int[] positions = saved[i].positions;
            ItemData itemData = saved[i].itemData;

            WorldItem editorPlacedItem;
            ItemType type = saved[i].itemData.ItemType;
            switch (type)
            {
                case ItemType.Furniture:

                    FurnitureData furnData = itemData as FurnitureData;
                    if (furnData.UsesTile)
                    {
                        CreateTileFurnitureItem(positions[0], furnData);
                    } else
                    {
                        if (worldItemsDict.TryGetValue(saved[i].key, out editorPlacedItem))
                        {
                            AssignDataToExistingWorldItem(editorPlacedItem, furnData.hasCollision, null);
                        }
                        else
                        {
                            CreateWorldItem(positions, itemData, furnData.hasCollision);
                        }
                    }
                    break;
                case ItemType.InteractableFurniture:
                    Debug.LogError("NOT IMPLEMENTED - RESTORE INTERACTABLE FURNITURE");
                    break;
                case ItemType.Storage:
                    StorageFurnitureData storageData = itemData as StorageFurnitureData;

                    // Assuming that extra data for storage is wholly the inventory
                    InventorySystem inv = InventorySystem.RestoreInventory(saved[i].extraData, false);

                    if (worldItemsDict.TryGetValue(saved[i].key, out editorPlacedItem))
                    {
                        StaticObjectData staticObjectData = itemData as StaticObjectData;
                        AssignDataToExistingWorldItem(editorPlacedItem, storageData.hasCollision, inv);
                    } else
                    {
                        CreateWorldItem(positions, itemData, storageData.hasCollision, inv);
                    }

                    break;
                case ItemType.Seed:

                    // NOTE plants can only take one tile
                    CreateSeedItem(positions[0], itemData as SeedItemData);
                    break;
                case ItemType.Plant:

                    string[] extraDataParts = saved[i].extraData.Split(ParserHelper.PART_SEPARATOR[0]);
                    FarmerDateData datePlanted = FarmerDateData.ParseDate(extraDataParts[0]);

                    // NOTE plants can only take one tile
                    CreatePlantItem(positions[0], itemData as PlantData, datePlanted);
                    break;
                default:
                    Debug.LogErrorFormat("Object of type {0} is not a placeable world item", type);
                    break;
            }
        }
    }

    public void CreateTileFurnitureItem(Vector3Int pos, FurnitureData furnData)
    {
        if (furnData.hasCollision)
        {
            SetCollisionTile(pos, furnData);
        }
        else
        {
            SetOverlayTile(pos, furnData);
        }
    }


    public struct SavedWorldItem
    {
        public string key;
        public Vector3Int[] positions;
        public ItemData itemData;
        public WorldItem worldItem;
        public string extraData; // contains stuff like plant growth stage, chest inventory

        public SavedWorldItem(string key, WorldItem worldItem)
        {
            this.key = key;
            this.positions = worldItem.tilePositions;
            this.itemData = worldItem.itemData;
            this.worldItem = worldItem;
            this.extraData = null;
        }

        public SavedWorldItem(string key, Vector3Int[] pos, ItemData itemData)
        {
            this.key = key;
            this.positions = pos;
            this.itemData = itemData;
            this.worldItem = null;
            this.extraData = null;
        }

        public SavedWorldItem(string key, Vector3Int[] pos, ItemData itemData, string extraData)
        {
            this.key = key;
            this.positions = pos;
            this.itemData = itemData;
            this.extraData = extraData;
            this.worldItem = null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        tileGrid = GetComponent<Grid>();
        
        for (int i = 0; i < tileDataObjects.Length; i++)
        {
            tileTypeDict.Add(tileDataObjects[i].type, tileDataObjects[i]);
        }

        worldItemsParent = new GameObject("WorldItemsParent");

        //int shortest = Mathf.Min(tileDataObjects.Length, terrainTypes.Length);
        //for (int i = 0; i < shortest; i++)
        //{
        //    tileTypeDict.Add(terrainTypes[i], tileDataObjects[i]);
        //}

        //var terrainTypes = (TerrainType[])Enum.GetValues(typeof(TerrainType));

        //for (int i = 0; i < terrainTypes.Length; i++)
        //{
        //    for (int j = 0; j < tileTypes.Length; j++)
        //    {
        //        if (tileTypes[j].type == (TerrainType)terrainTypes.GetValue(i))
        //        {
        //            tileTypeDict.Add(tileTypeDict[GetEnumValue<TerrainType>(i)], );
        //        }
        //    }
        //}


        RegisterExistingWorldItems();

        if (SavedWorldItemsExists())
        {
            RestoreWorldItemsFromSaved(ParserHelper.ReadSaved(GetCurrentSceneFilename(WORLD_ITEMS_SAVE_FILENAME), ParserHelper.ParseSavedWorldItem));
        }



        if (SavedTilemapExists())
        {
           terrainDataDict = RestoreTilemapFromSaved(ParserHelper.ReadSaved(GetCurrentSceneFilename(TILE_SAVE_FILENAME), ParserHelper.ParseSavedTile));
        }
        else
        {
           terrainDataDict = GenerateDataDictFromSceneData();
        }

        SaveForSceneTransition();
    }

    public void SaveForSceneTransition()
    {
        SaveTilemap();
        SaveWorldItems();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            CheatGrowAllPlants();
        }
    }

    private void RegisterExistingWorldItems()
    {
        WorldItem[] worldItems = FindObjectsOfType<WorldItem>();
        for (int i = 0; i < worldItems.Length; i++)
        {
            RegisterStaticObject(worldItems[i]);
        }
    }

    private void RegisterStaticObject(WorldItem item)
    {
        // This method is called before any "dynamic" objects are loaded in
        // therefore ANY object we find at this stage is a legitimate candidate

        ItemData itemData = item.itemData;

        Vector3Int topLeft = tileGrid.WorldToCell(item.transform.position);
        Vector3Int[] cellCoverage = itemData.CalculateCellCoverage(topLeft);

        // Multi objects are added as one dictionary item using concatenated position string as key
        string posKey = ParserHelper.Vector3IntToString(cellCoverage);
        worldItemsDict.Add(posKey, item);

        if (cellCoverage.Length > 1)
        {
            for (int i = 0; i < cellCoverage.Length; i++)
            {
                multiTileMap.Add(cellCoverage[i], posKey);
            }
        }

        if (itemData.ItemType == ItemType.Storage)
        {
            StorageFurnitureData storageData = itemData as StorageFurnitureData;
            StorageComponent store = item.GetComponent<StorageComponent>();
            store.InitialiseInventory(storageData.inventoryDimensions.x, storageData.inventoryDimensions.y);
        }
        
    }

    public static Vector3Int[] GetCellCoverage(Vector3Int topLeft, int width, int height)
    {
        Vector3Int[] occupiedPositions = new Vector3Int[width * height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                occupiedPositions[(y * height) + x] = topLeft + new Vector3Int(x, -y, 0);
            }
        }

        return occupiedPositions;
    }

    void CheatGrowAllPlants()
    {
        foreach (var entry in worldItemsDict)
        {
            if (entry.Value.itemData.ItemType == ItemType.Plant)
            {
                PlantComponent plant = entry.Value.GetComponent<PlantComponent>();
                plant.AdvanceGrowth();
            }   
        }
    }

    public Vector3 GetCellWorldPos(Vector3Int cellPosition)
    {
        return tileGrid.CellToWorld(cellPosition);
    }

    public bool UseOnTerrain(Vector3Int cellPosition, UseableItemData heldItem)
    {
        bool success = false;
        // Check that there is existing land at this position i.e. not a void
        if (terrainDataDict.ContainsKey(cellPosition))
        {
            TerrainTileData tileData = terrainDataDict[cellPosition];

            success = heldItem.HandleAction(cellPosition, tileData, this);
            
            //if (tileData.IsValidActionType(heldItem.interactionType))
            //{

                

            //    if (heldItem.interactionType == InteractionType.Place)
            //    {
            //        if (heldItem.ItemType == ItemType.Seed)
            //        {
                        
            //        } else if (heldItem.ItemType == ItemType.Furniture)
            //        {
                        
            //        }
            //    }

                
            //}

        }
        return success;
    }

    public InteractionResultData InteractWithItemAt(Vector3Int cellPosition)
    {
        if (terrainDataDict.ContainsKey(cellPosition))
        {
            ItemData worldItemData = GetWorldItemDataAt(cellPosition);
            if (worldItemData != null)
            {
                if (worldItemData.ItemType == ItemType.Plant)
                {
                    WorldItem worldItem = GetWorldItemAt(cellPosition);
                    PlantComponent plant = worldItem.GetComponent<PlantComponent>();

                    if (plant.FullyGrown)
                    {
                        PlantData plantData = worldItemData as PlantData;
                        DestroyWorldItem(cellPosition);
                        CreateDroppedItemAt(cellPosition, plantData.resultingCrop, plantData.cropYield);
                        return new InteractionResultData(InteractionResult.SpawnedItem);
                    }

                }
                else if (worldItemData.ItemType == ItemType.Furniture)
                {
                    FurnitureData furnData = worldItemData as FurnitureData;
                } else if (worldItemData.ItemType == ItemType.InteractableFurniture)
                {
                    //WorldItem worldItem = GetWorldItemAt(cellPosition);
                    //DoorPortal door = worldItem.GetComponent<DoorPortal>();
                    //if (door != null)
                    //{
                    //    return new InteractionResultData(InteractionResult.OpenDoor, worldItemData as InteractableFurnitureData);
                    //}
                } else if (worldItemData.ItemType == ItemType.Storage)
                {

                    // Storage type itemDatas should have a StorageComponent on their WorldItem
                    WorldItem worldItem = GetWorldItemAt(cellPosition);
                    StorageComponent store = worldItem.GetComponent<StorageComponent>();

                    //StorageFurnitureData storageData = worldItemData as StorageFurnitureData;
                    bool success = store.IsUnlocked();
                    if (success)
                    {
                        return new InteractionResultData(InteractionResult.OpenDualInventory, store.Inventory);
                    }

                }
            }
        }
        return new InteractionResultData(InteractionResult.Failure);
    }

    public void CreateWorldItem(Vector3Int cellPosition, ItemData itemDataPrefab, bool enableCollision)
    {
        CreateWorldItem(new Vector3Int[] { cellPosition }, itemDataPrefab, enableCollision, null);
    }

    public void CreateWorldItem(Vector3Int[] cellPositions, ItemData itemDataPrefab, bool enableCollision)
    {
        CreateWorldItem(cellPositions, itemDataPrefab, enableCollision, null);
    }

    public void CreateWorldItem(Vector3Int cellPosition, ItemData itemDataPrefab, bool enableCollision, InventorySystem existingInventory)
    {
        CreateWorldItem(new Vector3Int[] { cellPosition }, itemDataPrefab, enableCollision, existingInventory);
    }

    public void CreateWorldItem(Vector3Int[] cellPositions, ItemData itemDataPrefab, bool enableCollision, InventorySystem existingInventory)
    {
        WorldItem placedItem = InstantiateWorldItemAtTile(cellPositions[0]); // assume cellPositions[0] contains top left tile

        // Create a storage component on the WorldItem monobehaviour if the itemDataPrefab is a storage furniture type
        if (itemDataPrefab.ItemType == ItemType.Storage)
        {
            StorageFurnitureData storageData = itemDataPrefab as StorageFurnitureData;
            StorageComponent store = placedItem.gameObject.AddComponent<StorageComponent>();

            if (existingInventory != null)
            {
                store.Inventory = existingInventory;
            } else
            {
                store.InitialiseInventory(storageData.inventoryDimensions.x, storageData.inventoryDimensions.y);
            }
            store.SetDefaultInsertCallback();
        }

        placedItem.GetComponent<Collider2D>().enabled = enableCollision;

        placedItem.ChangeItemData(Instantiate(itemDataPrefab));


        // Store in dictionary with full key (may be for multiple positions)
        // in the case of multiple tiles the individual positions will be mapped to 
        // the multiple tiles through the multiTileMap
        string posKey = ParserHelper.Vector3IntToString(cellPositions);

        if (cellPositions.Length > 1)
        {
            for (int i = 0; i < cellPositions.Length; i++)
            {
                multiTileMap.Add(cellPositions[i], posKey);
            }
        }

        worldItemsDict.Add(posKey, placedItem);
    }

    void AssignDataToExistingWorldItem(WorldItem existingItem, bool enableCollision, InventorySystem existingInventory)
    {

        if (existingItem.itemData.ItemType == ItemType.Storage)
        {
            StaticObjectData staticObjectData = existingItem.itemData as StaticObjectData;
            StorageComponent store = existingItem.GetComponent<StorageComponent>();

            if (existingInventory != null)
            {
                store.Inventory = existingInventory;
            } else
            {
                // RegisterStaticObjects() already initialises inventories, don't need to do it here but might need
                // to in future for clarity. If inventories are turning up mysteriously null, probably init them here.
            }
        }

        existingItem.GetComponent<Collider2D>().enabled = enableCollision;
    }

    // This and create plant item could be merged into one, seed data could just be a parameter to the create plant item
    // since its not used for anything else.
    public void CreateSeedItem(Vector3Int position, SeedItemData seedData)
    {
        WorldItem placedItem = InstantiateWorldItemWithItemData(position, seedData.resultingPlantData);
        //placedItem.ChangeItemData(Instantiate(seedData.resultingPlantData));

        PlantComponent plantComponent = placedItem.gameObject.AddComponent<PlantComponent>();

        plantComponent.Initialise(seedData.resultingPlantData.growthStageSprites.Length, PersistentDayData.today); // this needs todays date

        placedItem.GetComponent<SpriteRenderer>().material = seedData.resultingPlantData.plantMaterial;

        worldItemsDict.Add(ParserHelper.Vector3IntToString(position), placedItem);
    }

    // this creates plants when loading back in
    public void CreatePlantItem(Vector3Int position, PlantData plantData, FarmerDateData datePlanted)
    {
        WorldItem placedItem = InstantiateWorldItemWithItemData(position, plantData);
        PlantComponent plantComponent = placedItem.gameObject.AddComponent<PlantComponent>();

        plantComponent.Initialise(plantData.growthStageSprites.Length, datePlanted);

        placedItem.GetComponent<SpriteRenderer>().material = plantData.plantMaterial;

        worldItemsDict.Add(ParserHelper.Vector3IntToString(position), placedItem);
    }

    public DroppedItem CreateDroppedItemAt(Vector3 position, ItemData itemData, int number)
    {
        DroppedItem droppedItem = Instantiate(droppedItemPrefab, position, Quaternion.identity);
        droppedItem.ChangeItemData(itemData);
        droppedItem.NumberHeld = number;

        return droppedItem;
    }

    public void DestroyWorldItem(Vector3Int cellPosition)
    {
        string posString = ParserHelper.Vector3IntToString(cellPosition);
        WorldItem worldItem = GetWorldItems()[posString];

        ItemData itemData = worldItem.itemData;

        if (itemData.ItemType == ItemType.Furniture)
        {
            FurnitureData furnData = itemData as FurnitureData;
            if (furnData.smashable && furnData.dropsWhenSmashed)
            {
                CreateDroppedItemAt(GetCellWorldPos(cellPosition), furnData, 1);
                Destroy(GetWorldItems()[posString].gameObject);
                GetWorldItems().Remove(posString);
            }
        }

        if (itemData.ItemType == ItemType.Storage)
        {
            StorageFurnitureData storageData = itemData as StorageFurnitureData;
            StorageComponent store = worldItem.GetComponent<StorageComponent>();
            if (storageData.smashable)
            {
                if (storageData.dropsWhenSmashed)
                {
                    CreateDroppedItemAt(GetCellWorldPos(cellPosition), storageData, 1);
                }
                DropItemsInStorage(cellPosition, store);
                Destroy(GetWorldItems()[posString].gameObject);
                GetWorldItems().Remove(posString);
            }
        }
        
    }

    public void DropItemsInStorage(Vector3Int cellPosition, StorageComponent storageComponent)
    {
        for (int i = 0; i < storageComponent.Inventory.cells.Length; i++)
        {
            InventoryCell cell = storageComponent.Inventory.cells[i];
            if (cell.ContainsItem)
            {
                UseableItemData item = null;
                int amountRemoved = 0;
                InventoryActionResult result = storageComponent.Inventory.RequestRemoval(i, out item, out amountRemoved, InventoryMoveType.WholeStack);
                CreateDroppedItemAt(GetCellWorldPos(cellPosition), item, amountRemoved);   
            }
        }
    }

    public WorldItem InstantiateWorldItemAtTile(Vector3Int cellpos)
    {
        return Instantiate(worldItemPrefab, baseMap.CellToWorld(cellpos) + (baseMap.cellSize * 0.5f), Quaternion.identity, worldItemsParent.transform);
    }

    public WorldItem InstantiateWorldItemWithItemData(Vector3Int cellPos, ItemData itemData)
    {
        WorldItem item = InstantiateWorldItemAtTile(cellPos);
        item.ChangeItemData(itemData);
        return item;
    }

    public Dictionary<string, WorldItem> GetWorldItems()
    {
        return worldItemsDict;
    }

    public Dictionary<Vector3Int, ItemData> GetWorldTiles()
    {
        return worldTilesDict;
    }

    public void SetCollisionTile(Vector3Int position, UseableItemData itemData)
    {
        collisionMap.SetTile(position, itemData.worldTile);
        worldTilesDict[position] = itemData; // may need to instantiate here and similar places?
    }

    public void SetOverlayTile(Vector3Int position, UseableItemData itemData)
    {
        overlayMap.SetTile(position, itemData.worldTile);
        worldTilesDict[position] = itemData;
    }

    public void SetOverlayTile(Vector3Int position, TerrainTileData terrainData)
    {
        overlayMap.SetTile(position, terrainData.tile);
        terrainDataDict[position] = terrainData;
    }

    public void TillTile(Vector3Int position)
    {
        SetOverlayTile(position, tileTypeDict[TerrainType.Tilled]);
    }

    public void WaterTile(Vector3Int position)
    {
        SetOverlayTile(position, tileTypeDict[TerrainType.Watered]);
    }

    public void RevertToDirt(Vector3Int position)
    {
        RemoveOverlayTile(position);
        terrainDataDict[position] = tileTypeDict[TerrainType.Dirt];
    }

    public void RemoveCollisionTile(Vector3Int position)
    {
        collisionMap.SetTile(position, null);
        worldTilesDict.Remove(position);
    }

    public void RemoveOverlayTile(Vector3Int position)
    {
        overlayMap.SetTile(position, null);
        worldTilesDict.Remove(position);
    }

    public bool WorldItemExistsAt(Vector3Int position)
    {
        return GetWorldItems().ContainsKey(ParserHelper.Vector3IntToString(position)) || GetWorldTiles().ContainsKey(position);
    }

    ItemData GetWorldItemDataAt(Vector3Int position)
    {
        string posKey = ParserHelper.Vector3IntToString(position);

        // Check for multi-tile item, get full key if exists
        if (multiTileMap.TryGetValue(position, out string multiKey))
        {
            posKey = multiKey;
        }

        if (GetWorldItems().ContainsKey(posKey))
        {
            return GetWorldItems()[posKey].itemData;
        } else if (GetWorldTiles().ContainsKey(position))
        {
            return GetWorldTiles()[position];
        } else
        {
            return null;
        }
    }

    WorldItem GetWorldItemAt(Vector3Int position)
    {
        string posKey = ParserHelper.Vector3IntToString(position);

        // Check for multi-tile item, get full key if exists
        if (multiTileMap.TryGetValue(position, out string multiKey))
        {
            posKey = multiKey;
        }

        if (GetWorldItems().ContainsKey(posKey))
        {
            return GetWorldItems()[posKey];
        }
        else
        {
            return null;
        }
    }

}
