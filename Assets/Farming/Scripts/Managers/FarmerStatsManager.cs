﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class FarmerStatsManager : MonoBehaviour
{

    const string STATS_SAVE_FILENAME = "Resources/PlayerStats.txt";

    public FarmerController currentFarmerInstance;

    private int currentMoney;
    public RangeStat health;
    public RangeStat energy;

    public event Action<int> OnMoneyChanged;

    private void Awake()
    {
        // Default stats
        SetDefaultStats();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (File.Exists(STATS_SAVE_FILENAME))
        {
            RestoreStatsFromSaved(ParserHelper.ReadSaved(STATS_SAVE_FILENAME, ParserHelper.ParseSavedStats)[0]);
        }
        else
        {
            Debug.Log("Fresh stats");
            SetDefaultStats();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetDefaultStats()
    {
        currentMoney = 0;
        health = new RangeStat(80, OnZeroHealth, () => { });
        energy = new RangeStat(100, OnZeroEnergy, () => { });
    }

    public int GetCurrentMoney()
    {
        return currentMoney;
    }

    private void SetCurrentMoney(int amount)
    {
        currentMoney = amount;
        OnMoneyChanged?.Invoke(currentMoney);
    }

    public void AddMoney(int amount)
    {
        currentMoney += amount;
        OnMoneyChanged?.Invoke(currentMoney);
    }

    public void RemoveMoney(int amount)
    {
        if (currentMoney - amount >= 0)
        {
            currentMoney -= amount;
        }
        OnMoneyChanged?.Invoke(currentMoney);
    }

    public void HandleItemUse(UseableItem item)
    {
        if (item.itemData.interactionType == InteractionType.Consume)
        {
            Consume(item);
        } else if (item.itemData.ItemType == ItemType.Tool)
        {
            ToolData toolData = item.itemData as ToolData;
            energy.Current -= toolData.energyCost;
        }
    }

    public void SaveForSceneTransition()
    {
        SavedPlayerStats stats = new SavedPlayerStats(health, energy, currentMoney);
        ParserHelper.WriteSavedToString(STATS_SAVE_FILENAME, stats, ParserHelper.SavedStatsToString);
    }

    public void RestoreStatsFromSaved(SavedPlayerStats stats)
    {
        health = stats.health;
        energy = stats.energy;
        SetCurrentMoney(stats.money);

        health.OnDepleted = OnZeroHealth;
        energy.OnDepleted = OnZeroEnergy;
    }

    public void Consume(UseableItem item)
    {
        Debug.Log("Consumed " + item.itemData.itemName);
    }

    void OnZeroHealth()
    {
        Debug.Log("ded");
    }

    void OnZeroEnergy()
    {
        Debug.Log("pooped");
    }

    public struct SavedPlayerStats
    {
        public RangeStat health;
        public RangeStat energy;
        public int money;

        public SavedPlayerStats(RangeStat health, RangeStat energy, int money)
        {
            this.health = health;
            this.energy = energy;
            this.money = money;
        }
    }

}

public class RangeStat
{

    int max;
    int current;

    public Action OnDepleted; // Called when current == 0
    public Action OnFilled; // Called when current == max; (e.g. particle/sound effect or something).
    public Action<int, int> OnChanged; // Called whenever value is changed. Passes new value.

    public int Max
    {
        get
        {
            return max;
        }
        set // automatically limit current health to a new max health value.
        {
            max = Mathf.Max(0, value);
            current = Mathf.Min(current, max);
            OnChanged?.Invoke(current, max);
        }
    }

    public int Current
    {
        get
        {
            return current;
        }
        set
        {
            current = Mathf.Clamp(value, 0, max);
            if (current == 0)
            {
                OnDepleted();
            } else if (current == max)
            {
                OnFilled();
            }
            OnChanged?.Invoke(current, max);
        }
    }

    public RangeStat(int max)
    {
        this.max = max;
        this.current = max;
        this.OnDepleted = () => { };
        this.OnFilled = () => { };
    }

    public RangeStat(int max, int current)
    {
        this.max = max;
        this.current = current;
        this.OnDepleted = () => { };
        this.OnFilled = () => { };
    }

    public RangeStat(int max, Action OnDepleted, Action OnFilled)
    {
        this.max = max;
        this.current = max;
        this.OnDepleted = OnDepleted;
        this.OnFilled = OnFilled;
        //OnChanged?.Invoke(current, max); // initial call in case something like a UI element needs to initialise off the first value.
    }

    public string GetSaveString()
    {
        return max.ToString() + ParserHelper.PART_SEPARATOR[0] + current.ToString();
    }

    public static RangeStat Parse(string rangeStatString)
    {
        string[] parts = rangeStatString.Split(ParserHelper.PART_SEPARATOR[0]);
        if (parts.Length == 2)
        {
            return new RangeStat(int.Parse(parts[0]), int.Parse(parts[1]));
        } else
        {
            Debug.LogError("Incorrect number of parts parsed from RangeStat string, expected 2: " + parts.Length);
            return null;
        }

        // WHY WOULD IT SWAP THE MAX AND CURRENT IN THE STATS FILE
    }
}

