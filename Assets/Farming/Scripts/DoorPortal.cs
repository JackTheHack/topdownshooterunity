﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorPortal : MonoBehaviour
{

    public string doorLinkTag; // Should match the tag of exactly one other door in the game.
    public string goesTo;

    public bool mustBeClicked;
    public bool isLocked;
    private float clickInteractionDistance = 2;

    public Transform SpawnPoint { get; private set; }

    private LevelChangerUI levelChanger;

    private void Awake()
    {
        if (transform.childCount > 0)
        {
            levelChanger = FindObjectOfType<LevelChangerUI>();

            SpawnPoint = transform.GetChild(0).transform;

            Collider2D coll = GetComponent<Collider2D>();
            if (coll.bounds.Contains(SpawnPoint.position))
            {
                Debug.LogError(string.Format("Spawn point is inside door collider on door: {0} leading to {1}", doorLinkTag, goesTo));
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {

        // Door interaction is outside of the normal item interaction processing for now.
        // The door itself will look for player interactions with it.
        if (Input.GetMouseButtonDown(1))
        {
            //if (mustBeClicked)
            //{
            //    Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //    if (Vector2.Distance(mousePos, transform.position) < clickInteractionDistance && !isLocked)
            //    {
            //        GoThrough();
            //    }
            //}
        }
    }

    public void GoThrough(InventorySystem inventory)
    {
        DoorManager.currentDoorTag = doorLinkTag;
        if (goesTo != "")
        {

            string invString = inventory.GenerateSaveString();
            PlayerPrefs.SetString(PlayerItemUseManager.INVENTORY_SAVE_KEY, invString);

            TerrainManager tm = FindObjectOfType<TerrainManager>();
            tm.SaveForSceneTransition();

            FarmerStatsManager statsMan = FindObjectOfType<FarmerStatsManager>();
            statsMan.SaveForSceneTransition();

            PersistentDayData.lastTravelTime = FindObjectOfType<GameTimeManager>().DateData;

            SceneLoadData.sceneLoadReason = SceneLoadReason.Travel;

            levelChanger.FadeToScene(goesTo);
        }
    }

    public bool IsValidClickForDoor(Vector2 clickPos)
    {
        return mustBeClicked && !isLocked && (Vector2.Distance(clickPos, transform.position) < clickInteractionDistance);
    }
}
