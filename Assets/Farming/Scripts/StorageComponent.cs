﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageComponent : MonoBehaviour
{

    public PlayerItemUseManager ItemUseManager { get; private set; }
    public FarmerUIManager FarmerUIManager { get; private set; }

    private InventorySystem inventory;
    public InventorySystem Inventory
    {
        get
        {
            if (inventory == null)
            {
                inventory = new InventorySystem(1, 1, false);
            }
            return inventory;
        }
        set
        {
            inventory = value;
        }
    }


    private bool isLocked;

    public bool IsUnlocked()
    {
        return !isLocked;
    }

    public void Lock()
    {
        isLocked = true;
    }

    public void Awake()
    {
        ItemUseManager = FindObjectOfType<PlayerItemUseManager>();
        FarmerUIManager = FindObjectOfType<FarmerUIManager>();
    }


    // must be called to get inventory at the right size.
    public void InitialiseInventory(int width, int height)
    {
        inventory = new InventorySystem(width, height, false);
    }

    public void SetDefaultInsertCallback()
    {
        inventory.OnInsert += ItemUseManager.InsertCallback;
    }


}
