﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class FarmerController : PhysicsObject
{

    //public UIManager uimanager;

    Animator animController;
    public Animator handAnimator;

    Vector2 movement = Vector2.zero;

    SpriteRenderer spriteRenderer;
    //float lastHorizontalVal = 0;

    [SerializeField]
    private float MaxMovementSpeed;
    //[SerializeField]
    //private float JumpTakeOffSpeed;


    //public Weapon currentWeapon;

    public UseableItem heldItem { get; private set; }

    public void EquipItem(UseableItemData itemData)
    {
        UnequipItem();
        heldItem.ChangeItemData(itemData);
    }

    public void UnequipItem()
    {
        heldItem.RemoveItemData();
    }

    private float actualScale = 1;

    public bool facingRight { get; private set; }

    LayerMask wallLayerMask;

    bool stoppedRight;
    bool stoppedLeft;
    bool stoppedUp;
    bool stoppedDown;

    bool isInvincible = false;

    Vector2 previousMove = Vector2.zero;
    [SerializeField]
    private float DodgeSpeedMultiplier;

    ParticleSystem dodgeParticles;
    bool skipDodge = false;

    PlayerItemUseManager itemUseManager;
    DamageController damageController;

    LayerMask damageLayerMask;
    LayerMask doorPortalLayerMask;

    public GameObject leftHand;
    public GameObject rightHand;

    private Vector2 leftHandStartPosLocal;
    private Vector2 rightHandStartPosLocal;

    DoorPortal[] currentSceneDoors;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        wallLayerMask = LayerMask.NameToLayer("SolidWall");
        damageLayerMask = LayerMask.GetMask(new string[] { "EnemyTrigger", "EnemyBullet" });

        doorPortalLayerMask = LayerMask.NameToLayer("DoorPortal");

        rb2d = GetComponent<Rigidbody2D>();
        animController = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        facingRight = true;

        //FullAmmoUIUpdate();

        dodgeParticles = GetComponent<ParticleSystem>();
        dodgeParticles.Stop();



        leftHandStartPosLocal = leftHand.transform.localPosition;
        rightHandStartPosLocal = rightHand.transform.localPosition;

        currentSceneDoors = FindObjectsOfType<DoorPortal>();

        

        //handAnimator.SetFloat("SpeedMult", currentWeapon.GetShootAnimSpeedMult());

        //rightHand.GetComponentInChildren<SpriteRenderer>().enabled = !currentWeapon.IsOneHanded;
    }

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animController = GetComponent<Animator>();

        damageController = GetComponent<DamageController>();
        heldItem = GetComponentInChildren<UseableItem>();

        itemUseManager = FindObjectOfType<PlayerItemUseManager>();

        FindSpawnPoint();
    }

    void FindSpawnPoint()
    {
        string doorToSpawnAt = DoorManager.currentDoorTag;
        if (doorToSpawnAt != "")
        {
            DoorPortal[] sceneDoors = FindObjectsOfType<DoorPortal>();
            for (int i = 0; i < sceneDoors.Length; i++)
            {
                if (sceneDoors[i].doorLinkTag == doorToSpawnAt)
                {
                    transform.position = sceneDoors[i].SpawnPoint.position;
                }
            }
        } else
        {
            // If I didn't enter the scene through a door, maybe I came in through a bed?
            Bed bed = FindObjectOfType<Bed>();
            if (bed != null)
            {
                transform.position = bed.transform.position;
            }
        }
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        if (animController.GetBool("InDodge"))
        {
            if (previousMove == Vector2.zero)
            {
                move.x = 1;
                move.y = 0;
            }
            else
            {
                move = previousMove;
            }
            targetVelocity = move.normalized * MaxMovementSpeed * DodgeSpeedMultiplier;
        }
        else
        {
            move.x = Input.GetAxisRaw("Horizontal");
            move.y = Input.GetAxisRaw("Vertical");
            animController.SetFloat("velocityX", Mathf.Abs(velocity.magnitude) / MaxMovementSpeed);
            targetVelocity = move * MaxMovementSpeed;
        }

        previousMove = move;

    }

    private new void Update()
    {
        base.Update();
        if (Input.GetMouseButton(0))
        {
            //bool success = currentWeapon.RequestShot();
            //if (success)
            //{
            //    handAnimator.SetTrigger("Shoot");
            //}
        }
        if (Input.GetMouseButtonDown(1))
        {
            bool success = TryDodge();
            if (success)
            {

            }

            for (int i = 0; i < currentSceneDoors.Length; i++)
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (currentSceneDoors[i].IsValidClickForDoor(mousePos))
                {
                    if (Vector2.Distance(transform.position, currentSceneDoors[i].transform.position) < 3)
                    {
                        currentSceneDoors[i].GoThrough(itemUseManager.playerInventory);
                    }
                }
            }


        }
        if (Input.GetButtonDown("Reload"))
        {
            //bool success = currentWeapon.RequestReload();
            //if (success)
            //{
            //    handAnimator.SetTrigger("Reload");
            //}

            //Door door = FindObjectOfType<Door>();
            //door.NextState();

        }

        RotateHandsToMouse();

    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        for (int i = 0; i < droppedItems.Count; i++)
        {
            float dist = Vector2.Distance(transform.position, droppedItems[i].transform.position);

            // If the item was spawned close to player and locked, unlock it once the player has moved out of range + 10%.
            if (droppedItems[i].moveLocked && dist > droppedItems[i].attractionDistance + (droppedItems[i].attractionDistance * 0.1f))
            {
                droppedItems[i].UnlockMovement();
            }

            if (dist < droppedItems[i].attractionDistance)
            {
                droppedItems[i].SuckIn();
            }
            if (dist < droppedItems[i].pickupDistance && !droppedItems[i].moveLocked && !itemUseManager.droppedItemQueue.Contains(droppedItems[i]))
            {
                itemUseManager.RequestInventoryInsertion(droppedItems[i]);
                //itemUseManager.playerInventory.OnInsert += (action) => { DestroyDroppedItem(action, droppedItems[i], i); };
                //itemUseManager.OnItemPickup += (action) => { itemUseManager.CheckToDestroyDroppedItem(action, droppedItems[i]); };
            }
        }   
    }

    public void SkipDodgeThisFrame()
    {
        skipDodge = true;
    }

    public void ClearDodgeSkip()
    {
        skipDodge = false;
    }

    private void RotateHandsToMouse()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5f;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;



        float angleRad = Mathf.Atan2(mousePos.y, mousePos.x);
        float angleDeg = angleRad * Mathf.Rad2Deg;
        leftHand.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleDeg));
        rightHand.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleDeg));

        if (angleDeg > 80 || angleDeg < -100)
        {
            SetFacingLeft();
        }
        else if (angleDeg < 100 || angleDeg > -80)
        {
            SetFacingRight();
        }

        leftHand.transform.localPosition = RotateAroundPoint(facingRight ? leftHandStartPosLocal : -leftHandStartPosLocal, facingRight ? angleRad : -angleRad, Vector2.zero);
        rightHand.transform.localPosition = RotateAroundPoint(facingRight ? rightHandStartPosLocal : -rightHandStartPosLocal, facingRight ? angleRad : -angleRad, Vector2.zero);

    }

    private Vector2 RotateAroundPoint(Vector2 point, float angle, Vector2 pivotPoint)
    {
        float s = Mathf.Sin(angle);
        float c = Mathf.Cos(angle);

        Vector2 workingPoint = new Vector2(point.x, point.y);

        workingPoint.x -= pivotPoint.x;
        workingPoint.y -= pivotPoint.y;

        float newx = (workingPoint.x * c) - (workingPoint.y * s);
        float newy = (workingPoint.x * s) + (workingPoint.y * c);

        workingPoint.x = newx + pivotPoint.x;
        workingPoint.y = newy + pivotPoint.y;

        return workingPoint;
    }

    //public void MagazineUIUpdate()
    //{
    //    uimanager.UpdateMagazineCount(currentWeapon.bulletsInMagazine);
    //}

    //public void ReserveAmmoUIUpdate()
    //{
    //    uimanager.UpdateTotalAmmoCount(currentWeapon.reserveAmmoCount);
    //}

    //public void FullAmmoUIUpdate()
    //{
    //    MagazineUIUpdate();
    //    ReserveAmmoUIUpdate();
    //}

    //public void ReloadUIUpdate()
    //{
    //    uimanager.DoReload(currentWeapon.bulletsInMagazine, currentWeapon.reserveAmmoCount);
    //}

    public void SetFacingLeft()
    {
        Vector3 playerScale = transform.localScale;
        playerScale.x = actualScale * -1;
        transform.localScale = playerScale;

        //foreach (Transform child in transform)
        //{
        //    Vector3 childScale = transform.localScale;
        //    //childScale.x *= -1;
        //    childScale.y *= -1;
        //    child.transform.localScale = childScale;

        //}

        Vector3 handScale = transform.localScale;
        handScale.x = actualScale * -1;
        handScale.y = actualScale * -1;
        leftHand.transform.localScale = handScale;
        rightHand.transform.localScale = handScale;

        //currentWeapon.transform.localScale = playerScale;


        //leftHand.transform.localScale = playerScale;
        //rightHand.transform.localScale = playerScale;

        facingRight = false;
    }

    public void SetFacingRight()
    {
        Vector3 playerScale = transform.localScale;
        playerScale.x = actualScale;
        transform.localScale = playerScale;

        Vector3 handScale = transform.localScale;
        leftHand.transform.localScale = handScale;
        rightHand.transform.localScale = handScale;

        //foreach (Transform child in transform)
        //{
        //    child.transform.localScale = playerScale;
        //}

        //Vector3 handScale = transform.localScale;
        //handScale.y = actualScale * -1;
        //leftHand.transform.localScale = handScale;
        //rightHand.transform.localScale = handScale;

        //currentWeapon.transform.localScale = playerScale;

        //foreach (Transform child in transform)
        //{
        //    Vector3 childScale = transform.localScale;
        //    childScale.x *= -1;
        //    transform.localScale = theScale;
        //}


        //leftHand.transform.localScale = playerScale;
        //rightHand.transform.localScale = playerScale;

        facingRight = true;
    }

    private bool TryDodge()
    {
        if (!skipDodge && !animController.GetBool("InDodge"))
        {
            CancelInvoke("ExitInvincibilityState");
            animController.SetBool("InDodge", true);
            EnterInvincibilityState();

            return true;
        }

        return false;
    }

    public void PlayDodgeParticles()
    {
        dodgeParticles.Play();
    }

    public void StopDodgeParticles()
    {
        dodgeParticles.Stop();
    }

    private void EnterInvincibilityState()
    {
        isInvincible = true;
    }

    public void ExitInvincibilityState()
    {
        isInvincible = false;
    }

    public void EnterInvincibilityStateForTime(float seconds)
    {
        EnterInvincibilityState();
        Invoke("ExitInvincibilityState", seconds);
    }

    public void SwitchWeapon(Weapon w)
    {

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log(collision + "  " + Time.frameCount);
        //Debug.Log("player collided with layer : " + (damageLayerMask.value & 1 << collision.gameObject.layer));
        // check for damage

        if ( (doorPortalLayerMask.value & 1 << collision.gameObject.layer) != 0)
        {
            TryToGoThroughDoor(collision.gameObject);
        }

    }

    private void TryToGoThroughDoor(GameObject doorGameobject)
    {
        DoorPortal door = doorGameobject.gameObject.GetComponent<DoorPortal>();
        if (door != null && !door.mustBeClicked)
        {
            door.GoThrough(itemUseManager.playerInventory);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (isInvincible)
        {
            // skip damage
        }
        else
        {

            if ((damageLayerMask.value & 1 << collision.gameObject.layer) != 0)
            {
                damageController.TakeDamage(new DamageController.DamageInfo(1, "Physical"));
                //uimanager.UpdateHealthText(damageController.GetHealth());

                EnterInvincibilityStateForTime(0.8f);
            }
            //if (collision.gameObject.layer == LayerMask.NameToLayer("EnemyBullet"))
            //{

            //}
            // do damage
        }
    }

    List<DroppedItem> droppedItems = new List<DroppedItem>();
    public void NotifyDroppedItemCreated(DroppedItem item)
    {
        droppedItems.Add(item);

        //itemUseManager.OnItemPickup += (action, droppedItem) => { item.CheckToDestroyDroppedItem(action, droppedItem, droppedItems.Count - 1); }; // assuming we always add to end of list

        // If the dropped item is spawned in pickup range then prevent it moving until the player has moved away and come back.
        if (Vector2.Distance(item.transform.position, transform.position) < item.attractionDistance)
        {
            item.LockMovement();
        }
    }

    public void NotifyDroppedItemDestroyed(DroppedItem item)
    {
        droppedItems.Remove(item);
    }


    //private void EndDodge()
    //{
    //    animController.SetBool("InDodge", false);
    //}

    //IEnumerator DodgeCoroutine()
    //{
    //    for (float f = 1f; f >= 0; f -= 0.1f)
    //    {
    //        animController.
    //        yield return null;
    //    }
    //}

    // Update is called once per frame
    //void Update () 
    //   {
    //       movement.x = Input.GetAxisRaw("Horizontal");
    //       movement.y = Input.GetAxisRaw("Vertical");

    //       //if (lastHorizontalVal <= 0 && movement.x > 0)
    //       //{
    //       //    SetFacingLeft();    
    //       //}
    //       //else if (lastHorizontalVal >= 0 && movement.x < 0)
    //       //{
    //       //    SetFacingRight();
    //       //}
    //       velocity = movement.normalized * Time.fixedDeltaTime * MovementSpeed;
    //       animController.SetBool("walking", Mathf.Abs(movement.x) > 0 || Mathf.Abs(movement.y) > 0);

    //       //transform.position += (Vector3);

    //       //transform.Translate();

    //           if (Input.GetMouseButton(0)) {
    //               bool success = currentWeapon.RequestShot();
    //}

    //       //lastHorizontalVal = movement.x;

    //}

    //private void FixedUpdate()
    //{

    //    rb2d.MovePosition((Vector2)transform.position + velocity);
    //}

    //void Flip()
    //{
    //    // Multiply the player's x local scale by -1
    //    Vector3 theScale = transform.localScale;
    //    theScale.x *= -1;
    //    transform.localScale = theScale;
    //}

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Debug.Log(collision.gameObject);
    //    if (collision.gameObject.layer == wallLayerMask)
    //    {
    //        ContactPoint2D contactPoint = collision.GetContact(0);
    //        // Determine direction player walked into wall.
    //        string direction = string.Empty;

    //        float playerAngle = transform.eulerAngles.z; // this probably won't change.

    //        float projection = Vector2.Dot(velocity, contactPoint.normal);
    //        if (projection < 0)
    //        {
    //            velocity = velocity - (projection * contactPoint.normal);
    //        }

    //        Vector2 unRotatedNormal = Quaternion.Euler(0, 0, -playerAngle) * contactPoint.normal;
    //        Debug.Log(velocity);
    //        unRotatedNormal.x = Mathf.Round(unRotatedNormal.x);
    //        unRotatedNormal.y = Mathf.Round(unRotatedNormal.y);

    //        if (Vector2.left == unRotatedNormal)
    //        {
    //            stoppedRight = true;
    //            Debug.Log("Stopped on right side.");
    //        }
    //        if (Vector2.right == unRotatedNormal)
    //        {
    //            stoppedLeft = true;
    //            Debug.Log("Stopped on left side.");
    //        }
    //        if (Vector2.up == unRotatedNormal)
    //        {
    //            stoppedDown = true;
    //            Debug.Log("Stopped on bottom side.");
    //        }
    //        if (Vector2.down == unRotatedNormal)
    //        {
    //            stoppedUp = true;
    //            Debug.Log("Stopped on top side.");
    //        }
    //    }
    //}

    //public static string DirectionToString(Vector2 direction)
    //{
    //    if (direction == Vector2.up)
    //    {
    //        return "UP";
    //    }
    //    else if (direction == Vector2.down)
    //    {
    //        return "DOWN";
    //    }
    //    else if (direction == Vector2.right)
    //    {
    //        return "RIGHT";
    //    }
    //    else if (direction == Vector2.left)
    //    {
    //        return "LEFT";
    //    }
    //    else
    //    {
    //        return "INCONCLUSIVE";
    //    }
    //}
}

