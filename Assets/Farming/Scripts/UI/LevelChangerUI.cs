﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChangerUI : MonoBehaviour
{

    // If this messes up Lighting the tutorial man 
    // said to turn off Auto-Generate lighting near
    // the end of this video...
    // https://www.youtube.com/watch?v=Oadq-IrOazg

    public Animator animator;

    private string sceneToLoadName;

    public void FadeToScene(string sceneName)
    {
        sceneToLoadName = sceneName;
        animator.SetTrigger("SceneLoadFadeOutTrigger");
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(sceneToLoadName);
    }
}
