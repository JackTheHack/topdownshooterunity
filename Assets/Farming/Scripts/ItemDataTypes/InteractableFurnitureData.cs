﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableFurnitureData : FurnitureData
{

    public override ItemType ItemType
    {
        get
        {
            return ItemType.InteractableFurniture;
        }
    }


}
