﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Represents a plant growing on the map.
[CreateAssetMenu(menuName = "FarmingItems/PlantData")]
public class PlantData : ItemData
{
    public Sprite[] growthStageSprites;
    public int daysToAdvanceStage;
    //public int growthStage = 0;

    public Material plantMaterial;

    public UseableItemData resultingCrop;
    public int cropYield; //

    public override ItemType ItemType
    {
        get
        {
            return ItemType.Plant;
        }
    }

    public override Sprite GetSpriteForWorldItem()
    {
        return growthStageSprites[0]; // not useable except when creating plant item
    }

    public override Vector3Int[] CalculateCellCoverage(Vector3Int topLeft)
    {
        throw new System.NotImplementedException();
    }
}