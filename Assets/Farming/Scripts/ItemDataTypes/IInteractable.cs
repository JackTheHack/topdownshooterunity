﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{

    bool Interact(); // return true for successful interaction
}
