﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

// Represents items that can be in the player's inventory and used in some way e.g. tools, food, seed bags, furniture.
public abstract class UseableItemData : ItemData
{
    public InteractionType interactionType;
    public float useCooldownTime;
    public int maxStackSize;
    //private int amountInStack;
    //public int AmountInStack
    //{
    //    get
    //    {
    //        return amountInStack;
    //    }
    //    set
    //    {
    //        amountInStack = Mathf.Max(0, value);
    //    }
    //}

    public Sprite inHandSprite; // Shown when holding object in hand or on ground in world
    public Sprite worldSprite; // Representation when placed in the world.
    public TileBase worldTile;

    public bool consumedOnUse;

    public int baseSellValue;

    public bool UsesTile
    {
        get
        {
            return worldSprite == null && worldTile != null;
        }
    }

    public bool ActsOnTilemap()
    {
        return interactionType != InteractionType.Consume;
    }

    public override Sprite GetSpriteForWorldItem()
    {
        return worldSprite;
    }

    // Determines what should be done when this item is used.
    public abstract bool HandleAction(Vector3Int cellPosition, TerrainTileData tileData, TerrainManager terrainManager);

}
