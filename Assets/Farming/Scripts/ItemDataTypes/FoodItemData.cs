﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/FoodData")]
public class FoodItemData : UseableItemData
{
    public override ItemType ItemType
    {
        get
        {
            return ItemType.Food;
        }
    }

    public int staminaRecovered;
    public int healthRecovered;

    public override bool HandleAction(Vector3Int cellPosition, TerrainTileData tileData, TerrainManager terrainManager)
    {
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override Vector3Int[] CalculateCellCoverage(Vector3Int topLeft)
    {
        return new Vector3Int[] { topLeft };
    }
}
