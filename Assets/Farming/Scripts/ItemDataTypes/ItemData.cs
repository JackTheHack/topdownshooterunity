﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType { Plant, Tool, Weapon, Seed, Furniture, Food, Storage, InteractableFurniture }

// Base class for all items (things that could be spawned/exist in the world
public abstract class ItemData : ScriptableObject
{
    public string itemName;
    public abstract ItemType ItemType
    {
        get;
    }
    public Sprite icon; // Shown in inventory and/or hotbar. Could be the same as hand sprite.

    public abstract Sprite GetSpriteForWorldItem();

    public abstract Vector3Int[] CalculateCellCoverage(Vector3Int topLeft);
}