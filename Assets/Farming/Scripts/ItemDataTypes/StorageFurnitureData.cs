﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/StorageFurnitureData")]
public class StorageFurnitureData : FurnitureData
{

    public override ItemType ItemType
    {
        get
        {
            return ItemType.Storage;
        }
    }

    public Vector2Int inventoryDimensions;
    public bool isLocked;

    //private InventorySystem inventory;

    //public InventorySystem Inventory
    //{
    //    get
    //    {
    //        if (inventory == null)
    //        {
    //            inventory = new InventorySystem(inventoryDimensions.x, inventoryDimensions.y, false);
    //        }
    //        return inventory;
    //    }
    //}

    //private bool isLocked;

    //public bool IsUnlocked()
    //{
    //    return !isLocked;
    //}

    //public void Lock()
    //{
    //    isLocked = true;
    //}

}