﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/SeedItemData")]
public class SeedItemData : UseableItemData
{
    public PlantData resultingPlantData;

    public override ItemType ItemType
    {
        get
        {
            return ItemType.Seed;
        }
    }

    public override bool HandleAction(Vector3Int cellPosition, TerrainTileData tileData, TerrainManager terrainManager)
    {
        if (tileData.type == TerrainType.Tilled || tileData.type == TerrainType.Watered)
        {
            if (!terrainManager.WorldItemExistsAt(cellPosition)) // check for existing seeds or other items.
            {
                terrainManager.CreateSeedItem(cellPosition, this);
                return true;
            }
        }
        return false;
    }

    public override Vector3Int[] CalculateCellCoverage(Vector3Int topLeft)
    {
        throw new System.NotImplementedException();
    }
}
