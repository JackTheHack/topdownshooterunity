﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/StaticObject")]
public class StaticObjectData : ItemData
{
    public ItemType itemType;
    public Vector2Int dimensions;
    public Vector2Int inventoryDimensions;

    public override ItemType ItemType
    {
        get
        {
            return itemType;
        }
    }

    public override Sprite GetSpriteForWorldItem()
    {
        return icon;
    }

    public override Vector3Int[] CalculateCellCoverage(Vector3Int topLeft)
    {
        return TerrainManager.GetCellCoverage(topLeft, dimensions.x, dimensions.y);
    }
}
