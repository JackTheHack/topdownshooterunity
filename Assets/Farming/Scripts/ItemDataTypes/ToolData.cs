﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/ToolData")]
public class ToolData : UseableItemData
{
    public override ItemType ItemType
    {
        get
        {
            return ItemType.Tool;
        }
    }

    public int energyCost;

    public ParticleSystem useEffect;

    public override bool HandleAction(Vector3Int cellPosition, TerrainTileData tileData, TerrainManager terrainManager)
    {
        bool success = false;
        if (interactionType == InteractionType.Smash)
        {
            string posKey = ParserHelper.Vector3IntToString(cellPosition);
            if (terrainManager.GetWorldItems().ContainsKey(posKey))
            {
                // Smashing a world sprite item, e.g. a plant.
                terrainManager.DestroyWorldItem(cellPosition);
                success = true;
            }
            else if (terrainManager.GetWorldTiles().ContainsKey(cellPosition))
            {
                // Smashing a world tile item e.g. a fence.
                ItemData itemToSmash = terrainManager.GetWorldTiles()[cellPosition];
                if (itemToSmash.ItemType == ItemType.Furniture)
                {
                    FurnitureData furnData = itemToSmash as FurnitureData;

                    if (furnData.dropsWhenSmashed)
                    {
                        terrainManager.CreateDroppedItemAt(cellPosition, furnData, 1);
                    }

                    if (furnData.hasCollision)
                    {
                        terrainManager.RemoveCollisionTile(cellPosition);
                    }
                    else
                    {
                        terrainManager.RemoveOverlayTile(cellPosition);
                    }
                    success = true;
                }

            } else
            {
                // Smashing the terrain itself, e.g. turning tilled back to dirt.
                if (tileData.type == TerrainType.Tilled)
                {
                    terrainManager.RevertToDirt(cellPosition);
                    success = true;
                }
            }
        } else if (interactionType == InteractionType.Till)
        {
            if (tileData.type == TerrainType.Dirt)
            {
                terrainManager.TillTile(cellPosition);
                success = true;
            }
        } else if (interactionType == InteractionType.Water)
        {
            if (tileData.type == TerrainType.Tilled)
            {
                terrainManager.WaterTile(cellPosition);
                success = true;
            }
        }

        return success;
    }

    public override Vector3Int[] CalculateCellCoverage(Vector3Int topLeft)
    {
        throw new System.NotImplementedException();
    }
}
