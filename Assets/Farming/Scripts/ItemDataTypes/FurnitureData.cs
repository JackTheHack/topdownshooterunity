﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FarmingItems/FurnitureData")]
public class FurnitureData : UseableItemData
{
    public override ItemType ItemType
    {
        get
        {
            return ItemType.Furniture;
        }
    }

    public bool hasCollision;
    public bool smashable;
    public bool dropsWhenSmashed;
    public Vector2Int dimensions;

    public override bool HandleAction(Vector3Int cellPosition, TerrainTileData tileData, TerrainManager terrainManager)
    {
        if (tileData.type != TerrainType.Water)
        {
            if (!terrainManager.WorldItemExistsAt(cellPosition))
            {
                if (UsesTile)
                {
                    if (hasCollision)
                    {
                        terrainManager.SetCollisionTile(cellPosition, this);
                    }
                    else
                    {
                        terrainManager.SetOverlayTile(cellPosition, this);
                    }
                }
                else
                {
                    terrainManager.CreateWorldItem(CalculateCellCoverage(cellPosition), this, hasCollision, null);
                }

                // Success
                return true;
            }
        }
        // Failure
        return false;
    }

    public override Vector3Int[] CalculateCellCoverage(Vector3Int topLeft)
    {
        return TerrainManager.GetCellCoverage(topLeft, dimensions.x, dimensions.y);
    }

}
