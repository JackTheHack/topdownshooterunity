﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class TerrainTileData : UpdateableData
{

    public TileBase tile;
    public TerrainType type;
    public bool IsTillable;
    public bool IsWalkable;
    [Range(0, 1)]
    public float startHeight;

    public bool IsValidActionType(InteractionType action)
    {
        // Terrain has no bearing on whether item can be consumed or not.
        // This isn't intended to ever be executed.
        if (action == InteractionType.Consume)
        {
            return true;
        }

        // Can place objects on anything except water.
        if (action == InteractionType.Place)
        {
            return type != TerrainType.Water;
        }

        if (type == TerrainType.Grass)
        {
            return false;
        } else if (type == TerrainType.Dirt)
        {
            return action == InteractionType.Till;
        } else if (type == TerrainType.Tilled)
        {
            return action == InteractionType.Smash || action == InteractionType.Water;
        } else if (type == TerrainType.Watered)
        {
            return action == InteractionType.Smash;
        } else if (type == TerrainType.Rock)
        {
            return false;
        }
        return false;
    }

}

public enum InteractionType { Smash, Till, Water, Place, Consume, None }
public enum TerrainType { Grass, Dirt, Tilled, Watered, Rock, Water }