﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PersistentDayData
{

    public static FarmerDateData lastTravelTime;
    public static FarmerDateData yesterday; // will be populated with date and time player went to sleep.
    public static FarmerDateData today;

    public static FarmerDateData AdvanceFromYesterday()
    {
        if (yesterday == null)
        {
            yesterday = new FarmerDateData();
        } else
        {
            yesterday.AdvanceDayToDayStartHour();
            
        }
        today = new FarmerDateData(yesterday.day, yesterday.season, yesterday.year);
        return yesterday;
    }


    public static List<SellRecord> soldItems = new List<SellRecord>();

    public static void AddSoldItem(SellRecord sold)
    {
        soldItems.Add(sold);
    }


}

public enum SceneLoadReason { Invalid, StartOfGame, NewDay, Travel }

public static class SceneLoadData
{
    // Used along with time went to sleep to determine how much we should restore the health/stamina.
    public static int farmerHealth;
    public static int farmerStamina;

    public static SceneLoadReason sceneLoadReason;
}