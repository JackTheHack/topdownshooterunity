﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    ProjectileController baseController;
    Vector2 velocity = Vector2.zero;
    Rigidbody2D rb2d;

    public void SetVelocity(Vector2 velocity)
    {
        this.velocity = velocity;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        Vector3 pos = transform.position;
        pos.z = -5;
        transform.position = pos;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position += ;
        rb2d.MovePosition(rb2d.position + (velocity * Time.deltaTime));
    }

    public void BeShot(ProjectileController baseController, Vector2 inheritedVelocity)
    {
        velocity = inheritedVelocity;
        this.baseController = baseController;
    }

}
