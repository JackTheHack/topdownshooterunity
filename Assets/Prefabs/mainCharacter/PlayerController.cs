﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject
{

    public UIManager uimanager;

    Animator animController;
    public Animator handAnimator;

    Vector2 movement = Vector2.zero;

    SpriteRenderer spriteRenderer;
    //float lastHorizontalVal = 0;

    [SerializeField]
    private float MaxMovementSpeed;
    //[SerializeField]
    //private float JumpTakeOffSpeed;


    public Weapon currentWeapon;

    private float actualScale = 1;

    public bool facingRight { get; private set; }

    LayerMask wallLayerMask;

    bool stoppedRight;
    bool stoppedLeft;
    bool stoppedUp;
    bool stoppedDown;

    bool isInvincible = false;

    Vector2 previousMove = Vector2.zero;
    [SerializeField]
    private float DodgeSpeedMultiplier;

    ParticleSystem dodgeParticles;

    DamageController damageController;

    LayerMask damageLayerMask;

    public GameObject leftHand;
    public GameObject rightHand;

    private Vector2 leftHandStartPosLocal;
    private Vector2 rightHandStartPosLocal;

    // Use this for initialization
    new void Start ()
    {
        base.Start();
        wallLayerMask = LayerMask.NameToLayer("SolidWall");
        rb2d = GetComponent<Rigidbody2D>();
        animController = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        facingRight = true;

        FullAmmoUIUpdate();

        dodgeParticles = GetComponent<ParticleSystem>();
        dodgeParticles.Stop();

        damageController = GetComponent<DamageController>();

        damageLayerMask = LayerMask.GetMask(new string[] { "EnemyTrigger", "EnemyBullet" });

        leftHandStartPosLocal = leftHand.transform.localPosition;
        rightHandStartPosLocal = rightHand.transform.localPosition;

        handAnimator.SetFloat("SpeedMult", currentWeapon.GetShootAnimSpeedMult());

        rightHand.GetComponentInChildren<SpriteRenderer>().enabled = !currentWeapon.IsOneHanded;
    }

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animController = GetComponent<Animator>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        if (animController.GetBool("InDodge"))
        {
            if (previousMove == Vector2.zero)
            {
                move.x = 1;
                move.y = 0;
            }
            else
            {
                move = previousMove;
            }
            targetVelocity = move.normalized * MaxMovementSpeed * DodgeSpeedMultiplier;
        }
        else
        {
            move.x = Input.GetAxisRaw("Horizontal");
            move.y = Input.GetAxisRaw("Vertical");
            animController.SetFloat("velocityX", Mathf.Abs(velocity.magnitude) / MaxMovementSpeed);
            targetVelocity = move * MaxMovementSpeed;
        }

        previousMove = move;

    }

    private new void Update()
    {
        base.Update();
        if (Input.GetMouseButton(0))
        {
            bool success = currentWeapon.RequestShot();
            if (success)
            {
                handAnimator.SetTrigger("Shoot");
            }
        }
        if (Input.GetMouseButton(1))
        {
            bool success = TryDodge();
            if (success)
            {

            }
        }
        if (Input.GetButtonDown("Reload"))
        {
            bool success = currentWeapon.RequestReload();
            if (success)
            {
                handAnimator.SetTrigger("Reload");
            }

            //Door door = FindObjectOfType<Door>();
            //door.NextState();

        }

        RotateHandsToMouse();

    }

    private void RotateHandsToMouse()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5f;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;

        

        float angleRad = Mathf.Atan2(mousePos.y, mousePos.x);
        float angleDeg = angleRad * Mathf.Rad2Deg;
        leftHand.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleDeg));
        rightHand.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleDeg));

        if (angleDeg > 80 || angleDeg < -100)
        {
            SetFacingLeft();
        }
        else if (angleDeg < 100 || angleDeg > -80)
        {
            SetFacingRight();
        }

        leftHand.transform.localPosition = RotateAroundPoint(facingRight ? leftHandStartPosLocal : -leftHandStartPosLocal, facingRight ? angleRad : -angleRad, Vector2.zero);
        rightHand.transform.localPosition = RotateAroundPoint(facingRight ? rightHandStartPosLocal : -rightHandStartPosLocal, facingRight ? angleRad : -angleRad, Vector2.zero);

    }

    private Vector2 RotateAroundPoint(Vector2 point, float angle, Vector2 pivotPoint)
    {
        float s = Mathf.Sin(angle);
        float c = Mathf.Cos(angle);

        Vector2 workingPoint = new Vector2(point.x, point.y);

        workingPoint.x -= pivotPoint.x;
        workingPoint.y -= pivotPoint.y;

        float newx = (workingPoint.x * c) - (workingPoint.y * s);
        float newy = (workingPoint.x * s) + (workingPoint.y * c);

        workingPoint.x = newx + pivotPoint.x;
        workingPoint.y = newy + pivotPoint.y;

        return workingPoint;
    }

    public void MagazineUIUpdate()
    {
        uimanager.UpdateMagazineCount(currentWeapon.bulletsInMagazine);
    }

    public void ReserveAmmoUIUpdate()
    {
        uimanager.UpdateTotalAmmoCount(currentWeapon.reserveAmmoCount);
    }

    public void FullAmmoUIUpdate()
    {
        MagazineUIUpdate();
        ReserveAmmoUIUpdate();
    }

    //public void ReloadUIUpdate()
    //{
    //    uimanager.DoReload(currentWeapon.bulletsInMagazine, currentWeapon.reserveAmmoCount);
    //}

    public void SetFacingLeft()
    {
        Vector3 playerScale = transform.localScale;
        playerScale.x = actualScale * -1;
        transform.localScale = playerScale;

        //foreach (Transform child in transform)
        //{
        //    Vector3 childScale = transform.localScale;
        //    //childScale.x *= -1;
        //    childScale.y *= -1;
        //    child.transform.localScale = childScale;

        //}

        Vector3 handScale = transform.localScale;
        handScale.x = actualScale * -1;
        handScale.y = actualScale * -1;
        leftHand.transform.localScale = handScale;
        rightHand.transform.localScale = handScale;

        //currentWeapon.transform.localScale = playerScale;


        //leftHand.transform.localScale = playerScale;
        //rightHand.transform.localScale = playerScale;

        facingRight = false;
    }

    public void SetFacingRight()
    {
        Vector3 playerScale = transform.localScale;
        playerScale.x = actualScale;
        transform.localScale = playerScale;

        Vector3 handScale = transform.localScale;
        leftHand.transform.localScale = handScale;
        rightHand.transform.localScale = handScale;

        //foreach (Transform child in transform)
        //{
        //    child.transform.localScale = playerScale;
        //}

        //Vector3 handScale = transform.localScale;
        //handScale.y = actualScale * -1;
        //leftHand.transform.localScale = handScale;
        //rightHand.transform.localScale = handScale;

        //currentWeapon.transform.localScale = playerScale;

        //foreach (Transform child in transform)
        //{
        //    Vector3 childScale = transform.localScale;
        //    childScale.x *= -1;
        //    transform.localScale = theScale;
        //}


        //leftHand.transform.localScale = playerScale;
        //rightHand.transform.localScale = playerScale;

        facingRight = true;
    }

    private bool TryDodge()
    {
        if (!animController.GetBool("InDodge"))
        {
            CancelInvoke("ExitInvincibilityState");
            animController.SetBool("InDodge", true);
            EnterInvincibilityState();
            
            return true;
        }
        
        return false;
    }

    public void PlayDodgeParticles()
    {
        dodgeParticles.Play();
    }

    public void StopDodgeParticles()
    {
        dodgeParticles.Stop();
    }

    private void EnterInvincibilityState()
    {
        isInvincible = true;
    }

    public void ExitInvincibilityState()
    {
        isInvincible = false;
    }

    public void EnterInvincibilityStateForTime(float seconds)
    {
        EnterInvincibilityState();
        Invoke("ExitInvincibilityState", seconds);
    }

    public void SwitchWeapon(Weapon w)
    {

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision + "  " + Time.frameCount);
        Debug.Log("plaeyr : " + (damageLayerMask.value & 1 << collision.gameObject.layer));
        // check for damage
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (isInvincible)
        {
            // skip damage
        }
        else
        {

            if ((damageLayerMask.value & 1 << collision.gameObject.layer) != 0)
            {
                damageController.TakeDamage(new DamageController.DamageInfo(1, "Physical"));
                uimanager.UpdateHealthText(damageController.GetHealth());

                EnterInvincibilityStateForTime(0.8f);
            }
            //if (collision.gameObject.layer == LayerMask.NameToLayer("EnemyBullet"))
            //{

            //}
            // do damage
        }
    }

    //private void EndDodge()
    //{
    //    animController.SetBool("InDodge", false);
    //}

    //IEnumerator DodgeCoroutine()
    //{
    //    for (float f = 1f; f >= 0; f -= 0.1f)
    //    {
    //        animController.
    //        yield return null;
    //    }
    //}

    // Update is called once per frame
    //void Update () 
    //   {
    //       movement.x = Input.GetAxisRaw("Horizontal");
    //       movement.y = Input.GetAxisRaw("Vertical");

    //       //if (lastHorizontalVal <= 0 && movement.x > 0)
    //       //{
    //       //    SetFacingLeft();    
    //       //}
    //       //else if (lastHorizontalVal >= 0 && movement.x < 0)
    //       //{
    //       //    SetFacingRight();
    //       //}
    //       velocity = movement.normalized * Time.fixedDeltaTime * MovementSpeed;
    //       animController.SetBool("walking", Mathf.Abs(movement.x) > 0 || Mathf.Abs(movement.y) > 0);

    //       //transform.position += (Vector3);

    //       //transform.Translate();

    //           if (Input.GetMouseButton(0)) {
    //               bool success = currentWeapon.RequestShot();
    //}

    //       //lastHorizontalVal = movement.x;

    //}

    //private void FixedUpdate()
    //{

    //    rb2d.MovePosition((Vector2)transform.position + velocity);
    //}

    //void Flip()
    //{
    //    // Multiply the player's x local scale by -1
    //    Vector3 theScale = transform.localScale;
    //    theScale.x *= -1;
    //    transform.localScale = theScale;
    //}

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Debug.Log(collision.gameObject);
    //    if (collision.gameObject.layer == wallLayerMask)
    //    {
    //        ContactPoint2D contactPoint = collision.GetContact(0);
    //        // Determine direction player walked into wall.
    //        string direction = string.Empty;

    //        float playerAngle = transform.eulerAngles.z; // this probably won't change.

    //        float projection = Vector2.Dot(velocity, contactPoint.normal);
    //        if (projection < 0)
    //        {
    //            velocity = velocity - (projection * contactPoint.normal);
    //        }

    //        Vector2 unRotatedNormal = Quaternion.Euler(0, 0, -playerAngle) * contactPoint.normal;
    //        Debug.Log(velocity);
    //        unRotatedNormal.x = Mathf.Round(unRotatedNormal.x);
    //        unRotatedNormal.y = Mathf.Round(unRotatedNormal.y);

    //        if (Vector2.left == unRotatedNormal)
    //        {
    //            stoppedRight = true;
    //            Debug.Log("Stopped on right side.");
    //        }
    //        if (Vector2.right == unRotatedNormal)
    //        {
    //            stoppedLeft = true;
    //            Debug.Log("Stopped on left side.");
    //        }
    //        if (Vector2.up == unRotatedNormal)
    //        {
    //            stoppedDown = true;
    //            Debug.Log("Stopped on bottom side.");
    //        }
    //        if (Vector2.down == unRotatedNormal)
    //        {
    //            stoppedUp = true;
    //            Debug.Log("Stopped on top side.");
    //        }
    //    }
    //}

    //public static string DirectionToString(Vector2 direction)
    //{
    //    if (direction == Vector2.up)
    //    {
    //        return "UP";
    //    }
    //    else if (direction == Vector2.down)
    //    {
    //        return "DOWN";
    //    }
    //    else if (direction == Vector2.right)
    //    {
    //        return "RIGHT";
    //    }
    //    else if (direction == Vector2.left)
    //    {
    //        return "LEFT";
    //    }
    //    else
    //    {
    //        return "INCONCLUSIVE";
    //    }
    //}
}

