﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using csDelaunay;

public class MinimumSpanningTree
{
    int sitesCount;
    List<JacksEdge> edges;

    public JacksEdge[] result { get; private set; }

    public MinimumSpanningTree(List<Edge> edges, int sitesCount)
    {
        this.sitesCount = sitesCount;
        this.edges = new List<JacksEdge>();

        foreach (Edge e in edges)
        {
            this.edges.Add(new JacksEdge(e));
        }


    }

    class Subset
    {
        public int parent;
        public int rank;
    }

    private void Union(List<Subset> subsets, int x, int y)
    {
        int xroot = Find(subsets, x);
        int yroot = Find(subsets, y);

        if (subsets[xroot].rank < subsets[yroot].rank)
        {
            subsets[xroot].parent = yroot;
        } else if (subsets[xroot].rank > subsets[yroot].rank)
        {
            subsets[yroot].parent = xroot;
        } else
        {
            subsets[yroot].parent = xroot;
            subsets[xroot].rank++;
        }
    }


    public JacksEdge[] KruskalsMinSpanTree()
    {
        int V = sitesCount;

        if (V == 0)
        {
            return new JacksEdge[0];
        }

        result = new JacksEdge[V-1];

        int e = 0;
        int i = 0;
        for (i = 0; i < result.Length; i++)
        {
            result[i] = new JacksEdge();
        }

        edges.Sort( (e1,e2) => e1.SitesDistance.CompareTo(e2.SitesDistance) );

        List<Subset> subsets = new List<Subset>();

        for (i = 0; i < V; i++)
        {
            subsets.Add(new Subset());
        }

        for (int v = 0; v < V; ++v)
        {
            subsets[v].parent = v;
            subsets[v].rank = 0;
        }


        i = 0;

        while (e < V - 1)
        {
            JacksEdge nextEdge = edges[i++];

            int x = Find(subsets, nextEdge.srcSiteIndex);
            int y = Find(subsets, nextEdge.destSiteIndex);

            if (x != y)
            {
                result[e++] = nextEdge;
                Union(subsets, x, y);
            }

        }

        return result;
    }

    static int Find(List<Subset> subsets, int i)
    {
        if (subsets[i].parent != i)
        {
            subsets[i].parent = Find(subsets, subsets[i].parent);
        }
        return subsets[i].parent;
    }

}
