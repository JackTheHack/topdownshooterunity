﻿using UnityEngine;
using System.Collections;
using csDelaunay;

public class JacksEdge
{

    public Vector2f src;
    public Vector2f dest;

    public int srcSiteIndex;
    public int destSiteIndex;

    public float SitesDistance
    {
        get { return Mathf.Sqrt(Vector2f.DistanceSquare(src, dest)); }
    }

    public JacksEdge(Vector2f src, Vector2f dest, int srcSiteIndex, int destSiteIndex)
    {
        this.src = src;
        this.dest = dest;

        this.srcSiteIndex = srcSiteIndex;
        this.destSiteIndex = destSiteIndex;
    }

    public JacksEdge(Edge e)
    {
        this.src = e.LeftSite.Coord;
        this.dest = e.RightSite.Coord;

        this.srcSiteIndex = e.LeftSite.SiteIndex;
        this.destSiteIndex = e.RightSite.SiteIndex;
    }

    public JacksEdge()
    {

    }
}
