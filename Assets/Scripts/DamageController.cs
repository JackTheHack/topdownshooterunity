﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour {

    [SerializeField]
    private int Health;


    public int GetHealth()
    {
        return Health;
    }

    public void TakeDamage(DamageInfo damageStruct)
    {
        Health -= damageStruct.amount;
        Debug.Log("Dealt " + damageStruct.amount + " " + damageStruct.type + " damage to " + gameObject);
        if (Health <= 0)
        {
            Health = 0;
            Kill();
        }

        //SendMessage("Damage", damageStruct);
    }

    private void Kill()
    {
        gameObject.SetActive(false);
    }

    public struct DamageInfo
    {
        public int amount;
        public string type;

        public DamageInfo(int amount, string type)
        {
            this.amount = amount;
            this.type = type;
        }
    }

}
