﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    public TextMeshProUGUI magazineCountText;
    public TextMeshProUGUI totalAmmoCountText;

    public Image reloadBar;

    [SerializeField]
    private Transform reloadBarFollowTarget;

    public void SetReloadBarFollowTarget(Transform t)
    {
        reloadBarFollowTarget = t;
    }

    public TextMeshProUGUI healthText;

    // Start is called before the first frame update
    void Start()
    {
        reloadBar.color = Color.clear;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateMagazineCount(int newCount)
    {
        magazineCountText.text = newCount + " |";
    }

    public void UpdateTotalAmmoCount(int newCount)
    {
        totalAmmoCountText.text = newCount.ToString();
    }

    public void DoReloadUIUpdate(int newMagazineCount, int newReserveAmmoCount)
    {
        UpdateMagazineCount(newMagazineCount);
        UpdateTotalAmmoCount(newReserveAmmoCount);
    }

    public void UpdateHealthText(int newHealth)
    {
        healthText.text = newHealth.ToString();
    }

    public void StartReloadBarCoroutine(Weapon.ReloadInfo reloadInfo)
    {
        StartCoroutine("RunReloadBarAnim", reloadInfo);
    }

    public IEnumerator RunReloadBarAnim(Weapon.ReloadInfo reloadInfo)
    {
        reloadBar.color = Color.white;
        for (float f = reloadInfo.reloadTime; f >= 0; f -= Time.deltaTime)
        {
            Vector2 barPos = Camera.main.WorldToScreenPoint(reloadBarFollowTarget.position);
            barPos.y += 30f;
            reloadBar.rectTransform.position = barPos;

            reloadBar.fillAmount = f / reloadInfo.reloadTime;
            yield return null;
        }
        reloadBar.color = Color.clear;
    }
}
