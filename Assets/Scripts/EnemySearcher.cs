﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using System.Diagnostics;

public class EnemySearcher : MonoBehaviour
{

    PathRequestManager requestManager;
    public Grid tilemapGrid;
    CreateSearchNodesFromTilemap grid;
    public Dictionary<Vector2Int, SearchNode> nodes;

    public Transform seeker, target;

    private void Awake()
    {
        requestManager = GetComponent<PathRequestManager>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump")) 
        {
            FindPath(seeker.position, target.position);    
        }
    }

    private void Start()
    {
        grid = GetComponent<CreateSearchNodesFromTilemap>();

        nodes = grid.nodes;
    }

    public void StartFindPath(Vector3 startPos, Vector3 endPos)
    {
        StartCoroutine(FindPath(startPos, endPos));
    }


    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {

        Stopwatch sw = new Stopwatch();
        sw.Start();

        Vector3[] waypoints = new Vector3[0];
        bool pathSuccess = false;

        Vector2Int startCellPos = (Vector2Int) tilemapGrid.WorldToCell(startPos);
        Vector2Int targetCellPos = (Vector2Int) tilemapGrid.WorldToCell(targetPos);

        SearchNode startNode = null;
        SearchNode targetNode = null;

        if (!nodes.TryGetValue(startCellPos, out startNode) || !nodes.TryGetValue(targetCellPos, out targetNode))
        {
            yield return null;
        }

        if (startNode.walkable && targetNode.walkable) {
            Heap<SearchNode> openSet = new Heap<SearchNode>(grid.MaxSize);
            HashSet<SearchNode> closedSet = new HashSet<SearchNode>();

            openSet.Add(startNode);



            while (openSet.Count > 0)
            {
                SearchNode currentNode = openSet.RemoveFirst();
                //for (int i = 1; i < openSet.Count; i++)
                //{
                //    if (openSet[i].fCost < currentNode.fCost || 
                //        openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                //    {
                //        currentNode = openSet[i];
                //    }
                //}

                //openSet.Remove(currentNode);
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    sw.Stop();
                    print("Path found: " + sw.ElapsedMilliseconds + "ms");
                    pathSuccess = true;
                    break; // we found it.
                }



                foreach (SearchNode neighbour in currentNode.myNeighbours)
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                    if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {

                        neighbour.gCost = newMovementCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                    }
                }
            }

        }

        yield return null;

        if (pathSuccess)
        {
            waypoints = RetracePath(startNode, targetNode);    
        }
        requestManager.FinishedProcessingPath(waypoints, pathSuccess);

    }

    Vector3[] RetracePath(SearchNode startNode, SearchNode endNode)
    {
        List<SearchNode> path = new List<SearchNode>();
        SearchNode currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);

        return waypoints;
    }

    Vector3[] SimplifyPath(List<SearchNode> path) 
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;

        for (int i = 1; i < path.Count; i++) 
        {
            Vector2 directionNew = new Vector2(path[i - 1].nodeArrayX - path[i].nodeArrayX,
                                               path[i-1].nodeArrayY - path[i].nodeArrayY);
            if (directionNew != directionOld)
            {
                waypoints.Add(path[i].worldPosition);
                directionOld = directionNew;
            }
        }

        return waypoints.ToArray();
    }

    int GetDistance(SearchNode a, SearchNode b)
    {
        int dstX = Mathf.Abs(a.tilemapPos.x - b.tilemapPos.x);
        int dstY = Mathf.Abs(a.tilemapPos.y - b.tilemapPos.y);

        if (dstX > dstY)
        {
            return 14 * dstY + 10 * (dstX - dstY);
        }
        else
        {
            return 14 * dstX + 10 * (dstY - dstX);
        }
    }

}
