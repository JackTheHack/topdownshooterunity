﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    [SerializeField]
    private Transform target;

    [SerializeField]
    private float CameraSpeed;

    [SerializeField]
    private float DeadZoneSize;

    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        if (target != null)
        {
            Vector3 newPos = target.transform.position;
            newPos.z = -10;

            transform.position = newPos;
        }
    }

    // Update is called once per frame
    void LateUpdate () {
        if (target != null)
        {

            Vector2 mousePosFromCentre = Camera.main.ScreenToWorldPoint(Input.mousePosition) - target.transform.position;

            if (mousePosFromCentre.sqrMagnitude < DeadZoneSize)
            {
                mousePosFromCentre = Vector2.zero;
            } else
            {
                mousePosFromCentre = Vector2.ClampMagnitude(mousePosFromCentre, Camera.main.orthographicSize * 0.5f);
            }

            
            Vector3 newPos = Vector3.SmoothDamp (transform.position, target.transform.position + (Vector3)mousePosFromCentre, ref velocity, Time.deltaTime * 0.01f, CameraSpeed);
            newPos.z = -10;

            //Debug.Log(Camera.main.WorldToScreenPoint(target.transform.position) + "   " + mousePosFromCentre);

            transform.position = newPos;

        }
	}

    public void SetFollowTarget(Transform t)
    {
        target = t;
    }
}
