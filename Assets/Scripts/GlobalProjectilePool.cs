﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalProjectilePool : MonoBehaviour {

    ObjectPool projectilePool;

	// Use this for initialization
	void Start () {

        projectilePool = GetComponent<ObjectPool>();

        projectilePool.AddNewPoolType<Bullet>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
