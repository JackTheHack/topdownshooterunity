﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public FloorGenerator floorGen;
    public PlayerController playerCharPrefab;

    // Start is called before the first frame update
    void Start()
    {
        floorGen.GenerateFloor();
        StartCoroutine(WaitForFloorGen());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator WaitForFloorGen()
    {
        while (!floorGen.InValidGenerationState)
        {
            yield return null;
        }
        SpawnPlayer();
    }

    void SpawnPlayer()
    {
        PlayerController player = Instantiate(playerCharPrefab, floorGen.GetSpawnPoint(), Quaternion.identity);
        player.uimanager = GameObject.Find("UIManager").GetComponent<UIManager>();
        player.uimanager.SetReloadBarFollowTarget(player.transform);
        Camera.main.GetComponent<FollowCamera>().SetFollowTarget(player.transform);

        player.FullAmmoUIUpdate();
    }
}
