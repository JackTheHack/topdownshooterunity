﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class Searcher<T>
{

    // An object containing state information, initial and goal state, and an action map for all states.
    private readonly Problem<T> problem;

    // The cost function for the search.
    private readonly Func<State<T>, State<T>, Double> cost;

    // Contains method references to specify in what order nodes should be taken and added to the frontier, and the data
    // structure of the frontier itself.
    // This allows the search to work in a e.g. breadth or depth first order with no changes to the actual algorithm.
    private readonly EvaluationOrder<ICollection<Node<T>>, Node<T>> evalOrder;

    private ICollection<Node<T>> frontier;

    // To log the frontier in its actual order when its a priority queue we have to track the order
    // elements were inserted so we can actually remove them, log them, and reinsert them properly.
    private LinkedList<Node<T>> frontierAdditionOrder;

    // All the distinct states that end up in explored set and frontier.
    HashSet<State<T>> exploredSetForLogging = new HashSet<State<T>>();
    HashSet<State<T>> frontierForLogging = new HashSet<State<T>>();

    // Used for timestamps for tie-breaks, to see which node is younger.
    private int currentTime = 0;

    StreamWriter outputWriter;

    Searcher(Problem<T> p, Func<State<T>, State<T>, Double> cost, EvaluationOrder<ICollection<Node<T>>, Node<T>> evalOrder, StreamWriter outputWriter)
    {
        this.problem = p;
        this.cost = cost;
        this.evalOrder = evalOrder;
        this.frontier = evalOrder.frontier;
        this.frontierAdditionOrder = new LinkedList<Node<T>>();
        this.outputWriter = outputWriter;
    }

    private Node<T> MakeNode(Node<T> parent, Move<T> action, State<T> state)
    {
        currentTime += 1;
        if (parent != null)
        {
            return new Node<T>(state, parent, action, parent.depth + 1, parent.pathCost + cost(parent.state, state), currentTime);
        }
        else
        {
            return new Node<T>(state, null, action, 0, 0, currentTime);
        }
    }

    private bool GoalTest(State<T> state, State<T> goal)
    {
        return state.Equals(goal);
    }

    /**
     * Expands a node in the search tree by finding descendants.
     * @param node The node to be expanded.
     * @param problem The search problem.
     * @return The set of descendant nodes.
     */
    private List<Node<T>> Expand(Node<T> node, Problem<T> problem, HashSet<State<T>> exploredSet)
    {
        // Get actions going FROM node.
        List<Move<T>> moves = problem.getMoves(node.state);

        // For each action from "node", make a node with "node" as parent,
        // action as action that arrives at node, and action.to as the state.
        //List<Node<T>> children = moves
        //                        .map(move => MakeNode(node, move, move.getTo()));

        List<Node<T>> children = new List<Node<T>>();
        foreach (Move<T> move in moves)
        {
            children.Add(MakeNode(node, move, move.getTo()));
        }


        // ======================== Logging ================================= //
        outputWriter.Write("\n\n=========\nNode being expanded: " + node.state.contents);
        outputWriter.Write("\nChildren that were expanded from node ():");

        children.ForEach(n=>outputWriter.Write("\n" + n.move.getFrom().contents
                + " -> " + n.move.getTo().contents
                + "  Action Cost: " + cost(n.move.getFrom(), n.move.getTo())
                + "  Path cost (from initial): " + n.pathCost
                + "  Estimated cost to goal: " + cost(n.state, problem.getGoalState())
                + "     Est. cost to goal + path cost: " + (cost(n.state, problem.getGoalState()) + n.pathCost)));
        // ================================================================== //

        // If the child node is already in the frontier with a larger path cost we should replace it (for A*)
        // but it shouldn't hurt the other algorithms.
        // Just gather a set of the new nodes for now, we will add them to children after removing the current frontier versions.
        HashSet<Node<T>> inFrontierAndMoreExpensive = new HashSet<Node<T>>();
        foreach (Node<T> child in children)
        {
            foreach (Node<T> fchild in frontier)
            {
                if (child.state.Equals(fchild.state) && fchild.pathCost > child.pathCost)
                {
                    inFrontierAndMoreExpensive.Add(child);
                }
            }
        }

        //======================== Logging ===============================//
        outputWriter.Write("\ninFrontierAndMoreExpensive\n");
        //inFrontierAndMoreExpensive.ForEach(n=>outputWriter.write(n.state.toString() + " "));
        foreach (Node<T> n in inFrontierAndMoreExpensive)
        {
            outputWriter.Write(n.state.ToString() + " ");
        }
        //================================================================//


        // Remove nodes already in frontier or explored set.

        // Can't simply use contains() for notInFrontier because we need to compare States rather than Nodes.
        Predicate<Node<T>> notInExploredSet = x => !exploredSet.Contains(x.state);
        Predicate<Node<T>> notInFrontier = x => !frontier.Select(n => n.state).ToList().Contains(x.state);
        //Predicate < Node < T >> notInFrontier = x => !frontier.stream().map(y->y.state).collect(Collectors.toList()).contains(x.state); // Java8

        children = children.FindAll(x => notInExploredSet(x) && notInFrontier(x)).ToList();

        //children = children.stream()
        //        .filter(notInExploredSet.and(notInFrontier))
        //        .collect(Collectors.toList()); // Java8

        // Re-add the nodes that were in the frontier but with larger path costs that we gathered before.
        children.AddRange(inFrontierAndMoreExpensive);

        return children;
    }

    // Based on general search algorithm from lecture notes, unused in this program.
    //    public SearchResult TreeSearch() {
    //
    //        Node initial = MakeNode(null, null, problem.getInitialState());
    //        frontier.add(initial);
    //
    //        List<Node> resultPath = new ArrayList<>();
    //        resultPath.add(initial);
    //
    //        int statesExpanded = 0;
    //
    //        while (frontier.size() > 0) {
    //
    //            Node removed = evalOrder.take.apply(frontier);
    //
    //            if (GoalTest(removed.state, problem.getGoalState())) {
    //                return new SearchResult(true, resultPath, statesExpanded);
    //            } else {
    //                List<Node> children = Expand(removed, problem);
    //                statesExpanded += 1;
    //                for (Node child : children) {
    //                    evalOrder.put.accept(frontier, child);
    //                }
    //                frontier.addAll(Expand(removed, problem));
    //                resultPath.add(removed);
    //            }
    //        }
    //        return new SearchResult(false, null, statesExpanded);
    //    }

    /**
     * Performs an uninformed search from initial state to goal state in problem.
     * @return On success returns the goal state and result path taken. On failure returns an empty result object.
     */
    SearchResult<T> Search()
    {

        outputWriter.Write("\nInitial State: " + problem.getInitialState().contents);
        outputWriter.Write("\nGoal State: " + problem.getGoalState().contents);

        Node<T> initial = MakeNode(null, null, problem.getInitialState()); // no parent or action for initial node.

        List<Node<T>> resultPath = new List<Node<T>>();
        int statesExpanded = 0;

        if (GoalTest(initial.state, problem.getGoalState()))
        {
            resultPath.Add(initial);
            return new SearchResult<T>(true, resultPath, statesExpanded, problem);
        }

        evalOrder.put(frontier, initial);
        frontierAdditionOrder.AddLast(initial);

        HashSet<State<T>> exploredSet = new HashSet<State<T>>();

        while (frontier.Count > 0)
        {

            // Apply the evaluation order take function which will remove a node from frontier.
            Node<T> removed = evalOrder.take(frontier);
            frontierAdditionOrder.Remove(removed);
            exploredSet.Add(removed.state);
            resultPath.Add(removed);
            List<Node<T>> children = Expand(removed, problem, exploredSet);
            statesExpanded += 1;


            // =============== Logging ==================
            outputWriter.Write("\nExplored Set (Nodes that have already been expanded, frontier can't contain these):\n");
            //exploredSet.ForEach(n=>outputWriter.write(n.contents + " "));
            foreach (State<T> node in exploredSet)
            {
                outputWriter.Write(node.contents + " ");
            }
            outputWriter.Write("\n");
            // ==========================================

            foreach (Node<T> child in children)
            {

                if (GoalTest(child.state, problem.getGoalState()))
                {
                    resultPath.Add(child);
                    outputWriter.Write("\n\nFound the goal state: " + child.state.contents + ", building result path.");
                    exploredSetForLogging = exploredSet;
                    return new SearchResult<T>(true, resultPath, statesExpanded, problem);
                }
                // Add child to frontier either at front or end according to evalOrder.
                evalOrder.put(frontier, child);
                frontierAdditionOrder.AddLast(child);
                frontierForLogging.Add(child.state);
            }

            // ============================== Frontier  logging ======================================================
            // Logging the frontier in order, this is unfortunately an inefficient operation
            // where I have to actually remove things from a copy of the frontier and re-add them due to the
            // searches that use a priority queue. This is the only way to get the ordering behaviour
            // of a priority queue as simply iterating will use an iterator with an undefined ordering.
            outputWriter.Write("\n\nThe current frontier:");

            int frontierSize = frontier.Count;
            Node<T>[] temp = new Node<T>[frontierSize];
            for (int i = 0; i < frontierSize; i++)
            {
                Node<T> node = evalOrder.take(frontier);
                outputWriter.Write("\n" + node.state.ToString());
                temp[i] = node;
            }
            foreach (Node<T> n in frontierAdditionOrder)
            {
                evalOrder.put(frontier, n);
            }
            // ============================== End of frontier logging ================================================

        }
        exploredSetForLogging = exploredSet;
        return new SearchResult<T>(false, null, statesExpanded, problem);
    }

}

class EvaluationOrder<T, E>
{

    public readonly Func<T, E> take;
    public readonly Action<T, E> put;
    public readonly T frontier;

    /**
     * Defines type of and operations on a search frontier.
     * @param take A function that removes a node from the frontier e.g. Deque.pollFirst();
     * @param put A function to add a node to the frontier e.g. List.addLast();
     * @param frontier The concrete data structure to hold the frontier.
     */
    public EvaluationOrder(Func<T, E> take, Action<T, E> put, T frontier)
    {
        this.take = take;
        this.put = put;
        this.frontier = frontier;
    }
}

class SearchResult<T>
{

    // Important results.
    public readonly bool succeeded;
    public readonly List<Node<T>> resultPath;
    public Problem<T> problem;

    // Performance data
    public int statesExpanded = 0;

    public SearchResult(bool succeeded, List<Node<T>> resultPath, int statesExpanded, Problem<T> problem)
    {
        this.succeeded = succeeded;
        this.resultPath = resultPath;

        this.statesExpanded = statesExpanded;
        this.problem = problem;
    }
}

class Node<T>
{

    public readonly State<T> state;
    public readonly Node<T> parent;
    public Move<T> move;
    public int depth = 0;
    public double pathCost = 0;
    public int timestamp;

    /**
     *
     * @param state The state represented by this node.
     * @param parent This node's predecessor.
     * @param depth The distance to this node from the root node.
     * @param pathCost The cost of moving from the root node to this node.
     */
    public Node(State<T> state, Node<T> parent, Move<T> move, int depth, double pathCost, int timestamp)
    {
        this.state = state;
        this.parent = parent;
        this.move = move;
        this.depth = depth;
        this.pathCost = pathCost;
        this.timestamp = timestamp;
    }
}

class State<T>
{

    public readonly T contents;

    State(T contents)
    {
        this.contents = contents;
    }

    public override bool Equals(object value)
    {
        if (Object.ReferenceEquals(null, value))
        {
            return false;
        }

        if (Object.ReferenceEquals(this, value))
        {
            return true;
        }

        if (value.GetType() != this.GetType())
        {
            return false;
        }

        return IsEqual((State<T>) value);
    }

    public bool Equals(State<T> state)
    {
        if (Object.ReferenceEquals(null, state))
        {
            return false;
        }

        if (Object.ReferenceEquals(this, state))
        {
            return true;
        }

        return IsEqual(state);
    }

    private bool IsEqual(State<T> state)
    {
        return contents != null ? contents.Equals(state.contents) : state.contents == null;
    }
   
    public override int GetHashCode()
    {
        return contents != null ? contents.GetHashCode() : 0;
    }

    
    public override String ToString()
    {
        return "State::" + contents;
    }

    public static double getManhattanDistance(Dictionary<State<T>, Point> stateLocationMap, State<T> from, State<T> to)
    {
        Point fromCoord = stateLocationMap[from];
        Point toCoord = stateLocationMap[to];

        return Math.Abs(fromCoord.x - toCoord.x) + Math.Abs(fromCoord.y - toCoord.y);
    }

    public static double getEuclideanDistance(Dictionary<State<T>, Point> stateLocationMap, State<T> from, State<T> to)
    {
        Point fromCoord = stateLocationMap[from];
        Point toCoord = stateLocationMap[to];

        return Math.Sqrt(Math.Pow(fromCoord.x - toCoord.x, 2) + Math.Pow(fromCoord.y - toCoord.y, 2));
    }
}

/**
 * Action applied to nodes to move between states.
 */
class Move<T>
{

    public readonly State<T> from;
    public readonly State<T> to;

    /**
     *
     * @param from The state that this action moves from.
     * @param to The state this action moves to.
     */
    Move(State<T> from, State<T> to)
    {
        this.from = from;
        this.to = to;
    }

    public State<T> getFrom()
    {
        return from;
    }

    public State<T> getTo()
    {
        return to;
    }

    public String toString()
    {
        return "Action::" + from.contents + " -> " + to.contents;
    }

    public static double getManhattanDistance(Dictionary<State<T>, Point> stateLocationMap, Move<T> action)
    {
        return State<T>.getManhattanDistance(stateLocationMap, action.getFrom(), action.getTo());
    }
}

/**
 * Represents a search problem, solvable by an AI agent.
 */
class Problem<T>
{

    private readonly State<T> initialState;
    private readonly State<T> goalState;
    private readonly List<State<T>> states;
    private readonly List<Move<T>> moves;

    private readonly Dictionary<State<T>, List<Move<T>>> stateActionMap;

    //    private Map<State, Point> stateLocationMap;
    //
    //    public Map<State, Point> getStateLocationMap() {
    //        return stateLocationMap;
    //    }

    /**
     * @param states A list of all the possible states of the "world" represented by this problem.
     * @param initialState The initial state of the problem.
     * @param goalState The goal state of the problem, at which operating searches will terminate.
     * @param actions A list of all the possible transitions between states.
     */
    Problem(List<State<T>> states, State<T> initialState, State<T> goalState, List<Move<T>> moves)
    {
        this.initialState = initialState;
        this.states = states;
        this.goalState = goalState;
        this.moves = moves;


        //stateActionMap = states.stream().collect(Collectors.toMap(str=>str, str=>moves.stream().filter(n=>str.equals(n.getFrom())).collect(Collectors.toList())));

        foreach (State<T> s in states)
        {
            List<Move<T>> movesFromState = new List<Move<T>>();
            foreach (Move<T> m in moves)
            {
                if (m.getFrom().Equals(s))
                {
                    movesFromState.Add(m);
                }
            }
            stateActionMap.Add(s, movesFromState);
        }

        //        for (Map.Entry<State, List<Action>> entry : stateActionMap.entrySet()) {
        //            System.out.println(entry.getKey().contents + "   " + entry.getValue().size());
        //        }

    }

    public List<Move<T>> getMoves(State<T> state)
    {
        return stateActionMap[state];
    }

    public State<T> getInitialState()
    {
        return initialState;
    }

    public State<T> getGoalState()
    {
        return goalState;
    }

    public List<State<T>> getStates() { return states; }

    public List<Move<T>> getAllActions() { return moves; }
}

public struct Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}