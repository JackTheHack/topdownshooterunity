using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponState { Ready, StartShot, Shooting, Cooldown, Reloading }

public class Weapon : MonoBehaviour {

    ObjectPool projectilePool;

    public WeaponState state { get; private set; }
    public int reserveAmmoCount { get; private set; }
    public int magazineSize { get; private set; }
    public int bulletsInMagazine { get; private set; }
    public float WeaponProjectileSpeed { get; private set; }

    float reloadTime = 1.5f;

    float cooldownTime;
    float cooldownTimeRemaining;

    public Animator animController;

    private float ACTUAL_ANIMATION_TIME = 0.1f;
    private float shootTimeRemaining;

    public Sprite ProjectileSprite;
    public int TotalAmmoCount;
    public int MagazineSize;
    public float CoolDownTime;
    public float ProjectileSpeed;

    private float initialScale;

    private PlayerController parent;

    private string weaponLayer;

    public string[] weaponDamageLayers;
    LayerMask weaponDamageLayerMask;

    AudioSource audioSource;

    public Transform MuzzleLocation;
    private Vector3 OriginalMuzzleOffset;

    public bool IsOneHanded;

    public void Awake()
    {
        Init(TotalAmmoCount, MagazineSize, CoolDownTime, ProjectileSpeed, true);
    }

    public void Start()
    {

        OriginalMuzzleOffset = MuzzleLocation.position - transform.position;

        projectilePool = GameObject.Find("GlobalProjectilePool").GetComponent<ObjectPool>();

        
        //Debug.Log(1 / (cooldownTime / ACTUAL_ANIMATION_TIME));
        animController.SetFloat("speedMult", GetShootAnimSpeedMult());
        initialScale = transform.localScale.x;

        parent = GetComponentInParent<PlayerController>();
        weaponDamageLayerMask = LayerMask.GetMask(weaponDamageLayers);

        audioSource = GetComponent<AudioSource>();
    }

    public void Init(int totalAmmoCount, int magazineSize, float coolDownTime, float projectileSpeed, bool ownedByPlayer) {
        state = WeaponState.Ready;
        this.reserveAmmoCount = totalAmmoCount;
        this.magazineSize = magazineSize;
        bulletsInMagazine = totalAmmoCount >= magazineSize ? magazineSize : totalAmmoCount;

        this.cooldownTime = coolDownTime;
        cooldownTimeRemaining = 0;

        this.WeaponProjectileSpeed = projectileSpeed;

        weaponLayer = ownedByPlayer ? "PlayerBullet" : "EnemyBullet";
    }

    public float GetShootAnimSpeedMult()
    {
        return animController.speed / ACTUAL_ANIMATION_TIME;
    }

    public bool RequestShot() {
        // try to shoot the weapon.
        if (WeaponReady() && bulletsInMagazine > 0) {
            state = WeaponState.StartShot;
            
            return true;
        }

        return false;
    }

    public struct ReloadInfo
    {
        public int bulletsInMagazine;
        public int reserveAmmoCount;
        public float reloadTime;

        public ReloadInfo(int bul, int res, float reloadTime)
        {
            bulletsInMagazine = bul;
            reserveAmmoCount = res;
            this.reloadTime = reloadTime;
        }
    }

    public bool RequestReload() {
        if (bulletsInMagazine < magazineSize && reserveAmmoCount > 0)
        {
            state = WeaponState.Reloading;
            parent.uimanager.StartReloadBarCoroutine(new ReloadInfo(bulletsInMagazine, reserveAmmoCount, reloadTime));
            Invoke("Reload", reloadTime);
            return true;
        }

        return false;
    }

    private void Shoot() {
        bulletsInMagazine -= 1;
        //reserveAmmoCount -= 1;

        //GameObject bulletInstance = Instantiate(bulletPrefab, transform.position, Quaternion.Euler(0,0, Random.Range(0,360)));
        //Bullet bulletComponent = bulletInstance.GetComponent<Bullet>();
        //bulletComponent.SetVelocity(transform.right * WeaponProjectileSpeed);

        InstantiateBullet();

        //audioSource.pitch = 1 + Random.Range(-0.05f, 0.05f);
        
    }

    private void InstantiateBullet()
    {
        GameObject projectile;

        projectile = projectilePool.RequestObject<Bullet>();
        ProjectileController baseController = projectile.GetComponent<ProjectileController>();

        // Produces a spread angle biased towards zero degrees. 
        // Weapon spread forms a distribution around zero
        // rather than uniform innaccuracy.
        //float randomBiasedToZero = Random.Range(0, weapon.shootPattern.spreadAngle) - Random.Range(0, weapon.shootPattern.spreadAngle);
        //Quaternion spreadQuat = Quaternion.Euler(new Vector3(0, 0, randomBiasedToZero));


        baseController.Setup(this, MuzzleLocation.position, Quaternion.LookRotation(transform.forward, transform.up), ProjectileSprite, weaponDamageLayerMask);

        projectile.GetComponent<Bullet>().BeShot(baseController, transform.right * WeaponProjectileSpeed);

        projectile.layer = LayerMask.NameToLayer(weaponLayer);
    }

    private void Reload() {
        int amountToLoad = 0;
        if (reserveAmmoCount >= magazineSize - bulletsInMagazine)
        {
            amountToLoad = magazineSize - bulletsInMagazine;
        } else {
            amountToLoad = reserveAmmoCount; // all remaining ammo
        }
        bulletsInMagazine += amountToLoad;
        reserveAmmoCount -= amountToLoad;

        parent.uimanager.DoReloadUIUpdate(bulletsInMagazine, reserveAmmoCount);

        state = WeaponState.Ready;
    }

    public bool OnCooldown() {
        return state == WeaponState.Cooldown;
    }

    public bool WeaponReady() {
        return state == WeaponState.Ready;
    }

    private void RotateWeaponToMouse()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5f;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;

        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        if (angle > 80 || angle < -100)
        {
            Vector3 weaponScale = transform.localScale;
            weaponScale.y = initialScale * -1;
            weaponScale.x = initialScale * -1;
            transform.localScale = weaponScale;

            parent.SetFacingLeft();
        }
        else if (angle < 100 || angle > -80)
        {
            Vector3 weaponScale = transform.localScale;
            weaponScale.y = initialScale;
            weaponScale.x = initialScale;
            transform.localScale = weaponScale;

            parent.SetFacingRight();
        }
    }

    public void Update()
    {

        //RotateWeaponToMouse();

        switch (state)
        {
            case WeaponState.Reloading:
                animController.SetBool("IsShooting", false);
                break;
            case WeaponState.Ready:
                break;
            case WeaponState.StartShot:
                Shoot();
                parent.MagazineUIUpdate();
                animController.SetBool("IsShooting", true);
                state = WeaponState.Shooting;
                shootTimeRemaining = ACTUAL_ANIMATION_TIME;
                break;
            case WeaponState.Shooting:
                // to provide for the shot animation time.
                shootTimeRemaining -= Time.deltaTime;
                if (shootTimeRemaining <= 0)
                {
                    state = WeaponState.Cooldown;
                    cooldownTimeRemaining = cooldownTime;
                    animController.SetBool("IsShooting", false);
                }
                break;
            case WeaponState.Cooldown:
                cooldownTimeRemaining -= Time.deltaTime;
                if (cooldownTimeRemaining <= 0) 
                {
                    state = WeaponState.Ready;
                }
                break;
        }

        MuzzleLocation.position = transform.TransformPoint(OriginalMuzzleOffset);
        //MuzzleLocation.rotation = transform.rotation;


    }



}