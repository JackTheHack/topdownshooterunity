﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{

    //public Rigidbody2D parentRigidbody; // The rigidbody of the gun that this is to be fired from, for doing velocity inheritance.
    public Weapon parentWeapon;

    SpriteRenderer spriteRenderer;
    Color initialColour;

    private float lifeTime = 10f;
    protected float lifeElapsed = 0f;

    LayerMask damageLayerMask;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        
        initialColour = spriteRenderer.color;
    }

    // Use this for initialization
    void OnEnable()
    {

    }

    void Update()
    {
        FadeOut();
        if (gameObject.activeInHierarchy && lifeElapsed >= lifeTime)
        {
            Deactivate();
        }
        else
        {
            lifeElapsed += Time.deltaTime;
        }
    }

    public void Setup(Weapon parent, Vector3 gunPosition, Quaternion playerRotation, Sprite sprite, LayerMask newDamageLayerMask)
    {
        parentWeapon = parent;
        transform.position = gunPosition;
        transform.rotation = playerRotation;
        spriteRenderer.sprite = sprite;
        Vector3 pos = transform.position;
        pos.z = -5;
        transform.position = pos;
        damageLayerMask = newDamageLayerMask;
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
        lifeElapsed = 0;
    }

    private void FadeOut()
    {
        spriteRenderer.color = Color.Lerp(initialColour, Color.clear, (lifeElapsed*lifeElapsed*lifeElapsed*lifeElapsed) / lifeTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((damageLayerMask.value & 1 << collision.gameObject.layer) != 0)
        {
            Deactivate();
        }
    }

}
