﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : MonoBehaviour
{

    public CreateSearchNodesFromTilemap searchNodeMaker;

    void Awake()
    {

        searchNodeMaker.GenerateNodes();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
