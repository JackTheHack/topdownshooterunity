﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyController : PhysicsObject
{

    public Transform followTarget;

    DamageController damageController;

    [SerializeField]
    private float MaxMovementSpeed = 3;

    Vector3[] path;
    int targetIndex;

    LayerMask playerBulletLayer;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        playerBulletLayer = LayerMask.NameToLayer("PlayerBullet");
        damageController = GetComponent<DamageController>();

        InvokeRepeating("CallRequestPath", 0f, 0.5f);
    }

    private void CallRequestPath()
    {

        if (isActiveAndEnabled)
        {
            PathRequestManager.RequestPath(transform.position,
                               followTarget.position,
                               OnPathFound);
        }
    }

    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        if (pathSuccessful) 
        {
            path = newPath;
            targetIndex = 0;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    IEnumerator FollowPath() 
    {
        Vector3 currentWayPoint = path[0];

        while (true) 
        {
            Vector2 pos2D;
            pos2D.x = transform.position.x;
            pos2D.y = transform.position.y;

            Vector2 waypoint2D;
            waypoint2D.x = currentWayPoint.x;
            waypoint2D.y = currentWayPoint.y;

            //Debug.Log(pos2D + "  " + waypoint2D + "  " + Vector2.Distance(pos2D, waypoint2D));

            if (Vector2.Distance(pos2D, waypoint2D) <= 0.1f)
            {
                
                targetIndex++;
                if (targetIndex >= path.Length)
                {
                    yield break;
                }
                currentWayPoint = path[targetIndex];
            }
            //transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, MaxMovementSpeed * Time.deltaTime);
            targetVelocity = (waypoint2D - pos2D).normalized * MaxMovementSpeed;
            //Debug.Log(((currentWayPoint - transform.position).normalized) + "  " + currentWayPoint + "  " + transform.position);
            yield return null;
        }
    }

    protected override void ComputeVelocity()
    {
        //if (targetIndex < path.Length)
        //{
            // The normalise operation in here has to operate on 2D vectors because if its on 
            // 3D vectors the Z value can be 1, which results in a vector with a magnitude of 1
            // making it seem like the magnitude is decreasing with distance since we're only looking
            // at X and Y.
            //Vector2 enemyPos2D = (Vector2) transform.position;
            //targetVelocity = ((Vector2) path[targetIndex] - enemyPos2D).normalized * MaxMovementSpeed;
            //Debug.Log(((path[targetIndex] - transform.position).normalized));
        //}

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision + "  " + Time.frameCount);
        if (collision.gameObject.layer == playerBulletLayer)
        {
            damageController.TakeDamage(new DamageController.DamageInfo(1, "Physical"));
        }
    }

    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);
                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);   
                }
                else 
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }
}
