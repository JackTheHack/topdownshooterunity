﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class WeaponData
{
    public WeaponState state { get; private set; }
    public int reserveAmmoCount { get; private set; }
    public int magazineSize { get; private set; }
    public int bulletsInMagazine { get; private set; }
    public float WeaponProjectileSpeed { get; private set; }

    float reloadTime = 1.5f;

    public Animator animController;

    public Transform MuzzleLocation;

    public bool IsOneHanded;
}

public class WeaponScriptable : ScriptableObject
{
    public List<WeaponAsset> WeaponItems;
}

public class WeaponAsset
{
    [MenuItem("Assets/Create/Weapon")]
    public static void CreateAsset ()
    {
        //U.CreateAsset<WeaponScriptable>();
    }
}
