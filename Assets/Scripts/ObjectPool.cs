﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{

    public int initialSize;
    public GameObject objectPrefab;

    private Dictionary<Type, GameObject[]> pools;


    private void Awake()
    {
        pools = new Dictionary<Type, GameObject[]>();
    }

    void InitialisePoolObjects<T>(GameObject[] pool, GameObject basePrefab) where T : Component
    {
        for (int i = 0; i < pool.Length; i++)
        {
            pool[i] = Instantiate(basePrefab);
            pool[i].AddComponent<T>();
            pool[i].SetActive(false);
        }
    }

    public void AddNewPoolType<T>() where T : Component
    {
        GameObject[] pool = new GameObject[initialSize];
        pools.Add(typeof(T), pool);
        InitialisePoolObjects<T>(pool, objectPrefab);
    }

    public GameObject[] GetPoolOfType<T>()
    {
        GameObject[] pool;
        bool result = pools.TryGetValue(typeof(T), out pool);
        if (result)
        {
            return pool;
        }
        else
        {
            throw new KeyNotFoundException();
        }
    }

    public GameObject RequestObject<T>() where T : Component
    {
        GameObject[] chosenPool = GetPoolOfType<T>();
        for (int i = 0; i < chosenPool.Length; i++)
        {
            if (!chosenPool[i].activeInHierarchy)
            {
                
                chosenPool[i].SetActive(true);
                return chosenPool[i];
            }
        }

        return GrowPool<T>();
    }

    GameObject GrowPool<T>() where T : Component
    {
        GameObject[] pool = GetPoolOfType<T>();
        int oldLength = pool.Length;

        GameObject[] newPool = new GameObject[oldLength * 2];
        pool.CopyTo(newPool, 0);

        for (int i = oldLength; i < newPool.Length; i++)
        {
            newPool[i] = Instantiate(objectPrefab);
            newPool[i].AddComponent<T>();
            newPool[i].SetActive(false);
        }

        pools[typeof(T)] = newPool;

        newPool[oldLength].SetActive(true);
        return newPool[oldLength];
    }
}