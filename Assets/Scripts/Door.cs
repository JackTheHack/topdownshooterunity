﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    public bool isOpen { get; private set; }

    Animator animator;
    Collider2D coll;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool RequestOpen()
    {
        if (!isOpen)
        {
            isOpen = true;
            animator.SetBool("isOpen", true);
            coll.enabled = false;
            return true;
        }
        return false;
    }

    public bool RequestClose()
    {
        if (isOpen)
        {
            isOpen = false;
            animator.SetBool("isOpen", false);
            coll.enabled = true;
            return true;
        }
        return false;
    }

    public void NextState()
    {
        if (isOpen)
        {
            RequestClose();
        } else
        {
            RequestOpen();
        }
    }
}
