﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class CreateSearchNodesFromTilemap : MonoBehaviour
{

    public bool displayGridGizmos;
    public Grid gridBase;
    public Tilemap floor;
    public List<Tilemap> obstacleLayers;
    public SearchNode nodePrefab;

    public int scanStartX = -250, scanStartY = -250, scanFinishX = 250, scanFinishY = 250;

    public List<SearchNode> unsortedNodes = new List<SearchNode>();
    public Dictionary<Vector2Int, SearchNode> nodes;

    public int gridBoundX = 0, gridBoundY = 0;

    public int unwalkableNodeBorder;

    private void Awake()
    {
        //unsortedNodes = new List<SearchNode>();
    }

    public void GenerateNodes()
    {
        CreateNodes();
        
    }

    public int MaxSize {
        get {
            return gridBoundX * gridBoundY;
        }
    }

    void CreateNodes()
    {
        int gridX = 0;
        int gridY = 0;

        bool foundTileOnLastPass = false;

        for (int x = scanStartX; x < scanFinishX; x++)
        {
            for (int y = scanStartY; y < scanFinishY; y++)
            {
                TileBase tb = floor.GetTile(new Vector3Int(x, y, 0));
                if (tb == null)
                {
                } else
                {
                    bool foundObstacle = false;
                    foreach (Tilemap t in obstacleLayers)
                    {
                        TileBase tb2 = t.GetTile(new Vector3Int(x, y, 0));

                        if (tb2 != null)
                        {
                            foundObstacle = true;
                        }

                        if (unwalkableNodeBorder > 0)
                        {
                            List<TileBase> neighbours = GetNeighbouringTiles(x, y, t);
                            foreach (TileBase tl in neighbours)
                            {
                                if (tl != null)
                                {
                                    foundObstacle = true;
                                }
                            }
                        }
                    }

                    float halfCellSize = gridBase.cellSize.x * 0.5f;
                    Vector3 worldPos = new Vector3((x*gridBase.cellSize.x) + halfCellSize + gridBase.transform.position.x, (y*gridBase.cellSize.x) + halfCellSize + gridBase.transform.position.y, 0);
                    SearchNode node = Instantiate(nodePrefab, worldPos, Quaternion.identity);
                    //SearchNode wt = node.GetComponent<SearchNode>();
                    node.nodeArrayX = gridX;
                    node.nodeArrayY = gridY;

                    node.walkable = !foundObstacle;

                    //Vector2Int cellPos = (Vector2Int) gridBase.WorldToCell(worldPos);
                    node.tilemapPos = new Vector2Int(x, y);
                    foundTileOnLastPass = true;
                    node.worldPosition = worldPos;
                    node.name = "NODE " + gridX.ToString() + " : " + gridY.ToString();
                    

                    unsortedNodes.Add(node);

                    gridY++;

                    if (gridX > gridBoundX)
                    {
                        gridBoundX = gridX;
                    }
                    if (gridY > gridBoundY)
                    {
                        gridBoundY = gridY;
                    }
                }
            }
            if (foundTileOnLastPass)
            {
                gridX++;
                gridY = 0;
                foundTileOnLastPass = false;
            }
        }

        //nodes = new SearchNode[gridBoundX + 1, gridBoundY + 1];
        nodes = new Dictionary<Vector2Int, SearchNode>();
        foreach (SearchNode n in unsortedNodes)
        {
            //Debug.Log("Adding node " + n);
            //nodes[g.nodeArrayX, g.nodeArrayY] = g;
            nodes.Add(n.tilemapPos, n);
   
        }

        foreach (SearchNode n in unsortedNodes)
        {
            n.myNeighbours = GetNeighbours(n);
        }
        

        //for (int x = 0; x < gridBoundX; x++)
        //{
        //    for (int y = 0; y < gridBoundY; y++)
        //    {
        //        if (nodes[x, y] != null)
        //        {
        //            SearchNode wt = nodes[x, y];
        //            wt.myNeighbours = GetNeighbours(wt);
        //        }
        //    }
        //}
    }

    public List<TileBase> GetNeighbouringTiles(int x, int y, Tilemap t)
    {
        List<TileBase> retVal = new List<TileBase>();

        for (int i = x - unwalkableNodeBorder; i <= x + unwalkableNodeBorder; i++)
        {
            for (int j = y - unwalkableNodeBorder; j <= y + unwalkableNodeBorder; j++)
            {
                TileBase tile = t.GetTile(new Vector3Int(i, j, 0));
                if (tile != null)
                {
                    retVal.Add(tile);
                }
            }
        }
        return retVal;
    }

    public Vector2Int[] directions = new Vector2Int[] { new Vector2Int(-1,0), new Vector2Int(1,0), new Vector2Int(0,-1), new Vector2Int(0,1) };

    public List<SearchNode> GetNeighbours(SearchNode node)
    {
        List<SearchNode> myNeighbours = new List<SearchNode>();


        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                //int checkX = node.tilemapPos.x + x;
                //int checkY = node.tilemapPos.y + y;

                Vector2Int checkPos = new Vector2Int(node.tilemapPos.x + x, node.tilemapPos.y + y);

                SearchNode maybeNeighbour;
                bool foundNeighbour = nodes.TryGetValue(checkPos, out maybeNeighbour);

                if (foundNeighbour)
                {
                    myNeighbours.Add(maybeNeighbour);
                }

                //if (checkPos.x >= 0 && checkPos.x < gridBoundX && checkPos.y >= 0 && checkPos.y < gridBoundY)
                //{
                //    myNeighbours.Add(nodes[checkPos]);
                //}
            }
        }

        //if (x > 0 && x < width - 1)
        //{
        //    if (y > 0 && y < height - 1)
        //    {

        //        for (int i = 0; i < directions.Length; i++)
        //        {
        //            Vector2Int dir = directions[i];

        //            if (x == 0 && dir.x == -1 || 
        //                x == width - 1 && dir.x == 1 ||
        //                y == 0 && dir.y == -1 ||
        //                y == height - 1 && dir.y == 1)
        //            {
        //                continue;                       
        //            }

        //            SearchNode wt = nodes[dir.x, dir.y].GetComponent<SearchNode>();
        //            if (nodes[dir.x, dir.y] != null)
        //            {
        //                myNeighbours.Add(wt);
        //            }
        //        }
        //    }
        //}

        return myNeighbours;
    }

    //public List<SearchNode> path;

    void OnDrawGizmos()
    {

        Gizmos.DrawWireCube(gridBase.transform.position, new Vector3(gridBoundX, 1, gridBoundY));

        if (gridBase != null && displayGridGizmos)
        {
            if (nodes != null)
            {
                foreach (KeyValuePair<Vector2Int, SearchNode> pair in nodes)
                {
                    SearchNode node = pair.Value;
                    if (node != null)
                    {
                        Gizmos.color = (node.walkable) ? Color.white : Color.red;
                        Gizmos.DrawCube(node.worldPosition, Vector3.one * (gridBase.cellSize.x - .3f));
                    }
                }
            }
        }
    }

}
