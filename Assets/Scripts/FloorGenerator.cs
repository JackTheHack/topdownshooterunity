﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;
using csDelaunay;

public class FloorGenerator : MonoBehaviour
{

    Grid gridmap;
    public Tilemap noCollTilemap;
    public Tilemap collisionTilemap;

    int lowerX = -50;
    int lowerY = -50;
    int higherX = 50;
    int higherY = 50;

    public TileBase[] noCollTiles;
    public TileBase[] collTiles;

    int minRoomWidth = 7;
    int maxRoomWidth = 20;
    int minRoomHeight = 7;
    int maxRoomHeight = 20;

    List<Room> rooms = new List<Room>();
    List<GameObject> physicsRooms = new List<GameObject>();

    private List<Room> mainRooms = new List<Room>();
    private List<Room> roomsToCreate = new List<Room>();

    private Dictionary<Vector2f, Site> graphSites;
    private List<Edge> graphEdges;

    private List<JacksEdge> minSpanResult = new List<JacksEdge>();

    private bool drawMainRooms = false;
    private bool drawVoronoi = false;
    private bool drawMinSpan = false;
    private bool drawHallways = false;

    Dictionary<int, Room> roomDict = new Dictionary<int, Room>();
    Dictionary<Vector2f, int> positionToRoomId;

    List<Hallway> hallways;

    public bool InValidGenerationState { get; private set; }

    class Room
    {
        private static int maxID;

        public int id;
        //public Vector2Int position;
        public BoundsInt boundingBox;
        public int width;
        public int height;

        public bool isMainRoom;

        public Room(Vector2Int bottomLeftPos, int width, int height)
        {
            this.id = maxID;
            maxID++;

            //this.position = position;
            this.width = width;
            this.height = height;

            // BoundsInt always has to have >0 size in the Z axis otherwise BoundsInt.Contains() will always fail.
            this.boundingBox = new BoundsInt(new Vector3Int(bottomLeftPos.x, bottomLeftPos.y, 0), new Vector3Int(width, height, 2));
        }

        public bool IntersectsHallway(Hallway hall)
        {
            foreach (var C in boundingBox.allPositionsWithin)
            {
                Vector3 A = new Vector3(hall.line.startPoint.x, hall.line.startPoint.y, 0);
                Vector3 B = new Vector3(hall.line.endPoint.x, hall.line.endPoint.y, 0);

                Vector3 AB = B - A;
                Vector3 AC = C - A;

                //Debug.Log(Vector3.Cross(AB, AC));
                if (Vector3.Cross(AB, AC).magnitude == 0)
                {
                    float ABdotAC = Vector3.Dot(AB, AC);
                    float ABdotAB = Vector3.Dot(AB, AB);

                    if (ABdotAC == 0 || ABdotAC == ABdotAB || (ABdotAC > 0 && ABdotAC < ABdotAB))
                    {
                        // C is on line segment AB
                        return true;
                    }
                }
            }

            return false;
        }
    }

    class HallwayLine
    {
        public Vector2Int startPoint;
        public Vector2Int endPoint;

        public HallwayLine(Vector2Int startPoint, Vector2Int endPoint)
        {
            this.startPoint = startPoint;
            this.endPoint = endPoint;
        }
    }

    class Hallway
    {
        public HallwayLine line;
        public HallwayLine secondLine;

        public bool UsingSecondLine
        {
            get { return secondLine != null; }
        }

        public Hallway(HallwayLine line, int srcRoomId, int destRoomId)
        {
            this.line = line;
        }

        public Hallway(HallwayLine horizontalLine, HallwayLine verticalLine, int srcRoomId, int destRoomId)
        {
            this.line = horizontalLine;
            this.secondLine = verticalLine;
        }
    }

    struct CornerPair
    {
        public bool usingTopLeft;

        // First and Second either refer to the top left and bottom right
        // or the bottom left and top right coordinates of a bounding box.
        public Vector2Int first;
        public Vector2Int second;

        public CornerPair(Vector2Int first, Vector2Int second)
        {
            if (first.x <= second.x && first.y >= second.y)
            {
                this.first = first;
                this.second = second;
                usingTopLeft = true;
            }
            else if (second.x <= first.x && second.y >= first.y)
            {
                this.first = second;
                this.second = first;
                usingTopLeft = true;
            }
            else if (first.x <= second.x && first.y <= second.y)
            {
                this.first = first;
                this.second = second;
                usingTopLeft = false;
            }
            else if (second.x <= first.x && second.y <= first.y)
            {
                this.first = second;
                this.second = first;
                usingTopLeft = false;
            } else
            {
                this.first = Vector2Int.zero;
                this.second = Vector2Int.zero;
                usingTopLeft = false;
            }
        }
    }

    void Awake()
    {

        gridmap = GetComponent<Grid>();
        //GenerateRandomFloor(gridmap);
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    GenerateFloor();
        //}

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            drawMainRooms = !drawMainRooms;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            drawVoronoi = !drawVoronoi;
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            drawMinSpan = !drawMinSpan;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            drawHallways = !drawHallways;
        }
    }

    public Vector2 GetSpawnPoint()
    {
        return mainRooms[Random.Range(0, mainRooms.Count)].boundingBox.center;
    }

    public void GenerateFloor()
    {
        InValidGenerationState = false;

        foreach (GameObject g in physicsRooms)
        {
            Destroy(g);
        }
        noCollTilemap.ClearAllTiles();
        collisionTilemap.ClearAllTiles();
        physicsRooms.Clear();
        rooms.Clear();
        mainRooms.Clear();
        minSpanResult.Clear();

        rooms = GetRoomsInCircle(10, (int)gridmap.cellSize.x, 60);
        foreach (Room room in rooms)
        {
            roomDict.Add(room.id, room);
        }
        physicsRooms = SetupPhysicsRooms(rooms);

        InvokeRepeating("CheckAllPhysicsRoomsSettled", 0, 1);
    }

    private void CheckAllPhysicsRoomsSettled()
    {
        foreach (GameObject physicsRoom in physicsRooms)
        {
            if (!physicsRoom.GetComponent<Rigidbody2D>().IsSleeping())
            {
                return;
            }
        }
        CancelInvoke("CheckAllPhysicsRoomsSettled");
        SetupMainRooms();

        SetFloorTiles(roomsToCreate, hallways);

        InValidGenerationState = true;
    }

    private void SetupMainRooms()
    {

        // Update room positions now that they've spread out with the physics engine.

        // Cell size has to be an int cus its going to get rounded here.
        for (int i = 0; i < physicsRooms.Count; i++)
        {
            Vector3Int roundedPos = new Vector3Int(RoundNtoM(physicsRooms[i].transform.position.x, (int)gridmap.cellSize.x), RoundNtoM(physicsRooms[i].transform.position.y, (int)gridmap.cellSize.y), 0);

            // BoundsInt always has to have >0 size in the Z axis otherwise BoundsInt.Contains() will always fail.
            rooms[i].boundingBox = new BoundsInt(roundedPos, new Vector3Int(rooms[i].width, rooms[i].height, 2));
        }

        double widthMean = rooms.Select(n => n.width).Average();
        double heightMean = rooms.Select(n => n.height).Average();

        double meanRatio = 1f;

        mainRooms = rooms.FindAll(n => n.width > meanRatio * widthMean && n.height > meanRatio * heightMean);
        mainRooms.ForEach(n => n.isMainRoom = true);

        Delaunay(mainRooms.Select(n => n.boundingBox.center).ToList());

        positionToRoomId = new Dictionary<Vector2f, int>();
        foreach (var entry in graphSites)
        {
            foreach (Room room in mainRooms)
            {
                // These need to be centre positions to match the edges produced by delauanay.
                // Centre positions have to be rounded from BoundsInt();
                //Vector2Int roundedRoomCentre = RoundVecToM(room.boundingBox.center, (int)gridmap.cellSize.x);
                Vector2f roomPosVector2f = new Vector2f(room.boundingBox.center.x, room.boundingBox.center.y);
                if (roomPosVector2f == entry.Key)
                {
                    positionToRoomId.Add(entry.Key, room.id);
                    break;
                }
            }
        }

        //foreach (var entry in graphSites)
        //{
        //    Debug.Log(entry.Key + "   " + positionToRoomId[entry.Key]);
        //}

        MinimumSpanningTree minSpanTree = new MinimumSpanningTree(graphEdges, graphSites.Count);
        minSpanResult = minSpanTree.KruskalsMinSpanTree().ToList<JacksEdge>();

        int tenPercentOfEdges = (int) (graphEdges.Count * 0.1f);
        for (int i = 0; i < tenPercentOfEdges; i++)
        {
            Edge randomEdge = graphEdges[Random.Range(0, graphEdges.Count)];
            minSpanResult.Add(new JacksEdge(randomEdge));
        }

        //foreach (JacksEdge e in minSpanResult)
        //{
        //    Debug.Log(e.src +  "  " + e.dest);
        //}

        hallways = CreateHallways(mainRooms, minSpanResult);

        roomsToCreate = new List<Room>();
        roomsToCreate.AddRange(mainRooms);
        foreach (var room in rooms)
        {
            if (!room.isMainRoom)
            {
                foreach (var hall in hallways)
                {
                    if (room.IntersectsHallway(hall))
                    {
                        roomsToCreate.Add(room);
                    }
                }
            }
        }
        
    }

    private void SetFloorTiles(List<Room> rooms, List<Hallway> hallways)
    {
        foreach (var room in rooms)
        {

                //for (int x = room.position.x; x < room.position.x + room.width; x++)
                //{
                //    for (int y = room.position.y; y < room.position.y + room.height; y++)
                //    {
                //        noCollTilemap.SetTile(new Vector3Int(x, y, 0), noCollTiles[0]);
                //    }
                //}
                //Vector2 topLeft = new Vector2(room.position.x - (room.width * 0.5f), room.position.y + (room.height * 0.5f));
                //Vector2 bottomRight = new Vector2(room.position.x + (room.width * 0.5f), room.position.y - (room.height * 0.5f));

                //SimpleSetTilesBlock(noCollTilemap, 
                //    RoundVecToM(topLeft, (int)gridmap.cellSize.x),
                //    RoundVecToM(bottomRight, (int)gridmap.cellSize.x),
                //    noCollTiles[0],
                //    new Vector2Int(0,0),
                //    noCollTilemap);

            SimpleSetTilesBlock(noCollTilemap, room.boundingBox, noCollTiles[0]);
                
            // Offset not using
            //RoundVecToM(new Vector2(room.width + (gridmap.cellSize.x * -0.5f), room.height + (gridmap.cellSize.y * -0.5f)) * -0.5f, (int)gridmap.cellSize.x
        }


        foreach (var hall in hallways)
        {
            //SimpleSetTilesBlock(noCollTilemap, hall.line.startPoint, hall.line.endPoint, noCollTiles[0]);
            //if (hall.UsingSecondLine)
            //{
            //    SimpleSetTilesBlock(noCollTilemap, hall.secondLine.startPoint, hall.secondLine.endPoint, noCollTiles[0]);
            //}
            SetHallwayTiles(hall);
        }

        hallways.ForEach(n => SetHallwayTiles(n));

        foreach (var room in rooms)
        {
            SetRoomWalls(room);
        }

        hallways.ForEach(n => SetHallwayWalls(n));

        // Place all the rooms floors, then their walls.
        // Then place hallway floors, eliminating room walls where they are equal to create doorways.
        // Then place hallway walls.



        foreach (var physicsRoom in physicsRooms)
        {
            Destroy(physicsRoom);
        }
    }

    private void SimpleSetTilesBlock(Tilemap tilemap, Vector2Int start, Vector2Int end, TileBase tile)
    {
        SimpleSetTilesBlock(tilemap, start, end, tile, new Vector2Int(0, 0), null);
    }

    private void SimpleSetTilesBlock(Tilemap tilemap, Vector2Int start, Vector2Int end, TileBase tile, Vector2Int offset, Tilemap skipIfAlreadyInOtherTilemap)
    {
        //bool usingTopLeft = true;

        //Vector2Int topLeft = Vector2Int.zero;
        //Vector2Int bottomRight = Vector2Int.zero;

        //Vector2Int bottomLeft = Vector2Int.zero;
        //Vector2Int topRight = Vector2Int.zero;

        CornerPair pair = new CornerPair(start, end);
        
        if (pair.usingTopLeft)
        {
            for (int x = pair.first.x; x <= pair.second.x; x++)
            {
                for (int y = pair.first.y; y >= pair.second.y; y--)
                {
                    Vector3Int offsetPos = new Vector3Int(x + offset.x, y + offset.y, 0);
                    if (skipIfAlreadyInOtherTilemap == null || (skipIfAlreadyInOtherTilemap != null && !skipIfAlreadyInOtherTilemap.HasTile(offsetPos)))
                    {
                        tilemap.SetTile(offsetPos, tile);
                    }
                }
            }
        } else
        {
            for (int x = pair.first.x; x <= pair.second.x; x++)
            {
                for (int y = pair.first.y; y <= pair.second.y; y++)
                {
                    Vector3Int offsetPos = new Vector3Int(x + offset.x, y + offset.y, 0);
                    if (skipIfAlreadyInOtherTilemap == null || (skipIfAlreadyInOtherTilemap != null && !skipIfAlreadyInOtherTilemap.HasTile(offsetPos)))
                    {
                        tilemap.SetTile(offsetPos, tile);
                    }
                }
            }
        }
        
    }

    private void SimpleSetTilesBlock(Tilemap tilemap, BoundsInt boundingBox, TileBase tile)
    {
        SimpleSetTilesBlock(tilemap, boundingBox, tile, null);
    }

    private void SimpleSetTilesBlock(Tilemap tilemap, BoundsInt boundingBox, TileBase tile, Tilemap skipIfAlreadyInOtherTilemap)
    {

        for (int x = boundingBox.min.x; x <= boundingBox.max.x; x++)
        {
            for (int y = boundingBox.min.y; y <= boundingBox.max.y; y++)
            {
                Vector3Int offsetPos = new Vector3Int(x, y, 0);
                if (skipIfAlreadyInOtherTilemap == null || (skipIfAlreadyInOtherTilemap != null && !skipIfAlreadyInOtherTilemap.HasTile(offsetPos)))
                {
                    tilemap.SetTile(offsetPos, tile);
                }
            }
        }
    }



    void SetRoomWalls(Room room)
    {

        for (int x = room.boundingBox.xMin - 1; x <= room.boundingBox.xMax + 1; x++)
        {
            if (x == room.boundingBox.xMin - 1 || x == room.boundingBox.xMax + 1)
            {
                for (int y = room.boundingBox.yMin - 1; y <= room.boundingBox.yMax + 1; y++)
                {
                    Vector3Int pos = new Vector3Int(x, y, 0);
                    if (!noCollTilemap.HasTile(pos))
                    {
                        collisionTilemap.SetTile(pos, collTiles[0]);
                    }
                }
            } else
            {
                Vector3Int topPos = new Vector3Int(x, room.boundingBox.yMax + 1, 0);
                if (!noCollTilemap.HasTile(topPos))
                {
                    collisionTilemap.SetTile(topPos, collTiles[0]);
                }

                Vector3Int bottomPos = new Vector3Int(x, room.boundingBox.yMin - 1, 0);
                if (!noCollTilemap.HasTile(bottomPos))
                {
                    collisionTilemap.SetTile(bottomPos, collTiles[0]);
                }
            }
        }

        //Vector2 first = new Vector2(room.position.x - (room.width * 0.5f), room.position.y + (room.height * 0.5f));
        //Vector2 second = new Vector2(room.position.x + (room.width * 0.5f), room.position.y - (room.height * 0.5f));

        //Vector2Int firstRounded = RoundVecToM(first, (int)gridmap.cellSize.x);
        //Vector2Int secondRounded = RoundVecToM(second, (int)gridmap.cellSize.x);

        //CornerPair pair = new CornerPair(firstRounded, secondRounded);

    }

    void SetHallwayTiles(Hallway hall)
    {

        SimpleSetTilesBlock(noCollTilemap, hall.line.startPoint, hall.line.endPoint, noCollTiles[0]);
        if (hall.UsingSecondLine)
        {
            SimpleSetTilesBlock(noCollTilemap, hall.secondLine.startPoint, hall.secondLine.endPoint, noCollTiles[0]);
        }

    }

    void SetHallwayWalls(Hallway hall)
    {
        // Create hallway walls.
        // Bear in mind that second line is always the vertical if it is in use.
        if (hall.UsingSecondLine)
        {
            // First line is always the horizontal.
            SetHorizontalHallwayWalls(hall.line.startPoint, hall.line.endPoint, 1);
            // Second line is horizontal.
            SetVerticalHallwayWalls(hall.secondLine.startPoint, hall.secondLine.endPoint, 1);

        }
        else
        {
            if (hall.line.startPoint.x == hall.line.endPoint.x)
            {
                // Vertical line
                SetVerticalHallwayWalls(hall.line.startPoint, hall.line.endPoint, 1);
            }
            else
            {
                // Horizontal line
                SetHorizontalHallwayWalls(hall.line.startPoint, hall.line.endPoint, 1);
            }
        }
    }

    private void SetHorizontalHallwayWalls(Vector2Int startPoint, Vector2Int endPoint, int hallwayThickness)
    {

        RemoveTileFromAIfPresentOnB(new Vector3Int(startPoint.x - 1, startPoint.y - 1, 0), noCollTilemap, collisionTilemap);
        RemoveTileFromAIfPresentOnB(new Vector3Int(startPoint.x + 1, startPoint.y + 1, 0), noCollTilemap, collisionTilemap);
        RemoveTileFromAIfPresentOnB(new Vector3Int(endPoint.x - 1, endPoint.y - 1, 0), noCollTilemap, collisionTilemap);
        RemoveTileFromAIfPresentOnB(new Vector3Int(endPoint.x + 1, endPoint.y + 1, 0), noCollTilemap, collisionTilemap);

        Vector2Int upHallStartPoint = new Vector2Int(startPoint.x, startPoint.y + hallwayThickness);
        Vector2Int upHallEndPoint = new Vector2Int(endPoint.x, endPoint.y + hallwayThickness);
        SimpleSetTilesBlock(collisionTilemap, upHallStartPoint, upHallEndPoint, collTiles[0], new Vector2Int(0, 0), noCollTilemap);

        Vector2Int downHallStartPoint = new Vector2Int(startPoint.x, startPoint.y - hallwayThickness);
        Vector2Int downHallEndPoint = new Vector2Int(endPoint.x, endPoint.y - hallwayThickness);
        SimpleSetTilesBlock(collisionTilemap, downHallStartPoint, downHallEndPoint, collTiles[0], new Vector2Int(0, 0), noCollTilemap);
    }

    private void RemoveTileFromAIfPresentOnB(Vector3Int position, Tilemap mapToCheck, Tilemap mapToRemoveFrom)
    {
        if (mapToCheck.HasTile(position))
        {
            mapToRemoveFrom.SetTile(position, null);
        }
    }

    private void SetVerticalHallwayWalls(Vector2Int startPoint, Vector2Int endPoint, int width)
    {
        RemoveTileFromAIfPresentOnB(new Vector3Int(startPoint.x, startPoint.y - 1, 0), noCollTilemap, collisionTilemap);
        RemoveTileFromAIfPresentOnB(new Vector3Int(startPoint.x, startPoint.y + 1, 0), noCollTilemap, collisionTilemap);
        RemoveTileFromAIfPresentOnB(new Vector3Int(endPoint.x, endPoint.y - 1, 0), noCollTilemap, collisionTilemap);
        RemoveTileFromAIfPresentOnB(new Vector3Int(endPoint.x, endPoint.y + 1, 0), noCollTilemap, collisionTilemap);

        Vector2Int leftHallStartPoint = new Vector2Int(startPoint.x - width, startPoint.y);
        Vector2Int leftHallEndPoint = new Vector2Int(endPoint.x - width, endPoint.y);
        SimpleSetTilesBlock(collisionTilemap, leftHallStartPoint, leftHallEndPoint, collTiles[0], new Vector2Int(0, 0), noCollTilemap);

        Vector2Int rightHallStartPoint = new Vector2Int(startPoint.x + width, startPoint.y);
        Vector2Int rightHallEndPoint = new Vector2Int(endPoint.x + width, endPoint.y);
        SimpleSetTilesBlock(collisionTilemap, rightHallStartPoint, rightHallEndPoint, collTiles[0], new Vector2Int(0, 0), noCollTilemap);
    }

    private void Delaunay(List<Vector3> midpoints)
    {

        List<Vector2f> points = new List<Vector2f>();
        points.AddRange(midpoints.Select(vec => Vector3ToVector2f(vec)));

        Rectf bounds = new Rectf(-300, -300, 300, 300);

        Voronoi voronoi = new Voronoi(points, bounds, 0);

        graphSites = voronoi.SitesIndexedByLocation;
        graphEdges = voronoi.Edges;
    }

    List<Vector2Int> midPointsRounded = new List<Vector2Int>();

    private List<Hallway> CreateHallways(List<Room> rooms, List<JacksEdge> edges)
    {

        List<Hallway> hallways = new List<Hallway>();

        foreach (var edge in edges)
        {
            Vector2 midpoint = new Vector2((edge.src.x + edge.dest.x) / 2, (edge.src.y + edge.dest.y) / 2);
            Vector2Int midpointRounded = RoundVecToM(midpoint, (int) gridmap.cellSize.x);
            
            int srcRoomId = positionToRoomId[edge.src];
            int destRoomId = positionToRoomId[edge.dest];

            Room srcRoom = roomDict[srcRoomId];
            Room destRoom = roomDict[destRoomId];

            // Checking if the midpoint between two rooms centres can trace a straight line between both rooms in either axis.
            // If we can't then we need to connect them with two lines in an L shape.
            if (srcRoom.boundingBox.Contains(new Vector3Int(RoundNtoM(edge.src.x, (int) gridmap.cellSize.x), midpointRounded.y, 0)) && 
                destRoom.boundingBox.Contains(new Vector3Int(RoundNtoM(edge.dest.x, (int) gridmap.cellSize.x), midpointRounded.y, 0)))
            {
                // Make horizontal line from
                Vector2Int startPoint = Vector2Int.zero;
                Vector2Int endPoint = Vector2Int.zero;

                if (srcRoom.boundingBox.center.x <= destRoom.boundingBox.center.x)
                {
                    // Srcroom left of destroom
                    startPoint = new Vector2Int(srcRoom.boundingBox.max.x, midpointRounded.y);
                    endPoint = new Vector2Int(destRoom.boundingBox.min.x, midpointRounded.y);
                } else
                {
                    // Srcroom right of destroom
                    startPoint = new Vector2Int(srcRoom.boundingBox.min.x, midpointRounded.y);
                    endPoint = new Vector2Int(destRoom.boundingBox.max.x, midpointRounded.y);
                }

                hallways.Add(new Hallway(new HallwayLine(startPoint, endPoint), srcRoomId, destRoomId));

            } else if (srcRoom.boundingBox.Contains(new Vector3Int(midpointRounded.x, RoundNtoM(edge.src.y, (int) gridmap.cellSize.x), 0)) && 
                destRoom.boundingBox.Contains(new Vector3Int(midpointRounded.x, RoundNtoM(edge.dest.y, (int) gridmap.cellSize.x), 0)))
            {
                // Make vertical line from
                Vector2Int startPoint = Vector2Int.zero;
                Vector2Int endPoint = Vector2Int.zero;



                if (srcRoom.boundingBox.center.y >= destRoom.boundingBox.center.y)
                {
                    // Srcroom above destroom
                    startPoint = new Vector2Int(midpointRounded.x, srcRoom.boundingBox.min.y);
                    endPoint = new Vector2Int(midpointRounded.x, destRoom.boundingBox.max.y);
                }
                else
                {
                    // Srcroom below destroom
                    startPoint = new Vector2Int(midpointRounded.x, srcRoom.boundingBox.max.y);
                    endPoint = new Vector2Int(midpointRounded.x, destRoom.boundingBox.min.y);
                }

                hallways.Add(new Hallway(new HallwayLine(startPoint, endPoint), srcRoomId, destRoomId));
            } else
            {
                // Make L shaped line
                Vector2Int horStartPoint = Vector2Int.zero;
                Vector2Int horEndPoint = Vector2Int.zero;
                if (srcRoom.boundingBox.center.x <= destRoom.boundingBox.center.x)
                {
                    // Srcroom left of destroom
                    horStartPoint = new Vector2Int(srcRoom.boundingBox.max.x, RoundNtoM(srcRoom.boundingBox.center.y, (int) gridmap.cellSize.x));
                    horEndPoint = new Vector2Int(RoundNtoM(destRoom.boundingBox.center.x, (int)gridmap.cellSize.x), RoundNtoM(srcRoom.boundingBox.center.y, (int) gridmap.cellSize.x));
                }
                else
                {
                    // Srcroom right of destroom
                    horStartPoint = new Vector2Int(srcRoom.boundingBox.min.x, RoundNtoM(srcRoom.boundingBox.center.y, (int)gridmap.cellSize.x));
                    horEndPoint = new Vector2Int(RoundNtoM(destRoom.boundingBox.center.x, (int)gridmap.cellSize.x), RoundNtoM(srcRoom.boundingBox.center.y, (int)gridmap.cellSize.x));
                }


                Vector2Int vertStartPoint = horEndPoint;
                Vector2Int vertEndPoint = Vector2Int.zero;
                if (srcRoom.boundingBox.center.y >= destRoom.boundingBox.center.y)
                {
                    // Srcroom above destroom
                    vertEndPoint = new Vector2Int(horEndPoint.x, destRoom.boundingBox.yMax);
                }
                else
                {
                    // Srcroom below destroom
                    vertEndPoint = new Vector2Int(horEndPoint.x, destRoom.boundingBox.yMin);
                }

                hallways.Add(new Hallway(new HallwayLine(horStartPoint, horEndPoint), new HallwayLine(vertStartPoint, vertEndPoint), srcRoomId, destRoomId));
            }
        }

        return hallways;
    }

    private Vector2f Vector2IntToVector2f(Vector2Int vec)
    {
        return new Vector2f(vec.x, vec.y);
    }

    private Vector2f Vector3ToVector2f(Vector3 vec)
    {
        return new Vector2f(vec.x, vec.y);
    }

    // Create physics boxes in the position/size we have generated so the physics engine
    // can space them out.
    private List<GameObject> SetupPhysicsRooms(List<Room> rooms)
    {
        var physicsRooms = new List<GameObject>();
        foreach (Room room in rooms)
        {
            GameObject physicsRoom = new GameObject();
            BoxCollider2D boxColl = physicsRoom.AddComponent<BoxCollider2D>();
            Rigidbody2D rb2d = physicsRoom.AddComponent<Rigidbody2D>();
            rb2d.gravityScale = 0;
            rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;

            boxColl.size = new Vector2(room.width, room.height);
            boxColl.edgeRadius = gridmap.cellSize.x * 2;

            physicsRoom.transform.position = new Vector3(room.boundingBox.center.x, room.boundingBox.center.y, 0);

            physicsRooms.Add(physicsRoom);

            //for (int x = room.position.x; x < room.width; x++)
            //{
            //    for (int y = room.position.y; y < room.height; y++)
            //    {
            //        noCollTilemap.SetTile(new Vector3Int(x, y, 0), noCollTiles[0]);
            //    }
            //}
        }
        return physicsRooms;
    }


    public void GenerateRandomFloor(Grid gridmap)
    {
        
        for (int y = lowerY; y < higherY; y++)
        {
            
            for (int x = lowerX; x < higherX; x++)
            {

                if (Random.value > 0.5)
                {
                    Vector2Int tilePos = new Vector2Int(x, y);
                    noCollTilemap.SetTile((Vector3Int)tilePos, noCollTiles[Random.Range(0, noCollTiles.Length)]);
                }
                
            }
        }
    }

    List<Room> GetRoomsInCircle(int radius, int tileSize, int numberOfRooms)
    {
        List<Room> rooms = new List<Room>();
        for (int i = 0; i < numberOfRooms; i++)
        {
            Vector2 posInCircle = Random.insideUnitCircle * radius;
            // Doesn't matter how we round it for now, will round to tilesize after physics simulation.
            Vector2Int posInCircleInt = new Vector2Int((int) posInCircle.x, (int) posInCircle.y);

            rooms.Add(new Room(posInCircleInt, Random.Range(minRoomWidth, maxRoomWidth), Random.Range(minRoomHeight, maxRoomHeight)));
        }

        return rooms;
    }

    public int RoundNtoM(float n, int m)
    {
        return Mathf.FloorToInt(((n + m - 1) / m)) * m;
    }

    public Vector2Int RoundVecToM(Vector2 vec, int m)
    {
        return new Vector2Int(RoundNtoM(vec.x, m), RoundNtoM(vec.y, m));
    }

    public Vector2 Vec2IntToVec2(Vector2Int vec2int)
    {
        return new Vector2(vec2int.x, vec2int.y);
    }

    public GameObject demoQuad;


    //private void DisplayVoronoiDiagram()
    //{
    //    Texture2D tx = new Texture2D(512, 512);
    //    Gizmos.color = Color.red;
    //    foreach (KeyValuePair<Vector2f, Site> kv in graphSites)
    //    {
    //        tx.SetPixel((int)kv.Key.x, (int)kv.Key.y, Color.red);
    //        Gizmos.DrawSphere(new Vector3(kv.Key.x, kv.Key.y), 5f);
    //    }

    //    Gizmos.color = Color.green;
    //    foreach (Edge edge in graphEdges)
    //    {
    //        if the edge doesn't have clippedEnds, if was not within the bounds, dont draw it
    //        if (edge.ClippedEnds == null) continue;

    //        Gizmos.DrawLine(new Vector3(edge.ClippedEnds[LR.LEFT].x, edge.ClippedEnds[LR.LEFT].y), new Vector3(edge.ClippedEnds[LR.RIGHT].x, edge.ClippedEnds[LR.RIGHT].y));
    //        DrawLine(edge.ClippedEnds[LR.LEFT], edge.ClippedEnds[LR.RIGHT], tx, Color.black);
    //    }
    //    tx.Apply();

    //    demoQuad.GetComponent<Renderer>().material.mainTexture = tx;
    //}

    private void OnDrawGizmos()
    {

        if (mainRooms != null)
        {
            if (drawMainRooms)
            {
                Gizmos.color = Color.red;
                foreach (Room room in mainRooms)
                {
                    Gizmos.DrawCube(new Vector3(room.boundingBox.center.x, room.boundingBox.center.y), new Vector3(room.width, room.height));
                }
            }

            foreach (var point in midPointsRounded)
            {
                Gizmos.DrawSphere(new Vector3(point.x, point.y), 2);
            }
        }


        if (graphSites != null || graphEdges != null)
        {

            //Gizmos.color = Color.red;
            //foreach (KeyValuePair<Vector2f, Site> kv in graphSites)
            //{
            //    //tx.SetPixel((int)kv.Key.x, (int)kv.Key.y, Color.red);
            //    Gizmos.DrawSphere(new Vector3(kv.Key.x, kv.Key.y), 5f);
            //}

            if (drawVoronoi)
            {
                Gizmos.color = Color.green;
                foreach (Edge edge in graphEdges)
                {
                    // if the edge doesn't have clippedEnds, if was not within the bounds, dont draw it
                    //if (edge.ClippedEnds == null) continue;
                    Gizmos.DrawLine(new Vector3(edge.LeftSite.x, edge.LeftSite.y), new Vector3(edge.RightSite.x, edge.RightSite.y));
                    //Gizmos.DrawLine(new Vector3(edge.ClippedEnds[LR.LEFT].x, edge.ClippedEnds[LR.LEFT].y), new Vector3(edge.ClippedEnds[LR.RIGHT].x, edge.ClippedEnds[LR.RIGHT].y));
                    //DrawLine(edge.ClippedEnds[LR.LEFT], edge.ClippedEnds[LR.RIGHT], tx, Color.black);
                }
            }
            
        }

        if (minSpanResult != null)
        {
            if (drawMinSpan)
            {
                Gizmos.color = Color.cyan;
                foreach (JacksEdge edge in minSpanResult)
                {
                    Gizmos.DrawLine(new Vector3(edge.src.x, edge.src.y), new Vector3(edge.dest.x, edge.dest.y));
                }
            }
        }

        if (hallways != null)
        {
            if (drawHallways)
            {
                Gizmos.color = Color.red;
                foreach (Hallway hallway in hallways)
                {
                    Gizmos.DrawLine(ToVector3(hallway.line.startPoint), ToVector3(hallway.line.endPoint));
                    if (hallway.UsingSecondLine)
                    {
                        Gizmos.DrawLine(ToVector3(hallway.secondLine.startPoint), ToVector3(hallway.secondLine.endPoint));
                    }
                }
            }
        }

    }

    private Vector3 ToVector3(Vector2Int vec)
    {
        return new Vector3(vec.x, vec.y, 0);
    }

    // Bresenham line algorithm
    private void DrawLine(Vector2f p0, Vector2f p1, Texture2D tx, Color c, int offset = 0)
    {
        int x0 = (int)p0.x;
        int y0 = (int)p0.y;
        int x1 = (int)p1.x;
        int y1 = (int)p1.y;

        int dx = Mathf.Abs(x1 - x0);
        int dy = Mathf.Abs(y1 - y0);
        int sx = x0 < x1 ? 1 : -1;
        int sy = y0 < y1 ? 1 : -1;
        int err = dx - dy;

        while (true)
        {
            tx.SetPixel(x0 + offset, y0 + offset, c);

            if (x0 == x1 && y0 == y1) break;
            int e2 = 2 * err;
            if (e2 > -dy)
            {
                err -= dy;
                x0 += sx;
            }
            if (e2 < dx)
            {
                err += dx;
                y0 += sy;
            }
        }
    }
}
