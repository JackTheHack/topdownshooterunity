﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchNode : MonoBehaviour, IHeapItem<SearchNode>
{

    public bool walkable;
    public Vector3 worldPosition;

    public int gCost;
    public int hCost;
    public int nodeArrayX, nodeArrayY;
    public Vector2Int tilemapPos;
    public List<SearchNode> myNeighbours;
    public SearchNode parent;

    int heapIndex;

    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }

        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(SearchNode other)
    {
        int compare = fCost.CompareTo(other.fCost);
        if (compare == 0) {
            compare = hCost.CompareTo(other.hCost);
        }
        return -1 * compare;
    }

    //public SearchNode(bool _walkable, Vector3 _worldPos)
    //{
    //    walkable = _walkable;
    //    worldPosition = _worldPos;
    //}


}
