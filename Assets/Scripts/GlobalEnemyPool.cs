﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEnemyPool : MonoBehaviour
{

    ObjectPool projectilePool;

    // Start is called before the first frame update
    void Start()
    {
        projectilePool = GetComponent<ObjectPool>();

        projectilePool.AddNewPoolType<BasicEnemyController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
